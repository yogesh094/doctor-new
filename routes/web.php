<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/', 'Auth\LoginController@showRetailStudentLoginForm');
///Route::get('/', function () {
//    return view('welcome');
//});

Route::any('/email/exist', 'Auth\RegisterController@checkEmailExist');

Auth::routes(['verify' => true]);


/**
 * Admin Forgot Password
 */
Route::get('/admin/password/reset', 'Auth\AdminAuth\ForgotPasswordController@showLinkRequestForm');
Route::post('/admin/password/email', 'Auth\AdminAuth\ForgotPasswordController@sendResetLinkEmail');
Route::get('/admin/password/reset/{token}', 'Auth\AdminAuth\ResetPasswordController@showResetForm');
Route::post('/admin/password/reset', 'Auth\AdminAuth\ResetPasswordController@reset');

// Guest Admin
Route::group(['middleware' => ['guest:admin'] ], function(){
    Route::any('/admin', 'Auth\LoginController@showAdminLoginForm');
    Route::post('/admin/check-login', 'Auth\LoginController@adminLogin')->name('admin-check-login');
});

// Admin Panel
Route::group(['prefix'=>'admin','middleware' => ['auth:admin'] ], function(){
    Route::get('/dashboard', 'Admin\DashboardController@index');
    Route::any('/logout', 'Auth\LoginController@logout');


    /**
     * Role Management
     */
    Route::group(['prefix'=>'role'], function() {
        Route::get('/', 'Admin\RoleController@index');
        Route::get('/add', 'Admin\RoleController@create');
        Route::post('/store', 'Admin\RoleController@store');
        Route::get('/edit/{id}', 'Admin\RoleController@edit');
        Route::post('/update', 'Admin\RoleController@update');
        Route::post('/datatable', ['uses' => 'Admin\RoleController@datatable']);
        Route::post('/delete', 'Admin\RoleController@delete')->name('admin.role.delete');
    });
    /**
     * Admin User Management
     */
    Route::group(['prefix'=>'user'], function() {
        Route::any('/', 'Admin\AdminUserController@index');
        Route::get('/add', ['as' => 'createMock', 'uses' => 'Admin\AdminUserController@create']);
        Route::get('/edit/{id}', ['as' => 'editMock', 'uses' => 'Admin\AdminUserController@edit']);
        Route::post('/store', ['as' => 'storeMock', 'uses' => 'Admin\AdminUserController@store']);
        Route::post('/update', ['as' => 'updateMock', 'uses' => 'Admin\AdminUserController@update']);
        Route::post('/datatable', ['uses' => 'Admin\AdminUserController@datatable']);
        Route::post('/delete', ['as' => 'deleteMockExam', 'uses' => 'Admin\AdminUserController@delete']);
        Route::post('/change_status', ['uses' => 'Admin\AdminUserController@changeStatus']);
    });

    /**
     * Permission management
     */
    Route::group(['prefix'=>'permission'], function() {
        Route::any('/', 'Admin\PermissionController@index');
        Route::get('/add', ['as' => 'createPermission', 'uses' => 'Admin\PermissionController@create']);
        Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'Admin\PermissionController@edit']);
        Route::post('/store', ['as' => 'storePermission', 'uses' => 'Admin\PermissionController@store']);
        Route::post('/update', ['as' => 'update', 'uses' => 'Admin\PermissionController@update']);
        Route::post('/datatable', ['uses' => 'Admin\PermissionController@datatable']);
        Route::post('/delete', ['as' => 'delete', 'uses' => 'Admin\PermissionController@delete']);
    });
});

Route::group(['middleware' => ['auth', 'verified'] ], function(){
  Route::get('/dashboard', 'User\DashboardController@index');

  // Profile Management
  Route::get('/profile', 'User\ProfileController@index');
  Route::get('/profile/{username}/{noti_id?}', 'User\ProfileController@userDetail');

  Route::post('/update', 'User\ProfileController@update');
  Route::post('/save-profile-image', 'User\ProfileController@updateProfilePicture');
  Route::post('/save-cover-image', 'User\ProfileController@updateCoverPicture');
  Route::get('/changepassword', 'User\ProfileController@showChangePassword');
  Route::post('/changepassword/update', 'User\ProfileController@changePassword');
  // Education
  Route::get('/education', 'User\ProfileController@showEducation');
  Route::post('/education/store', 'User\ProfileController@addEducation');
  Route::post('/education/edit', 'User\ProfileController@editEducation');
  Route::post('/education/delete', 'User\ProfileController@deleteEducation');
  // Experience
  Route::post('/experience/store', 'User\ProfileController@addExperience');
  Route::post('/experience/edit', 'User\ProfileController@editExperience');
  Route::post('/experience/delete', 'User\ProfileController@deleteExperience');


  Route::get('/interest', 'User\ProfileController@showInterest');
  Route::post('/interest/store', 'User\ProfileController@addInterest');


  // Timeline
  Route::get('/timeline/{username}', 'User\TimelineController@index');
    Route::post('/more-timeline/list', 'User\TimelineController@morePost');

  //Post-Comment- Reply

    Route::get('/news-feed', 'User\PostController@index');
    Route::get('/comment/{post_id}', 'User\PostController@index');
    Route::get('/reply/{comment_id}', 'User\PostController@index');
    Route::post('/add/post', 'User\PostController@store');
    Route::any('/add/comment', 'User\CommentController@store');
    Route::any('/post/edit', 'User\PostController@edit');
    Route::any('/post/delete', 'User\PostController@deletePost');
    Route::post('/add/reply', 'User\ReplyController@store');
    Route::post('/add/like-dislike', 'User\PostController@addPostLike');
    Route::post('/more-comment/list', 'User\CommentController@moreComment');
    Route::post('/more-reply/list', 'User\ReplyController@moreReply');
    Route::post('/more-post/list', 'User\PostController@morePost');
    Route::post('/delete/comment', 'User\CommentController@delete');
    Route::post('/delete/reply', 'User\ReplyController@delete');
  // Friend Request Management
  Route::get('/friend-list', 'User\ConnectionRequestController@index');
  Route::post('/more-friend/list', 'User\ConnectionRequestController@moreFriend');
  Route::post('/pending-request/list', 'User\ConnectionRequestController@morePendingRequest');
  Route::post('/send-request/{request_to}', 'User\ConnectionRequestController@sendRequest');
  Route::post('/cancel-request/{request_id}', 'User\ConnectionRequestController@cancelRequest');
  Route::post('/confirm-request/{request_id}', 'User\ConnectionRequestController@confirmRequest');

  //Notification

  Route::get('/notification', 'User\NotificationController@index');
  Route::get('/get-push-notification-ajax', 'User\NotificationController@getPushNotificationMessage');
  Route::get('/post/{post_id}/{noti_id}', 'User\PostController@singlePost');
  Route::post('/more-noti/list', 'User\NotificationController@moreNotification');

});

/**
 * common route
 */
Route::post('/getstate', 'Admin\CountryController@getState');

//Route::get('/home', 'HomeController@index')->name('home');
