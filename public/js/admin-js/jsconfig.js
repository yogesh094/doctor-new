/**
 * @file
 * A JavaScript file for Application configuration
 *
 */


// Define App and datatable variable

var app = app || {};
var dataTable, searchBtn;

app.config = {
    SITE_PATH: baseUrl,
    init: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
};

// Log data in console
function l(data) {
    console.log(data);
}

// Check string is valid json or not
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

// generate random password
function generate_password(){
    var randomstring = Math.random().toString(36).slice(-8);
    $('#password').val(randomstring);
}


// Show loading mask
app.showLoader = function (id) {
    $((id) ? id : 'body').mask("<img src='" + app.config.SITE_PATH + "global/photos/loading.svg'");
}

// Hide loading mask
app.hideLoader = function (id) {
    $((id) ? id : 'body').unmask();
}

// Remove loading mask
app.removeLoader = function () {
    $('.loadmask').remove();
    $('.loadmask-msg').remove();
    $('div.masked').removeClass('masked');
}

//Check for mobile device
var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPod/i);
    },
    ipad: function () {
        return navigator.userAgent.match(/iPad/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    BB: function () {
        return navigator.userAgent.match(/BB10/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() || isMobile.BB());
    }
};

// Check all check box
app.checkAll = function () {
    $('#checkAll').on('click', function () {
        $('.checkAllChild').prop('checked', $(this).is(":checked"));
    });

    $('.checkAllChild').on('click', function () {
        if ($(this).is(":checked")) {
            if ($('.checkAllChild').length === $('.checkAllChild:checked').length) {
                $('#checkAll').prop('checked', $(this).is(":checked"));
            }
        } else {
            $('#checkAll').prop('checked', $(this).is(":checked"));
        }
    });
}

// change status

app.changeStatus = function (id,url,status) {

    $.ajax({
        type: "POST",
        url: url,
        data: { id : id,status:status,_token: csrf_token}
    }).done(function(data){
        location.reload();
    });
}

// Delete Record

app.deleteRecord = function (id,url) {
    $.ajax({
        type: "POST",
        url: url,
        data: { id : id,_token: csrf_token}
    }).done(function(data){
        location.reload();
    });
}

// datepicker
app.datepicker = function (param) {
    $("#"+param).datetimepicker({
        formatDate: 'd.m.Y',
        format:'d-M-Y',
        timepicker:false,
        closeOnDateSelect:true
    });
}

app.validate = {
    init:function (params) {
        if(typeof params != undefined) {
            $("#app_form").validate(params);
        }else{
            $("#app_form").validate();
        }
    }
}

// Hide Flash and validation message
$(function() {
    setTimeout(function(){
        $(".card-alert").slideUp(500).html("");
    },5000);

    $(".close").click(function(){
        $(".card-alert").hide();
    });

});
app.changeState = function (id,url,state_id) {
    $.ajax({
        type: "POST",
        url: url,
        data: { country_id : id,state_id : state_id,  _token: csrf_token}
    }).done(function(data){
        $("#state_id").html(data.stateList);
    });
}

app.selectedCountryState = function () {
    var name = 'country';
    var id = $('select[name="' + name + '"]').val();
    var state_id = $('#selected_state').val();
    var url = app.config.SITE_PATH +'getstate';
    app.changeState(id, url,state_id);
}


$('#password, #confirm_password').on('keyup', function () {
    if ($('#password').val() == $('#password-confirm').val()) {
        $('#message').html('Matching').css('color', 'green');
    } else
        $('#message').html('Not Matching').css('color', 'red');
});

$('#register-submit-btn').on('click', function () {

    var error_cnt = 0;
    if ($('#password').val() != $('#password-confirm').val()) {
        var msg = "The password confirmation does not match.";
        $('#cnf-password-err').html('<strong>'+msg+'</strong>');
        error_cnt = error_cnt + 1;
    }
    if ($('#password').val().length < 6){
        var msg = "The password must be at least 6 characters.";
        $('#password-err').html('<strong>'+msg+'</strong>');
        error_cnt = error_cnt + 1;
    }
    if ($('#name').val() == '') {
        var msg = "The name field is required.";
        $('#name-err').html('<strong>'+msg+'</strong>');
        error_cnt = error_cnt + 1;
    }
    if ($('#email').val() == '') {
        var msg = "The email field is required.";
        $('#email-err').html('<strong>'+msg+'</strong>');
        error_cnt = error_cnt + 1;
    }
    if ($('#username').val() == '' || $('#username').val().length < 4 ) {
        var msg = "Please Enter valid user name";
        $('#username-err').html('<strong>'+msg+'</strong>');
        error_cnt = error_cnt + 1;
    }

    if($('#from_india').val() == 1){
        if ($('select[name=state]').val() == 0){
            var msg = "The state field is required.";
            $('#state-err').html('<strong>'+msg+'</strong>');
            error_cnt = error_cnt + 1;
        }
    }else{
        if ($('select[name=country]').val() == 0){
            var msg = "The country field is required.";
            $('#country-err').html('<strong>'+msg+'</strong>');
            error_cnt = error_cnt + 1;
        }
    }


    if(error_cnt > 0){
        $('.invalid-feedback').css('display','block');
        var error_cnt = 0;
        return false;
    }else{
        $("#retail-register-form").submit();
    }

});

$(document).on("change", "#question_image", function(){
    var ext = $('#question_image').val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
        alert('invalid Input File!');
        $("#submit-btn").attr("disabled", true);
    }else{
        $("#submit-btn").attr("disabled", false);

    }
});

$(document).on("change", "#question_audio", function(){
    var ext = $('#question_audio').val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['mp3','mpeg','mp4']) == -1) {
        alert('invalid Input File!');
        $("#submit-btn").attr("disabled", true);
    }else{
        $("#submit-btn").attr("disabled", false);
    }
});

/* Detect any change of option*/
$("#select").change(function(){
    var icono = $(this).val();
    $("#icon").html('<i class="fa fa-2x ' + icono + '"></i>');
});


// Number validation
// $('input[name="mobile_no"]').keyup(function(e)
// {
//     if (/\D/g.onlyNumber(this.value))
//     {
//         // Filter non-digits from input value.
//         this.value = this.value.replace(/\D/g, '');
//     }
// });

// app.ipMask = function (v) {
//     if (v.match(/^\d{3}$/) !== null) {
//         this.value = v + '.';
//     } else if (v.match(/^\d{3}.\d{3}$/) !== null) {
//         this.value = v + '.';
//     }else if (v.match(/^\d{3}.\d{3}.\d{3}$/) !== null) {
//         this.value = v + '.';
//     }
//     alert(this.value);
// }