
$(document).ready(function () {
    $('.video').parent().click(function () {
        if ($(this).children(".video").get(0).paused) {
            $(this).children(".video").get(0).play();
            $(this).children(".playpause").fadeOut();
        } else {
            $(this).children(".video").get(0).pause();
            $(this).children(".playpause").fadeIn();
        }
    });


    $('#headline').on("keyup", function () {
        $('.campaign_headline').text($(this).val());
    });
    $('.details').on("keyup", function () {
        $('.campaign_details').text($(this).val());
    });
    $('.video_upload').on("change", function (evt){
        $('#loader_video').hide();
        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.mp4)$");
        if (regex.test(this.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            // if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            $('.pre_media_image').attr('src', "");
            $('.pre_media_image').hide();
            var reader = new FileReader();

            $('#avatar-modal').modal('hide');
            $('.res_video_main').hide();
            $('.thumb_img').show();
            $('.media_type').val(1);
            var $source = $('#res-video-preview');
            $source[0].src = URL.createObjectURL(this.files[0]);
            localStorage.setItem('video_file',URL.createObjectURL(this.files[0]));
            $source.parent()[0].load();
            var formData = new FormData($("#upload_form")[0]);
            //formData.append('_token', $("input[name=_token]").val());
            $('#loader_video').show();
            $.ajax({
                url: $("#base_url").val() + "/campaign/uploadThumbnail",
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (result)
                {
//                    console.log(result);
//                    $('.pre_media_image').attr('src', result.thumbUrl);

                    $('.video_name').val(result.video_name);
                    $('.video_thumb').val(result.thum_name);
                    $('#is_video_upload').val(1);
                    $('.video_upload').val("");
                    $('.res_video_main').show();
                    $('.res_video_main').attr('poster', result.thumbUrl);
                    $('#loader_video').hide();
                    localStorage.setItem('videoThumbnail',result.thumbUrl);

                },
                error: function (data)
                {
                    console.log(data);
                }
            });


        } else {
            $('.video_upload').val("");
            $('#popupModal .modal-body p').text('Please select a valid mp4 video.');
            $('#popupModal').modal("show");
            return false;
        }
    });
    $('.media_upload').on("change", function (evt) {
        var error = null;

        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
        if (regex.test(this.value.toLowerCase())) {
            var reader = new FileReader();
            $('.res_video_main').hide();
            $('.media_type').val(0);
            //Read the contents of Image File.
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    console.log(this.width);
                     var height = this.height;
                     var width = this.width;
                     if(width < 400){
                    // $('.pre_media_image').css({"style":"width:400px;"});
                    $('.pre_media_image').css("width","300px");
                        localStorage.setItem('tempImgwidth',"300px");
                    }else{
                        localStorage.setItem('tempImgwidth',"");
                        $('.pre_media_image').css("width","");
                    }
                }
                // console.log(width);
                localStorage.setItem('tempImg',"image");
                //Set the Base64 string return from FileReader as source.
                $('.pre_media_image').show();
                $('.pre_media_image').attr('src', e.target.result);
                $('#hidden_media_upload').val(1);
                 $('#is_media_upload').val(1);
                 $('#video_upload').val(0);
            }
        } else {
            $('.media_upload').val();
            $('#popupModal .modal-body p').text("Please select a valid image (.png, .jpeg, .jpg).");
            $('#popupModal').modal("show");
            return false;
        }
    });

    $(document).on("keyup",'*[id^=reply-box-]',function (event) {
        if (event.which == 13) {
        var currentId = this.id;
        console.log(currentId);
        // var campaignUserId =$(this).attr('data-campaignUserId');
        var commentId = currentId.replace("reply-box-","");
        $('#reply_btn_'+commentId).trigger('click');
        }
    });

    $(document).on("keyup",'*[id^=append-comment_]',function (event) {
        if (event.which == 13) {
            var id = this.id;
            var split_id = id.split("_");//new,comment,1,1
            var postId = split_id[1]; // campaign id
            $('#new_comment_'+postId).trigger('click');
        }
    });
    $(document).on('click','*[id^=new_comment_]',function (e){
        e.preventDefault();
        var id = this.id;
        var split_id = id.split("_");//new,comment,1,1
        var postId = split_id[2]; // campaign id
        var comment = $("#comment_"+postId).val();
        var commentcount = $('#commentcount_'+postId).html();
        if($.trim(comment).length > 0){
            $.ajax({
                url: baseUrl + "add/comment",
                type: 'POST',
                data: {'description': comment,'post_id': postId},
                dataType: "json",
                timeout: 20000,
                headers: {'X-CSRF-TOKEN':csrf_token },
                success: function (data) {
                    if (data.status == 200) {
                        //$('#append-comment_'+postId).append(data.comment_html);
                        $('#append-comment_'+postId).find(' > li:nth-last-child(2)').before(data.comment_html);
                        $('#commentcount_'+postId).html(parseInt(commentcount) + 1);
                        $("#comment_"+postId).focus();
                        $("#comment_"+postId).val('');
                    }else{
                        $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                        $('#popupModal').modal("show");
                        return false;
                    }
                },
            });
        }
        return false;
    });
        //hide-show unique reply button
    $(document).on("click",'.reply-box-added',function () {
        var commentId = $(this).attr('data-commentId');
        if($("#reply-box_"+commentId).css('display') == 'none'){
            $("#append-reply_"+commentId).show();
            $("#reply-box_"+commentId).show();
        }else {
            $("#reply_text_"+commentId).focus();
        }
    });

    $(document).on("keyup",'*[id^=reply_text_]',function (event) {
        if (event.which == 13) {
            var id = this.id;
            var split_id = id.split("_");//new,comment,1,1
            var comment_id = split_id[2]; // campaign id
            $('#new_reply_'+comment_id).trigger('click');
        }
    });
        //ajax on new reply
    $(document).on('click','*[id^=new_reply_]',function (e){
        e.preventDefault();
        var currentId = $(this).attr('id');
        var commentId = currentId.replace("new_reply_", "");
        var postId =$(this).attr('data-postId');
        var comment = $("#reply_text_"+commentId).val();
        var commentcount = $('#commentcount_'+postId).html();
        if($.trim(comment).length > 0){
            $.ajax({
                url: baseUrl + "add/reply",
                type: 'POST',
                data: {'description': comment,'post_id': postId,'post_comment_id':commentId},
                dataType: "json",
                timeout: 20000,
                headers: {'X-CSRF-TOKEN':csrf_token },
                success: function (data) {
                    if (data.status == 200) {
                        $('#append-reply_'+commentId).find(' > li:nth-last-child(1)').before(data.comment_html);
                        $('#commentcount_'+postId).html(parseInt(commentcount) + 1);
                        $("#reply_text_"+commentId).val('');
                    }else{
                        $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                        $('#popupModal').modal("show");
                        return false;
                    }
                },
            });
        }
        return false;
    });
    //like-dislike part
    $(document).on('click','*[id^=postlike_]',function (e){

        //$.LoadingOverlay("show");
        var response_flag = 1;
        var id = this.id;
        var split_id = id.split("_");
        var postId = split_id[1];
        var already_response = $('#islike_'+postId).html();
        // var already_response= $("#is-like-"+campaignId).val(); //already like or not
            var likecount = parseInt($("#likecount_"+postId).html());
            var dislikecount = parseInt($("#dislikecount_"+postId).html());

            if(already_response == 1){
                $('#likeclass_'+postId).removeAttr('class');
                $('#likeclass_'+postId).attr('class', 'fa fa-thumbs-o-up');
                response_flag = 0;
                likecount = likecount -1;
            }else if(already_response == 2){
                $('#dislikeclass_'+postId).removeAttr('class');
                $('#dislikeclass_'+postId).attr('class', 'fa fa-thumbs-o-down');
                $('#likeclass_'+postId).removeAttr('class');
                $('#likeclass_'+postId).attr('class', 'fa fa-thumbs-up');// $(this).find($(".fa")).toggleClass('fa fa-thumbs-o-down fa fa-thumbs-down');
                response_flag = 1;
                dislikecount = dislikecount -1;
                likecount = likecount +1;
            }else{
                $('#likeclass_'+postId).removeAttr('class');
                $('#likeclass_'+postId).attr('class', 'fa fa-thumbs-up');
                response_flag = 1;
                likecount = likecount +1;
            }
            $('#islike_'+postId).html(response_flag);
            $("#likecount_"+postId).html(likecount);
            $("#dislikecount_"+postId).html(dislikecount);
            $.ajax({
                url: baseUrl + "add/like-dislike",
                type: 'POST',
                data: {'is_like': response_flag,'post_id': postId},
                dataType: "json",
                timeout: 20000,
                headers: {'X-CSRF-TOKEN':csrf_token },
                success: function (data) {
                    //$.LoadingOverlay("hide");
                    if (data.status == 200) {
                    }else{
                        $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                        $('#popupModal').modal("show");
                        return false;
                    }
                },
            });
            return false;
    });

    $(document).on('click','*[id^=postdislike_]',function (e){
        var response_flag = 2;
        var id = this.id;
        var split_id = id.split("_");
        var postId = split_id[1];
        // var already_response= $("#is-like-"+campaignId).val(); //already like or not
        var likecount = parseInt($("#likecount_"+postId).html());
        var dislikecount = parseInt($("#dislikecount_"+postId).html());
        var already_response = $('#islike_'+postId).html();
        if(already_response == 2){
            $('#dislikeclass_'+postId).removeAttr('class');
            $('#dislikeclass_'+postId).attr('class', 'fa fa-thumbs-o-down');
            response_flag = 0;
            dislikecount = dislikecount -1;
        }else if(already_response ==1){
            $('#likeclass_'+postId).removeAttr('class');
            $('#likeclass_'+postId).attr('class', 'fa fa-thumbs-o-up');
            $('#dislikeclass_'+postId).removeAttr('class');
            $('#dislikeclass_'+postId).attr('class', 'fa fa-thumbs-down');
            response_flag = 2;
            likecount = likecount -1;
            dislikecount = dislikecount +1;
        }else{
            $('#dislikeclass_'+postId).removeAttr('class');
            $('#dislikeclass_'+postId).attr('class', 'fa fa-thumbs-down');
            response_flag = 2;
            dislikecount = dislikecount +1;
        }
        $('#islike_'+postId).html(response_flag);
        $("#likecount_"+postId).html(likecount);
        $("#dislikecount_"+postId).html(dislikecount);
        $.ajax({
            url: baseUrl + "add/like-dislike",
            type: 'POST',
            data: {'is_like': response_flag,'post_id': postId},
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                if (data.status == 200) {
                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
        return false;
    });

    //load more'*[id^=adwall_]'
    $(document).on('click','*[id^=load-more-reply-]',function (e){
        e.preventDefault();
        var id = this.id;
        var split_id = id.split("-");//new,comment,1,1
        var commentId = split_id[3];  //
        var nextPage = $('#next-page-'+commentId).val();
        $.LoadingOverlay("show");
        $.ajax({
            url: baseUrl + "more-reply/list"+ '?page=' + nextPage,
            type: 'POST',
            data: {'comment_id': commentId},
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                $.LoadingOverlay("hide");
                if (data.status == 200) {
                    $('#append-reply_'+commentId).find(' > li:nth-last-child(2)').before(data.reply_html);
                    $('#next-page-'+commentId).val(data.next_page);
                    // $('#loader-'+postId).hide();
                    if (data.next_page > data.last_page) {
                        $('#load-more-reply-'+commentId).hide();
                    }

                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
    });


    $(document).on('click','*[id^=load-more-comment-]',function (e){
        e.preventDefault();
        var id = this.id;
        var split_id = id.split("-");//new,comment,1,1
        var postId = split_id[3];  //
        var nextPage = $('#next-page-'+postId).val();
        $.LoadingOverlay("show");
        //$('#loader-'+postId).show();
        $.ajax({
            url: baseUrl + "more-comment/list"+ '?page=' + nextPage,
            type: 'POST',
            data: {'post_id': postId},
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                $.LoadingOverlay("hide");
                if (data.status == 200) {
                    //$('#append-comment_'+campaignId).append(data.comment_html);
                    $('#append-comment_'+postId).find(' > li:nth-last-child(2)').before(data.comment_html);
                    $('#next-page-'+postId).val(data.next_page);
                   // $('#loader-'+postId).hide();
                    if (data.next_page > data.last_page) {
                        $('#load-more-comment-'+postId).hide();
                    }
                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
    });


    $('#load-more-post').click(function (e) {
        e.preventDefault();
        var nextPage = $('#next-page-post').val();
        $.LoadingOverlay("show");
        //$('#loader-'+postId).show();
        $.ajax({
            url: baseUrl + "more-post/list"+ '?page=' + nextPage,
            type: 'POST',
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                $.LoadingOverlay("hide");
                if (data.status == 200) {
                    $('#new-post').append(data.post_html);
                    $('#next-page-post').val(data.next_page);
                    // $('#loader-'+postId).hide();
                    if (data.next_page > data.last_page) {
                        $('#load-more-post').hide();
                    }

                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
    });

    $('#load-more-timeline').click(function (e) {
        $.LoadingOverlay("show");
        e.preventDefault();
        var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
        var username = url.substring(url.lastIndexOf('/') + 1);
        var nextPage = $('#next-page-post').val();
        //$('#loader-'+postId).show();
        $.ajax({
            url: baseUrl + "more-timeline/list"+ '?page=' + nextPage,
            type: 'POST',
            data: {'username' : username},
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                $.LoadingOverlay("hide");
                if (data.status == 200) {
                    $('#new-post').append(data.post_html);
                    $('#next-page-post').val(data.next_page);
                    // $('#loader-'+postId).hide();
                    if (data.next_page > data.last_page) {
                        $('#load-more-timeline').hide();
                    }

                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
    });

    /*
     * Event for load more notification
     */
    $('#load-more-notification').click(function (e) {
        e.preventDefault();
        var nextPage = $('#next-page').val();
        var baseUrl = $('#base_url').val();
        $('#loader').show();
        $.ajax({
            url: baseUrl + "/notification/ajax-list?page=" + nextPage,
            type: "GET",
            dataType: "json",
            success: function (response) {
                $('.append-notification').append(response.content);
                $('#next-page').val(response.next_page);
                $('#loader').hide();
                if (response.next_page > response.last_page) {
                    $('#load-more-notification').hide();
                }
            }
        })
    });

    $(document).on('click','.delete-comment',function (e){
        e.preventDefault();
        var id = this.id;
        var split_id = id.split("_");//new,comment,1,1
        var comment_id =$(this).attr('data-commentId');
        var post_id =$(this).attr('data-postId');
        $.LoadingOverlay("show");
        var commentcount = $('#commentcount_'+post_id).html();
            $.ajax({
                url: baseUrl + "delete/comment",
                type: 'POST',
                data: {'id': comment_id},
                dataType: "json",
                timeout: 20000,
                headers: {'X-CSRF-TOKEN':csrf_token },
                success: function (data) {
                    $.LoadingOverlay("hide");
                    if (data.status == 200) {
                        $('#commentcount_'+post_id).html(parseInt(commentcount) - 1);
                        $("#comment-block-"+comment_id).remove();
                    }else{
                        $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                        $('#popupModal').modal("show");
                        return false;
                    }
                },
            });
        return true;
    });

    $(document).on('click','.delete-reply',function (e){
        e.preventDefault();
        var id = this.id;
        var split_id = id.split("_");//new,comment,1,1
        var reply_id =$(this).attr('data-replyId');
        var post_id =$(this).attr('data-postId');
        var commentcount = $('#commentcount_'+post_id).html();
        $.LoadingOverlay("show");
        $.ajax({
            url: baseUrl + "delete/reply",
            type: 'POST',
            data: {'id': reply_id},
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                $.LoadingOverlay("hide");
                if (data.status == 200) {
                    $('#commentcount_'+post_id).html(parseInt(commentcount) - 1);
                    $("#reply-block-"+reply_id).remove();
                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
        return true;
    });

    $('.post-delete').click(function(){
        var delConfirmationResult = confirm("Are you sure you want to delete this post?");
        if (!delConfirmationResult) {
            return;
        }
        var id = $(this).find('a').attr("data-id");
        var ajax_url = app.config.SITE_PATH + 'post/delete';
        $.LoadingOverlay("show");
        $.ajax({
            url: ajax_url,
            type: 'POST',
            data: {"id":id},
            cache: false,
            timeout: 20000,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (data) {
                $.LoadingOverlay("hide");
                if(data.status == 200){
                    window.location.reload();
                }else{
                    $('#popupModal .modal-body p').text(data.msg);
                    $('#popupModal').modal("show");
                    return false;
                }
            }
        });
    });

    $('.post-edit').click(function(){
        $('.post_id').remove();
        $('.postForm textarea').text('');
        $('.postForm img').hide();
        $('#post_upload_video').hide();
        $('.postForm img').attr("src",'#');
        $('#post_upload_video').attr("src",'#');

        var id = $(this).next().find('a').attr("data-id");
        var ajax_url = app.config.SITE_PATH + 'post/edit';
        $.ajax({
            url: ajax_url,
            type: 'POST',
            data: {"id":id},
            cache: false,
            timeout: 20000,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (data) {
                setEditPostModalData(data);
                $('.new-postbox').click();
                return false;
            }
        });

    });
});
