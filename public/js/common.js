$(document).ready(function(){



    getPushMessage();
    setInterval(getPushMessage,20000);


    $('#noti_Counter').css({opacity: 0}).css({top: '-10px'}).animate({left: '-2px', top: '8px', opacity: 1}, 500);
    $('.noti_Button').click(function () {
        $.ajax({
            url: $("#base_url").val() + "/read-message-counter-ajax",
            type: 'POST',
            data: $("#readCounter").serialize(),
            success: function (result)
            {
            },
            error: function (data)
            {
                console.log(data);
            }
        });
        $('#notifications').fadeToggle('fast', 'linear', function () {
        });
        $('#noti_Counter').fadeOut('slow');                 // HIDE THE COUNTER.
        return false;
    });
    $(document).click(function () {
        $('#notifications').hide();
        if ($('#noti_Counter').is(':hidden')) {
        }
    });
    $('.not-hide').click(function () {
        return false;       // DO NOTHING WHEN CONTAINER IS CLICKED.
    });


    app.selectedCountryState();
    //for edit selected state and country
    $(document).on('change', "#country_id", function () {
        var name = 'country';
        var id = $('select[name="' + name + '"]').val();
        var state_id = $('#selected_state').val();
        var url = app.config.SITE_PATH + 'getstate';
        $.ajax({
        type: "POST",
        url: url,
            data: { country_id : id,state_id : state_id,  _token: csrf_token}
        }).done(function(data){
            $("#state_id").html(data.stateList);
        });
    });

    

   
});

 $('#password-confirm').keyup(function(e){
    var pass = $('#password').val();
    var cpass = $('#password-confirm').val();
    if(cpass != pass){
        var msg = "The password confirmation does not match.";
        $('#error').text(msg).css('color','red');
        return false;
    }else{
        $('#error').text('');
    }
});

$('.addFriend').click(function(e){
    event.preventDefault();
    var request_to = $(this).attr("data-to");
    var lir = $(this).parent().parent();
    var ajax_url = app.config.SITE_PATH + 'send-request/'+request_to;
      $.ajax({
          url: ajax_url,
          type: 'POST',
          cache: false,
          timeout: 20000,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function (data) {
            if(data.status == 200){
                lir.remove();
                $('#messageModal .modal-body p').text('Friend request is sent successfully');
                $('#messageModal').modal("show");
            }else{
              $('#popupModal .modal-body p').text(data.msg);
              $('#popupModal').modal("show");
              return false;
            }
          }
      });
});


function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    var type = input.files[0].type; // image/jpg, image/png, image/jpeg...

    // allow jpg, png, jpeg, bmp, gif, ico
    var type_reg = /^image\/(jpg|png|jpeg|bmp|gif|ico)$/;

    
    
    reader.onload = function(e) {

      if (type_reg.test(type)) {
        $('#post_upload_img').attr('src', e.target.result);
        $('#post_upload_img').show();
      } else {
        $('#post_upload_video').attr('src', window.URL.createObjectURL(input.files[0]));
        $('#post_upload_video').show();
      }
      
    }
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function setEditPostModalData(postJSON) {
    // var storage_url = "<?php echo config('services.upload_url') ?>";
    // alert(storage_url);
    $("textarea.post-desc").val('');
    $("textarea.post-desc").val(postJSON.description);
    if(postJSON.media_type == 1){
        $('.postForm img').attr("src",storageUrl+'/image/user/'+postJSON.user_id+'/'+postJSON.media);
        $('.postForm img').show();
    }else if(postJSON.media_type == 2){
        $('.postForm video').attr("src",storageUrl+'/image/user/'+postJSON.user_id+'/'+postJSON.media);
        $('.postForm video').show();
    }
    var el = '<input type="hidden" name="id" class="post_id" value="'+postJSON.id+'"></input>';
    $('.postForm').append(el);
}
function getPushMessage() {
    $.ajax({
        url: baseUrl + "get-push-notification-ajax",
        type: 'GET',
        dataType: 'json',
        success: function (result)
        {
            $("#top-notification").html(result.notification);
            // $("#last_message_id").val(result.last_message_id);
            //
            // if(parseInt($(".notification-badge").text())  != parseInt(result.totalCount) ){
            // $('#noti_Counter').css({opacity: 0}).css({top: '-10px'}).animate({left: '-2px', top: '8px', opacity: 1}, 500);
            // }
            $("#noti_count").html(result.noti_count);
        },
        error: function (data)
        {
            console.log(data);
        }
    });
}

$("#postImgUpload").change(function() {
  readURL(this);
});
