
app.user = {

    events: {
        switch: function () {
            $(document).on('click', ".switch-button", function () {
                var id = parseInt($(this).attr('id'));
                var status = parseInt($(this).attr('rel'));
                var spath = $(this).attr('formaction');
                var url = app.config.SITE_PATH + spath +'/change_status';
                app.changeStatus(id, url, status);
            });
        },

        delete: function () {
            $(document).on('click', ".deleteRecord", function () {
                var dpath = $(this).attr('formaction');
                var result = confirm("Are you sure you want to delete this record?");
                if (result) {
                    var id = $(this).attr('rel');
                    var url = app.config.SITE_PATH + dpath +'/delete';
                    app.deleteRecord(id, url);
                }
            });
        },
        init: function () {
            app.user.events.switch();
            app.user.events.delete();
            app.dataTable.search();
            app.dataTable.reset();
        },

    },

    init: function () {
        app.user.events.init();
        app.dataTable.custom({"url":'admin/user/datatable'});
        app.dataTable.eventFire();
    }
}
