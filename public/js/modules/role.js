
app.role = {

    events: {
        switch: function () {
            $(document).on('click', ".change_status", function () {
                alert('hhee');
                var status = '1';
                if ($(this).closest("div.switch-button").children("input:checked").length > '0') {
                    status = '0';
                }

                var id = $(this).closest("div.switch-button").children("input").attr('rel');
                var spath = $(this).closest("div.switch-button").children("input").attr('formaction');
                var url = app.config.SITE_PATH + spath +'/change_status';
                app.changeStatus(id, url, status);
            });
        },

        delete: function () {
            $(document).on('click', ".deleteRecord", function () {
                var dpath = $(this).attr('formaction');
                var result = confirm("Are you sure you want to delete this record?");
                if (result) {
                    var id = $(this).attr('rel');
                    var url = app.config.SITE_PATH + dpath +'/delete';
                    app.deleteRecord(id, url);
                }
            });
        },

        init: function () {
            app.role.events.switch();
            app.role.events.delete();
            app.dataTable.search();
            app.dataTable.reset();

        },

    },



    init: function () {
        app.role.events.init();
        app.dataTable.custom({"url":'admin/role/datatable'});
        app.dataTable.eventFire();
    }
}
