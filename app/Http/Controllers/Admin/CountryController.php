<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use Spatie\Permission\Models\Role;
use DB;
use Validator;
use Mail;
use Auth;
use Carbon\Carbon;

class CountryController extends Controller
{

    protected $country;
    protected $state;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Country $country,State $state)
    {
        $this->middleware('permission:country-list|country-create|country-edit|country-delete', ['only' => ['index','store']]);

        //$this->middleware('permission:country-list');
        $this->middleware('permission:country-create', ['only' => ['create','store']]);
        $this->middleware('permission:country-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:country-delete', ['only' => ['destroy']]);
        $this->country = $country;
        $this->state = $state;
    }

    /**
     * Display a listing of the country.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /**
         * getCollection from App/Models/Country
         *
         * @return mixed
         */
        $data['countryData'] = $this->country->getCollection();
        $data['countryTab'] = 'active open';
        $data['module'] = 'country';
        return view('admin.country.list', $data);
    }

    public function datatable(Request $request)
    {
        // default count of country $countryCount
        $countryCount = 0;

        /**
         * getDatatableCollection from App/Models/Country
         * get all countrys
         *
         * @return mixed
         */
        $countryData = $this->country->getDatatableCollection();

        /**
         * scopeGetFilteredData from App/Models/Country
         * get filterred countrys
         *
         * @return mixed
         */
        $countryData = $countryData->GetFilteredData($request);

        /**
         * getCountryCount from App/Models/Country
         * get count of countrys
         *
         * @return integer
         */
        $countryCount = $this->country->getCountryCount($countryData);

        // Sorting country data base on requested sort order
        if (isset(config('constant.countryDataTableFieldArray')[$request->order['0']['column']])) {
            $countryData = $countryData->SortCountryData($request);
        } else {
            $countryData = $countryData->SortDefaultDataByRaw('tbl_country.id', 'desc');
        }

        /**
         * get paginated collection of country
         *
         * @param  \Illuminate\Http\Request $request
         * @return mixed
         */
        $countryData = $countryData->GetCountryData($request);

        $appData = array();
        foreach ($countryData as $countryData) {
            $row = array();
            $row[] = $countryData->name;
            $row[] = $countryData->code;
            $row[] = $countryData->currency;
            $row[] = view('datatable.switch', ['module' => "country",'url' => 'admin/country','status' => $countryData->status, 'id' => $countryData->id])->render();
            $row[] = view('datatable.action', ['module' => "country",'url' => 'admin/country','id' => $countryData->id])->render();
            $appData[] = $row;
        }

        return [
            'draw' => $request->draw,
            'recordsTotal' => $countryCount,
            'recordsFiltered' => $countryCount,
            'data' => $appData,
        ];
    }

    /**
     * Show the form for creating a new country.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['countryTab'] = 'active open';
        return view('admin.country.add',$data);
    }

    /**
     * Display the specified country.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        /**
         * get details of the specified country. from App/Models/Country
         *
         * @param mixed $id
         * @param string (id) fieldname
         * @return mixed
         */
        $data['details'] = $this->country->getCountryByField($id, 'id');
        $data['countryTab'] = 'active open';
        return view('admin.country.edit', $data);
    }

    /**
     * Validation of add and edit action customeValidate
     *
     * @param array $data
     * @param string $mode
     * @return mixed
     */
    public function customeValidate($data, $mode)
    {
        $rules = array(
            'name' => 'required|max:50',
            'code' => 'required',
            'phonecode' => 'required',
        );
        if($mode == 'edit') {
            $rules = array(
                'name' => 'required|max:50',
                'code' => 'required',
                'phonecode' => 'required',
            );
        }

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $errorRedirectUrl = "admin/country/add";
            if ($mode == "edit") {
                $errorRedirectUrl = "admin/country/edit/" . $data['id'];
            }
            return redirect($errorRedirectUrl)->withInput()->withErrors($validator);
        }
        return false;
    }

    /**
     * Store a newly created country in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {

        $validations = $this->customeValidate($request->all(), 'add');
        if ($validations) {
            return $validations;
        }

        // Start Communicate with database
        DB::beginTransaction();
        try{
            $addcountry = $this->country->addCountry($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('admin/country/add')->withInput();

        }
        if ($addcountry) {
            //Event::dispatch(new SendMail($addcountry));
            $request->session()->flash('alert-success', __('app.default_add_success',["module" => __('app.country')]));
            return redirect('admin/country/');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.country'),"action"=>__('app.add')]));
            return redirect('admin/country/add')->withInput();
        }
    }

    /**
     * Update the specified country in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(request $request)
    {
        $validations = $this->customeValidate($request->all(), 'edit');
        if ($validations) {
            return $validations;
        }

        // Start Communicate with database
        DB::beginTransaction();
        try{
            $addcountry = $this->country->addCountry($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('admin/country/edit/' . $request->get('id'))->withInput();

        }

        if ($addcountry) {

            $request->session()->flash('alert-success', __('app.default_edit_success',["module" => __('app.country')]));
            return redirect('admin/country');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.country'),"action"=>__('app.update')]));
            return redirect('admin/country/edit/' . $request->get('id'))->withInput();
        }
    }

    /**
     * Delete the specified country in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function delete(request $request)
    {
        $deleteCountry = $this->country->deleteCountry($request->id);
        if ($deleteCountry) {
            $request->session()->flash('alert-success', __('app.default_delete_success',["module" => __('app.country')]));
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.country'),"action"=>__('app.delete')]));
        }
        echo 1;
    }

    /**
     * Update status to the specified country in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(request $request)
    {
        $updateCountry = $this->country->updateStatus($request->all());
        if ($updateCountry) {
            $request->session()->flash('alert-success', __('app.default_status_success',["module" => __('app.country')]));
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.country'),"action"=>__('app.change_status')]));
        }
        echo 1;
    }

    /**
     * Get state by country
     *
     * @return \Illuminate\Http\Response
     */
    public function getState(Request $request)
    {
        $country_id = $request->country_id;
        $field_name = 'country_id';
        $dataList = $this->state->getCollectionByCounty($field_name,$country_id);
        $stateArray = '<option value="">Select State</option>';
        if(!empty($dataList)){
            foreach ($dataList as $state) {
                if($request->state_id == $state->id){
                    $stateArray.= '<option value="' . $state->id . '" selected>' . $state->name . '</option>';
                }else{
                    $stateArray.= '<option value="' . $state->id . '">' . $state->name . '</option>';
                }

            }
        }
        return response()->json([
            'stateList' => $stateArray,
        ]);
    }

}
