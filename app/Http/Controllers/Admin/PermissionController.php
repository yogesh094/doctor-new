<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomPermission;
use DB;
use Validator;
use Mail;
use Auth;


class PermissionController extends Controller
{

    protected $permission;
    protected $role;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CustomPermission $permission)
    {
        $this->middleware('permission:permission-list|permission-create|permission-edit|permission-delete', ['only' => ['index','store']]);
        $this->middleware('permission:permission-create', ['only' => ['create','store']]);
        $this->middleware('permission:permission-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:permission-delete', ['only' => ['destroy']]);
        $this->permission = $permission;
    }

    /**
     * Display a listing of the permission.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /**
         * getCollection from App/Models/Permission
         *
         * @return mixed
         */
        $data['permissionData'] = $this->permission->getCollection();
        $data['userManagementTab'] = 'active open';
        $data['permissionTab'] = 'active';
        $data['module'] = 'permission';
        return view('admin.permission.list', $data);
    }

    public function datatable(Request $request)
    {
        // default count of permission $permissionCount
        $permissionCount = 0;

        /**
         * getDatatableCollection from App/Models/Permission
         * get all permissions
         *
         * @return mixed
         */
        $permissionData = $this->permission->getDatatableCollection();

        /**
         * scopeGetFilteredData from App/Models/Permission
         * get filterred permissions
         *
         * @return mixed
         */
        $permissionData = $permissionData->GetFilteredData($request);

        /**
         * getPermissionCount from App/Models/Permission
         * get count of permissions
         *
         * @return integer
         */
        $permissionCount = $this->permission->getPermissionCount($permissionData);

        // Sorting permission data base on requested sort order
        if (isset(config('constant.permissionDataTableFieldArray')[$request->order['0']['column']])) {
            $permissionData = $permissionData->SortPermissionData($request);
        } else {
            $permissionData = $permissionData->SortDefaultDataByRaw('permissions.id', 'desc');
        }

        /**
         * get paginated collection of permission
         *
         * @param  \Illuminate\Http\Request $request
         * @return mixed
         */
        $permissionData = $permissionData->GetPermissionData($request);

        $appData = array();
        foreach ($permissionData as $permissionData) {
            $row = array();
            $row[] = $permissionData->model_name;
            $row[] = $permissionData->model_slug;
            $row[] = $permissionData->name;
            $row[] = view('datatable.action', ['module' => "permission",'url' => 'admin/permission','id' => $permissionData->id])->render();
            $appData[] = $row;
        }

        return [
            'draw' => $request->draw,
            'recordsTotal' => $permissionCount,
            'recordsFiltered' => $permissionCount,
            'data' => $appData,
        ];
    }

    /**
     * Show the form for creating a new permission.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['permissionTab'] = 'active open';
        $data['userManagementTab'] = 'active open';
        return view('admin.permission.add',$data);
    }

    /**
     * Display the specified permission.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        /**
         * get details of the specified permission. from App/Models/Permission
         *
         * @param mixed $id
         * @param string (id) fieldname
         * @return mixed
         */
        $data['details'] = $this->permission->getPermissionByField($id, 'id');
        $data['userManagementTab'] = 'active open';
        $data['permissionTab'] = 'active';
        return view('admin.permission.edit', $data);
    }

    /**
     * Validation of add and edit action customeValidate
     *
     * @param array $data
     * @param string $mode
     * @return mixed
     */
    public function customeValidate($data, $mode)
    {
        $rules = array(
            'model_name' => 'required|max:50',
        );
        if($mode == 'edit') {
            $rules = array(
                'model_name' => 'required|max:50',
            );
        }

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $errorRedirectUrl = "admin/permission/add";
            if ($mode == "edit") {
                $errorRedirectUrl = "admin/permission/edit/" . $data['id'];
            }
            return redirect($errorRedirectUrl)->withInput()->withErrors($validator);
        }
        return false;
    }

    /**
     * Store a newly created permission in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {

        $validations = $this->customeValidate($request->all(), 'add');
        if ($validations) {
            return $validations;
        }

        // Start Communicate with database
        DB::beginTransaction();
        try{
            $addpermission = $this->permission->addPermission($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('admin/permission/add')->withInput();

        }
        if ($addpermission) {
            //Event::dispatch(new SendMail($addpermission));
            $request->session()->flash('alert-success', __('app.default_add_success',["module" => __('app.permission')]));
            return redirect('admin/permission/');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.permission'),"action"=>__('app.add')]));
            return redirect('admin/permission/add')->withInput();
        }
    }

    /**
     * Update the specified permission in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(request $request)
    {
        $validations = $this->customeValidate($request->all(), 'edit');
        if ($validations) {
            return $validations;
        }

        // Start Communicate with database
        DB::beginTransaction();
        try{
            $addpermission = $this->permission->addPermission($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('admin/permission/edit/' . $request->get('id'))->withInput();

        }

        if ($addpermission) {

            $request->session()->flash('alert-success', __('app.default_edit_success',["module" => __('app.permission')]));
            return redirect('admin/permission');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.permission'),"action"=>__('app.update')]));
            return redirect('admin/permission/edit/' . $request->get('id'))->withInput();
        }
    }

    /**
     * Update status to the specified permission in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(request $request)
    {
        $updatePermission = $this->permission->updateStatus($request->all());
        if ($updatePermission) {
            $request->session()->flash('alert-success', __('app.default_status_success',["module" => __('app.permission')]));
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.permission'),"action"=>__('app.change_status')]));
        }
        echo 1;
    }

    /**
     * Delete the specified permission in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function delete(request $request)
    {
        $deletePermission = $this->permission->deletePermission($request->id);
        if ($deletePermission) {
            $request->session()->flash('alert-success', __('app.default_delete_success',["module" => __('app.permission')]));
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.permission'),"action"=>__('app.delete')]));
        }
        echo 1;
    }

}
