<?php

namespace App\Http\Controllers\Admin;

use App\Models\AdminUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    protected $adminUser;
    /**
     * AdminUserController constructor.
     */

    public function __construct(AdminUser $adminUser)
    {
        $this->adminUser = $adminUser;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['homeTab'] = 'active open';
        // $data['details'] = $this->adminUser->dashboard();
        return view('admin.dashboard',$data);
    }
}
