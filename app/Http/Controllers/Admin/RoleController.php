<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\CustomRole;
use App\Models\CustomPermission;
use DB;
use Validator;
use Mail;
use Auth;
use Carbon\Carbon;

class RoleController extends Controller
{

    protected $role;
    protected $permission;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CustomRole $role,CustomPermission $permission)
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        $this->role = $role;
        $this->permission = $permission;
    }

    /**
     * Display a listing of the role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /**
         * getCollection from App/Models/Role
         *
         * @return mixed
         */
        $data['roleData'] = $this->role->getCollection();
        $data['userManagementTab'] = 'active open';
        $data['roleTab'] = 'active';
        $data['module'] = 'role';
        return view('admin.role.list', $data);
    }

    public function datatable(Request $request)
    {
        // default count of role $roleCount
        $roleCount = 0;

        /**
         * getDatatableCollection from App/Models/Role
         * get all roles
         *
         * @return mixed
         */
        $roleData = $this->role->getDatatableCollection();

        /**
         * scopeGetFilteredData from App/Models/Role
         * get filterred roles
         *
         * @return mixed
         */
        $roleData = $roleData->GetFilteredData($request);

        /**
         * getRoleCount from App/Models/Role
         * get count of roles
         *
         * @return integer
         */
        $roleCount = $this->role->getRoleCount($roleData);

        // Sorting role data base on requested sort order
        if (isset(config('constant.roleDataTableFieldArray')[$request->order['0']['column']])) {
            $roleData = $roleData->SortRoleData($request);
        } else {
            $roleData = $roleData->SortDefaultDataByRaw('roles.id', 'desc');
        }

        /**
         * get paginated collection of role
         *
         * @param  \Illuminate\Http\Request $request
         * @return mixed
         */
        $roleData = $roleData->GetRoleData($request);

        $appData = array();
        foreach ($roleData as $roleData) {
            $row = array();
            $row[] = $roleData->name;
            $row[] = view('datatable.action', ['module' => "role",'url' => 'admin/role','id' => $roleData->id])->render();
            $appData[] = $row;
        }

        return [
            'draw' => $request->draw,
            'recordsTotal' => $roleCount,
            'recordsFiltered' => $roleCount,
            'data' => $appData,
        ];
    }

    /**
     * Show the form for creating a new role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['permission'] = $this->permission->getCollection();
        $roleModelArr = [];
        if(isset($data['permission'])){
            foreach ($data['permission'] as  $value){
                if(!in_array($value->model_name,$roleModelArr)){
                    $roleModelArr[$value->model_name][$value->id] = $value->name;
                }else{
                    $roleModelArr[$value->model_name][$value->id] = $value->name;
                }
            }
        }
        $data['permissionList'] = $roleModelArr;
        $data['userManagementTab'] = 'active open';
        $data['roleTab'] = 'active open';
        return view('admin.role.add',$data);
    }

    /**
     * Display the specified role.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        /**
         * get details of the specified role. from App/Models/Role
         *
         * @param mixed $id
         * @param string (id) fieldname
         * @return mixed
         */
        $data['details'] = $this->role->getRoleByField($id,'id');
        $data['permission'] = $this->permission->getCollection();
        $data['rolePermissions'] = $this->role->getRolePermissions($id);
        $roleModelArr = [];
        if(isset($data['permission'])){
            foreach ($data['permission'] as  $value){
                if(!in_array($value->model_name,$roleModelArr)){
                    $roleModelArr[$value->model_name][$value->id] = $value->name;
                }else{
                    $roleModelArr[$value->model_name][$value->id] = $value->name;
                }
            }
        }
        $data['userManagementTab'] = 'active open';
        $data['roleTab'] = 'active open';
        $data['permissionList'] = $roleModelArr;
        return view('admin.role.edit', $data);
    }

    /**
     * Validation of add and edit action customeValidate
     *
     * @param array $data
     * @param string $mode
     * @return mixed
     */
    public function customeValidate($data, $mode)
    {
        $rules = array(
            'name' => 'required|max:50',
        );
        if($mode == 'edit') {
            $rules = array(
                'name' => 'required|max:50',
            );
        }

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $errorRedirectUrl = "admin/role/add";
            if ($mode == "edit") {
                $errorRedirectUrl = "admin/role/edit/" . $data['id'];
            }
            return redirect($errorRedirectUrl)->withInput()->withErrors($validator);
        }
        return false;
    }

    /**
     * Store a newly created role in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {

        $validations = $this->customeValidate($request->all(), 'add');
        if ($validations) {
            return $validations;
        }
        DB::beginTransaction();
        try{
            $addrole = $this->role->addRole($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('admin/role/add')->withInput();

        }
        if ($addrole) {
            //Event::dispatch(new SendMail($addrole));
            $request->session()->flash('alert-success', __('app.default_add_success',["module" => __('app.role')]));
            return redirect('admin/role/');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.role'),"action"=>__('app.add')]));
            return redirect('admin/role/add')->withInput();
        }
    }

    /**
     * Update the specified role in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(request $request)
    {
        $validations = $this->customeValidate($request->all(), 'edit');
        if ($validations) {
            return $validations;
        }

        // Start Communicate with database
        DB::beginTransaction();
        try{
            $addrole = $this->role->addRole($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('admin/role/edit/' . $request->get('id'))->withInput();

        }

        if ($addrole) {

            $request->session()->flash('alert-success', __('app.default_edit_success',["module" => __('app.role')]));
            return redirect('admin/role');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.role'),"action"=>__('app.update')]));
            return redirect('admin/role/edit/' . $request->get('id'))->withInput();
        }
    }

    /**
     * Delete the specified role in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function delete(request $request)
    {
        $deleteRole = $this->role->deleteRole($request->id);
        if ($deleteRole) {
            $request->session()->flash('alert-success', __('app.default_delete_success',["module" => __('app.role')]));
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.role'),"action"=>__('app.delete')]));
        }
        echo 1;
    }

}