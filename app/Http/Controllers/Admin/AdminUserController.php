<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdminUser;
use App\Models\State;
use App\Models\Country;
use Spatie\Permission\Models\Role;
use DB;
use Validator;
use Mail;
use Auth;
use Carbon\Carbon;


class AdminUserController extends Controller
{

    protected $adminUser;
    /**
     * AdminUserController constructor.
     */

    public function __construct(AdminUser $adminUser,Role $role)
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','store']]);
//        $this->middleware('permission:user-list');
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->adminUser = $adminUser;
        $this->role = $role;

    }

    /**
     * Validation of add and edit action custom Validate
     *
     * @param array $data
     * @param string $mode
     * @return mixed
     */
    public function customeValidate($data, $mode)
    {
        $rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:tbl_admin,email',
            'username' => 'required|unique:tbl_admin,username',
            'password' => 'required',
            'roles' => 'required'
        );

        if ($mode == "edit") {
            $rules = array(
                'name' => 'required',
                'email' => 'required|email|unique:tbl_admin,email,'.$data['id'],
                'username' => 'required|unique:tbl_admin,username,'.$data['id'],
                'roles' => 'required'
            );
        }

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $errorRedirectUrl = "admin/user/add";
            if ($mode == "edit") {
                $errorRedirectUrl = "admin/user/edit/" . $data['id'];
            }
            return redirect($errorRedirectUrl)->withInput()->withErrors($validator);
        }
        return false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * getCollection from App/Models/User
         *
         * @return mixed
         */
        $data['userData'] = $this->adminUser->getCollection();
        $data['userManagementTab'] = 'active open';
        $data['userTab'] = 'active';
        $data['module'] = 'user';
        return view('admin.users.list', $data);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function datatable(Request $request)
    {
        // default count of user $userCount
        $userCount = 0;

        /**
         * getDatatableCollection from App/Models/User
         * get all users
         *
         * @return mixed
         */
        $userData = $this->adminUser->getDatatableCollection();

        /**
         * scopeGetFilteredData from App/Models/User
         * get filterred users
         *
         * @return mixed
         */
        $userData = $userData->GetFilteredData($request);

        /**
         * getUserCount from App/Models/User
         * get count of users
         *
         * @return integer
         */
        $userCount = $this->adminUser->getUserCount($userData);

        // Sorting user data base on requested sort order
        if (isset(config('constant.userDataTableFieldArray')[$request->order['0']['column']])) {
            $userData = $userData->SortUserData($request);
        } else {
            $userData = $userData->SortDefaultDataByRaw('tbl_admin.id', 'desc');
        }

        /**
         * get paginated collection of user
         *
         * @param  \Illuminate\Http\Request $request
         * @return mixed
         */
        $userData = $userData->GetUserData($request);

        $appData = array();
        foreach ($userData as $userData) {
            $row = array();
            $row[] = $userData->name;
            $row[] = $userData->email;
            //$row[] = ($userData->code) ? $userData->code : "---";
            $row[] = view('datatable.switch', ['module' => "user", 'url' => 'admin/user','status' => $userData->status, 'id' => $userData->id])->render();
            $row[] = view('datatable.action', ['module' => "user",'url' => 'admin/user', 'id' => $userData->id])->render();
            $appData[] = $row;
        }

        return [
            'draw' => $request->draw,
            'recordsTotal' => $userCount,
            'recordsFiltered' => $userCount,
            'data' => $appData,
        ];
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['rolesData'] = $this->adminUser->getRoles();
        $data['userManagementTab'] = 'active open';
        $data['userTab'] = 'active open';
        return view('admin.users.add',$data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validations = $this->customeValidate($request->all(), 'add');
        if ($validations) {
            return $validations;
        }

        // Start Communicate with database
        DB::beginTransaction();
        try {
            $addAdminUser = $this->adminUser->addAdminUser($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('admin/user/add')->withInput();
        }
        if ($addAdminUser) {
            $request->session()->flash('alert-success', __('app.default_add_success', ["module" => __('app.admin_user')]));
            return redirect('admin/user');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error', ["module" => __('app.admin_user'), "action" => __('app.add')]));
            return redirect('admin/user/add')->withInput();
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['details'] = $this->adminUser->getUserByField($id,'id');
        $data['roles'] = $this->adminUser->getRoles();
        $data['userRole'] = $this->adminUser->getRoleById($id);
        $data['userManagementTab'] = 'active open';
        $data['userTab'] = 'active';
        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validations = $this->customeValidate($request->all(), 'edit');
        if ($validations) {
            return $validations;
        }
        // Start Communicate with database
        DB::beginTransaction();
        try{
            $addUser = $this->adminUser->addAdminUser($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('admin/user/edit/' . $request->get('id'))->withInput();

        }

        if ($addUser) {
            $request->session()->flash('alert-success', __('app.default_edit_success',["module" => __('app.adminUser')]));
            return redirect('admin/user/');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.mockexam'),"action"=>__('app.update')]));
            return redirect('admin/user/edit/' .$request->get('id'))->withInput();
        }
    }


    /**
     * Update status to the specified user in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(request $request)
    {
        $updateUser = $this->adminUser->updateStatus($request->all());
        if ($updateUser) {
            $request->session()->flash('alert-success', __('app.default_status_success',["module" => __('app.user')]));
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.user'),"action"=>__('app.change_status')]));
        }
        echo 1;
    }

    /**
     * Delete the specified user in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function delete(request $request)
    {
        $deleteUser = $this->adminUser->deleteUser($request->id);
        if ($deleteUser) {
            $request->session()->flash('alert-success', __('app.default_delete_success',["module" => __('app.user')]));
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.user'),"action"=>__('app.delete')]));
        }
        echo 1;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */

    function downloadfile(Request $request)
    {
        $file_name =  $request->file_name;
        $path = storage_path('app/'.$file_name);
        return response()->download($path);
    }
}