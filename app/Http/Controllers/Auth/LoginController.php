<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Redirect;
use Validator;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {      
       return ['email' => $request->email, 'password' => $request->password];
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @desc Display the login form for super admin
     */
    public function showAdminLoginForm()
    {
        return view('auth.adminlogin', ['url' => 'admin']);
    }

        /**
     * @param Request $request
     * @return Redirect
     * @desc Check super admin login credentials valid or not
     */
    public function adminLogin(Request $request)
    {
        $rules = [
            'login_name' => 'required|max:50|unique:tbl_institute_admin',
            'password' => 'required|string|min:8',

        ];
        $username = $request->login_name; //the input field has name='login_name' in form

        Validator::make($request->all(), $rules);
        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
            //user sent their email
            if (Auth::guard('admin')->attempt(['email' =>$username, 'password' => $request->password, 'status' => 1], $request->get('remember'))) {
                return redirect('/admin/dashboard');
            }
        } else {
            //they sent their username instead
            if (Auth::guard('admin')->attempt(['username' => $username, 'password' => $request->password, 'status' => 1], $request->get('remember'))) {
               // Log::info("Admin : ".Session::getId());
                return redirect('/admin/dashboard');
            }
        }

        Session::flash('message', "Invalid Credentials");
        return Redirect::back()->withInput($request->only('email', 'remember'));
    }

    /**
     * @return Redirect
     * Logout the super admin
     */
    public function adminLogout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
}
