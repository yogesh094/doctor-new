<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\Models\User;
use Session;
use Redirect;
use Auth;
use URL;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    //  /**
    //  * Show the email verification notice.
    //  *
    //  */
    // public function show(Request $request)
    // {
    //     if (count($request->all())) {
    //         if ($request->user('users')->hasVerifiedEmail()) {
    //             return redirect('/login');
    //         }
    //     }
    //     return view('auth.verify');
    // }

    // /**
    //  * Mark the authenticated user’s email address as verified.
    //  *
    //  * @param \Illuminate\Http\Request $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function verify(Request $request) {

    //     $userID = $request['id'];
    //     $user = User::findOrFail($userID);
    //     $date = date('Y-m-d g:i:s');
    //     $user->email_verified_at = $date; // to enable the “email_verified_at field of that user be a current time stamp by mimicing the must verify email feature
    //     $user->save();

    //     Auth::guard('users')->loginUsingId($userID);
    //     return redirect('/dashboard');
    // }
    // /**
    //  * Resend the email verification notification.
    //  *
    //  * @param \Illuminate\Http\Request $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function resend(Request $request)
    // {
    //     if (count($request->all())) {
    //         if ($request->user('users')->hasVerifiedEmail()) {
    //             // return response()->json('User already have verified email!', 422);
    //             return redirect('/login');
    //         }
    //         $request->user('users')->sendEmailVerificationNotification();
    //         Session::flash('message', "Email has been sent to your registered email.");
    //         return redirect('/email/verify');
    //     }
    // }
}
