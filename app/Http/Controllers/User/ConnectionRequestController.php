<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ConnectionRequest;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\Notification;
use DB;
use Validator;
use Mail;
use Auth;
use Carbon\Carbon;

class ConnectionRequestController extends Controller
{

    protected $connectionrequest;
    protected $user;
    protected $notification;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user,ConnectionRequest $connectionrequest,Notification $notification)
    {
        $this->connectionrequest = $connectionrequest;
        $this->user = $user;
        $this->notification = $notification;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $my_friends = $data['my_friends'] = $this->connectionrequest->getMyFriends();
        $pending_request = $data['pending_request'] = $this->connectionrequest->getPendingRequest();
        if(isset($my_friends)){
            $data['my_friends_total'] = $my_friends->total();
            $data['my_friends_perPage'] = $my_friends->perPage();
            $data['my_friends_currentPage'] = $my_friends->currentPage();
            $data['my_friends_lastPage'] = $my_friends->lastPage();
        }
        if(isset($pending_request)){
            $data['pending_request_total'] = $pending_request->total();
            $data['pending_request_perPage'] = $pending_request->perPage();
            $data['pending_request_currentPage'] = $pending_request->currentPage();
            $data['pending_request_lastPage'] = $pending_request->lastPage();
        }
        return view('user.profile.friend',$data);
    }

    public function sendRequest($request_to){
        if(!empty($request_to)){
            $request['request_to'] = $request_to;
            $request['request_from'] = Auth::user()->id;
            DB::beginTransaction();
              try{
                  $connectionrequest = $this->connectionrequest->addConnectionRequest($request);
                  DB::commit();
              } catch (\Exception $e) {
                  //exception handling
                  DB::rollback();
                  $errorMessage = $e->getMessage();
                  $data['status'] = 401;
                  $data['msg'] = $errorMessage;
                  return $data;
              }
              if($connectionrequest){
                $data['status'] = 200;
                $data['request_id'] = $connectionrequest->id;
                $data['msg'] = 'Sent Successfully.';
                return $data;
              }else{
                $data['status'] = 401;
                $data['msg'] = 'Something went wrong. Please contact to support team';
                return $data;
              }
        }else{
            $data['status'] = 401;
            $data['msg'] = 'Something went wrong. Please contact to support team';
            return $data;
        }
    }

    public function cancelRequest($request_id){

        if(!empty($request_id)){
            $where['id'] = $request_id;
            $update['type'] = 2;
            DB::beginTransaction();
              try{
                  $connectionrequest = $this->connectionrequest->updateData($where,$update);
                  DB::commit();
                  $requestData = $this->connectionrequest->getConnectionRequestByField($request_id,'id');
                  if(isset($requestData)){
                      $request['user_from'] = $requestData->request_to;
                      $request['user_to'] = $requestData->request_from;
                      $request['description'] = Auth::user()->name. " cancel the request";
                      $notification = $this->notification->addNotification($request);
                  }
              } catch (\Exception $e) {
                  //exception handling
                  DB::rollback();
                  $errorMessage = $e->getMessage();
                  $data['status'] = 401;
                  $data['msg'] = $errorMessage;
                  return $data;
              }
              if($connectionrequest){
                $data['status'] = 200;
                $data['msg'] = 'Cancel Successfully.';
                return $data;
              }else{
                $data['status'] = 401;
                $data['msg'] = 'Something went wrong. Please contact to support team';
                return $data;
              }
        }else{
            $data['status'] = 401;
            $data['msg'] = 'Something went wrong. Please contact to support team';
            return $data;
        }
    }

    public function confirmRequest($request_id){

        if(!empty($request_id)){
            $where['id'] = $request_id;
            $update['type'] = 1;
            DB::beginTransaction();
              try{
                  $connectionrequest = $this->connectionrequest->updateData($where,$update);
                  DB::commit();
                  $requestData = $this->connectionrequest->getConnectionRequestByField($request_id,'id');
                  if(isset($requestData)){
                      $request['user_from'] = $requestData->request_to;
                      $request['user_to'] = $requestData->request_from;
                      $request['description'] = Auth::user()->name. " accepted the request";
                      $notification = $this->notification->addNotification($request);
                  }
              } catch (\Exception $e) {
                  //exception handling
                  DB::rollback();
                  $errorMessage = $e->getMessage();
                  $data['status'] = 401;
                  $data['msg'] = $errorMessage;
                  return $data;
              }
              if($connectionrequest){
                $data['status'] = 200;
                $data['msg'] = 'Accepted Successfully.';
                return $data;
              }else{
                $data['status'] = 401;
                $data['msg'] = 'Something went wrong. Please contact to support team';
                return $data;
              }
        }else{
            $data['status'] = 401;
            $data['msg'] = 'Something went wrong. Please contact to support team';
            return $data;
        }
    }

    public function moreFriend(Request $request){
        $my_friends = $data['my_friends'] = $this->connectionrequest->getMyFriends();
        if(isset($my_friends)){
            $data['my_friends_total'] = $my_friends->total();
            $data['my_friends_perPage'] = $my_friends->perPage();
            $data['my_friends_currentPage'] = $my_friends->currentPage();
            $data['my_friends_lastPage'] = $my_friends->lastPage();
        }
        $post_html = '';
        if(isset($my_friends) && count($my_friends) > 0){
            foreach($my_friends as $key => $value){
                
                if($value->request_to == Auth::user()->id){
                    $friend_id = $value->request_from;
                }else{
                    $friend_id = $value->request_to;
                }
                $userData = getNameById($friend_id);
                $userType = getProfileTypeName(Auth::user()->profile_type);

                if(!empty($userData->profile_image) && $userData->profile_image != ''){
                    $image_url = config('services.upload_url').'/image/user/'.$userData->profile_image;
                }else{
                    $image_url = url('images/frontend-image/resources/user-avatar.jpg');
                }
               
                $post_html .= '<li>
                <div class="nearly-pepls">
                        <figure>
                          <a href="'.url('profile/'.$userData->username).'" title="">
                              <img src="'.$image_url.'">
                          </a>
                        </figure>
                        <div class="pepl-info">
                            <h4><a href="'.url('profile/'.$userData->username).'" title="">'.$userData->name.'</a></h4>
                            <span>'.$userType.'</span>
                            <a href="#" title="Unfriend" class="add-butn" onclick="cancelRequest(this)" data-id="'.$value->id.'" data-to="'.$friend_id.'" data-type="1">Unfriend</a>
                        </div>
                    </div>
                </li>';
            }
          }
        return response()->json([
            'message' => 'sucess',
            'status' => 200,
            'post_html' => $post_html,
            'next_page' => $data['my_friends_currentPage'] + 1,
            'last_page' => $data['my_friends_lastPage']
        ]);
    }

    public function morePendingRequest(Request $request){

        $pending_request = $data['pending_request'] = $this->connectionrequest->getPendingRequest();
        if(isset($pending_request)){
            $data['pending_request_total'] = $pending_request->total();
            $data['pending_request_perPage'] = $pending_request->perPage();
            $data['pending_request_currentPage'] = $pending_request->currentPage();
            $data['pending_request_lastPage'] = $pending_request->lastPage();
        }
        $post_html = '';
        if(isset($pending_request) && count($pending_request) > 0){
            foreach($pending_request as $key => $value){
                
                if($value->request_to == Auth::user()->id){
                    $friend_id = $value->request_from;
                }else{
                    $friend_id = $value->request_to;
                }
                $userData = getNameById($friend_id);
                $userType = getProfileTypeName(Auth::user()->profile_type);

                if(!empty($userData->profile_image) && $userData->profile_image != ''){
                    $image_url = config('services.upload_url').'/image/user/'.$userData->profile_image;
                }else{
                    $image_url = url('images/frontend-image/resources/user-avatar.jpg');
                }
               
                $post_html .= '<li>
                <div class="nearly-pepls">
                        <figure>
                          <a href="'.url('profile/'.$userData->username).'" title="">
                              <img src="'.$image_url.'">
                          </a>
                        </figure>
                        <div class="pepl-info">
                            <h4><a href="'.url('profile/'.$userData->username).'" title="">'.$userData->name.'</a></h4>
                            <span>'.$userType.'</span>
                            <a href="#" title="Delete Request" class="add-butn more-action" onclick="cancelRequest(this)" data-id="'.$value->id.'" data-to="'.$friend_id.'" data-type="2">Delete Request</a>
                            <a href="#" title="Confirm Request" class="add-butn" onclick="confirmRequest(this)" data-id="'.$value->id.'" data-to="'.$friend_id.'" data-type="2">Confirm</a>
                        </div>
                    </div>
                </li>';
            }
          }
        return response()->json([
            'message' => 'sucess',
            'status' => 200,
            'post_html' => $post_html,
            'next_page' => $data['pending_request_currentPage'] + 1,
            'last_page' => $data['pending_request_lastPage']
        ]);
    }
}
