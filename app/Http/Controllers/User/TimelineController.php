<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostReply;
use App\Models\PostLike;
use App\Models\ConnectionRequest;
use App\Models\Country;
use DB;
use Auth;

class TimelineController extends Controller
{
    protected $user;
    protected $post;
    protected $comment;
    protected $reply;
    protected $like;
    protected $connectionrequest;
    protected $country;
    /**
     * AdminUserController constructor.
     */

    public function __construct(User $user,Post $post,PostComment $comment, PostReply $reply,PostLike $like,ConnectionRequest $connnectionrequest,Country $country)
    {
        $this->user = $user;
        $this->post = $post;
        $this->comment = $comment;
        $this->reply = $reply;
        $this->like = $like;
        $this->country = $country;
        $this->connnectionrequest = $connnectionrequest;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($username)
    {
        $content = [];
        $newsfeed_data = [];
        $data['homeTab'] = 'active open';
        $userData = $this->user->getUserByField($username,'username');
        $login_user_id = $user_id = Auth::user()->id;
        $post_list = $this->post->getPostList($userData->id);
        $like_count = $dislike_count = $is_like = 0;
        if(count($post_list) > 0){
            $newsfeed_data['total'] = $post_list->total();
            $newsfeed_data['perPage'] = $post_list->perPage();
            $newsfeed_data['currentPage'] = $post_list->currentPage();
            $newsfeed_data['lastPage'] = $post_list->lastPage();
            $comment_html = '';
            $flag = 0;
            foreach ($post_list as $post_k => $post_v){
                $post_html = '';
                $comment_html = '';
                $post_id = $post_v->id;
                $comment_count = $this->comment->getCommentCountByPostId($post_v->id);
                if($comment_count > 0){
                    $comment_data = $this->comment->getCommentByPostId($post_v->id);
                    $post_data['comment_data']['details'] = $comment_data;
                    $post_data['comment_data']['total'] = $comment_data->total();
                    $post_data['comment_data']['perPage'] = $comment_data->perPage();
                    $post_data['comment_data']['currentPage'] = $comment_data->currentPage();
                    $post_data['comment_data']['lastPage'] = $comment_data->lastPage();
                    foreach ($comment_data as $comment_k => $comment_v){
                        $reply_html= '';
                        $reply_count_on_comment = $this->reply->getReplyCountByCommentId($comment_v->id);
                        if($reply_count_on_comment > 0){
                            $reply_data = $this->reply->getReplyByCommentId($comment_v->id);
                            $post_data['comment_data']['reply']['details'] = $reply_data;
                            $reply_html = view('user.blocks.reply',compact('reply_data','login_user_id'))->render();
                            $post_data['comment_data']['reply']['total'] = $reply_data->total();
                            $post_data['comment_data']['reply']['perPage'] = $reply_data->perPage();
                            $post_data['comment_data']['reply']['currentPage'] = $reply_data->currentPage();
                            $post_data['comment_data']['reply']['lastPage'] = $reply_data->lastPage();

                        }
                        $comment_html.= view('user.blocks.comment',compact('comment_v','reply_html','post_id','post_data','login_user_id'))->render();
                    }
                }
                $reply_count = $this->reply->getReplyCountByPostId($post_v->id);
                $like_count = $this->like->getLikeCountByPostId($post_v->id);
                $dislike_count = $this->like->getDislikeCountByPostId($post_v->id);
                $is_like = $this->like->checkUserStatus($user_id,$post_v->id);
                $post_data['details'] = $post_v;
                $comment_count = $comment_count + $reply_count;
                if(!empty($is_like)){
                    $is_like = $is_like->status;
                }else{
                    $is_like = 0;
                }
                $postadd_html = '';
                if($flag == 0){
                    $postadd_html = view('user.blocks.postadd')->render();
                }
                $post_html = view('user.blocks.post',compact('post_v','comment_html','flag','postadd_html','post_id','is_like'
                    ,'comment_count','like_count','dislike_count','post_data','login_user_id'))->render();
                $flag = 1;
                $newsfeed_data['post_html'][] = $post_html;

            }
            $newsfeed_data['postadd_html'] = view('user.blocks.postadd')->render();
        }//posst
        $postadd_html = '';
        $postadd_html = view('user.blocks.postadd')->render();
        $newsfeed_data['postadd_html'] = view('user.blocks.postadd')->render();
        $recentComment = $this->comment->getRecentComment(Auth::user()->id);

        $connectionUser = $this->connnectionrequest->userConnection();
        $section = $this->user->getUsersForSidebars($connectionUser);

        $data['country'] = $this->country->getCollection();
        $userData = $this->user->getUserByField($username,'username');
        if(!empty($userData)){
            $profile_intro_html = view('user.blocks.profileintro',compact('userData'))->render();
            $newsfeed_data['profile_intro_html'] = $profile_intro_html;
            $newsfeed_data['details'] = $userData;
        }
        return view('user.profile.timeline',$newsfeed_data);
        //return view('user.blocks.postadd',$data);
    }

    public function morePost(Request $request){
        $content = [];
        $newsfeed_data = [];
        $username = $request->username;
        $data['homeTab'] = 'active open';
        $userData = $this->user->getUserByField($username,'username');
        $login_user_id = $user_id = Auth::user()->id;
        $post_list = $this->post->getPostList($userData->id);
        $like_count = $dislike_count = $is_like = 0;
        $post_html = '';
        if(count($post_list) > 0){
            $newsfeed_data['total'] = $post_list->total();
            $newsfeed_data['perPage'] = $post_list->perPage();
            $newsfeed_data['currentPage'] = $post_list->currentPage();
            $newsfeed_data['lastPage'] = $post_list->lastPage();
            $comment_html = '';

            $flag = 0;
            foreach ($post_list as $post_k => $post_v){
                $comment_html = '';
                $post_id = $post_v->id;
                $comment_count = $this->comment->getCommentCountByPostId($post_v->id);
                if($comment_count > 0){
                    $comment_data = $this->comment->getCommentByPostId($post_v->id);
                    $post_data['comment_data']['details'] = $comment_data;
                    $post_data['comment_data']['total'] = $comment_data->total();
                    $post_data['comment_data']['perPage'] = $comment_data->perPage();
                    $post_data['comment_data']['currentPage'] = $comment_data->currentPage();
                    $post_data['comment_data']['lastPage'] = $comment_data->lastPage();
                    foreach ($comment_data as $comment_k => $comment_v){
                        $reply_html= '';
                        $reply_count_on_comment = $this->reply->getReplyCountByCommentId($comment_v->id);
                        if($reply_count_on_comment > 0){
                            $reply_data = $this->reply->getReplyByCommentId($comment_v->id);
                            $post_data['comment_data']['reply']['details'] = $reply_data;
                            $reply_html = view('user.blocks.reply',compact('reply_data','login_user_id'))->render();
                            $post_data['comment_data']['reply']['total'] = $reply_data->total();
                            $post_data['comment_data']['reply']['perPage'] = $reply_data->perPage();
                            $post_data['comment_data']['reply']['currentPage'] = $reply_data->currentPage();
                            $post_data['comment_data']['reply']['lastPage'] = $reply_data->lastPage();

                        }
                        $comment_html.= view('user.blocks.comment',compact('comment_v','reply_html','post_id','post_data','login_user_id'))->render();
                    }
                }
                $reply_count = $this->reply->getReplyCountByPostId($post_v->id);
                $like_count = $this->like->getLikeCountByPostId($post_v->id);
                $dislike_count = $this->like->getDislikeCountByPostId($post_v->id);
                $is_like = $this->like->checkUserStatus($user_id,$post_v->id);
                $post_data['details'] = $post_v;
                $comment_count = $comment_count + $reply_count;
                if(!empty($is_like)){
                    $is_like = $is_like->status;
                }else{
                    $is_like = 0;
                }
                $postadd_html = '';
                if($flag == 0){
                    $postadd_html = view('user.blocks.postadd')->render();
                }
                $post_html .= view('user.blocks.post',compact('post_v','comment_html','flag','postadd_html','post_id','is_like'
                    ,'comment_count','like_count','dislike_count','post_data','login_user_id'))->render();
            }
        }//posst

        return response()->json([
            'message' => 'sucess',
            'status' => 200,
            'post_html' => $post_html,
            'next_page' => $newsfeed_data['currentPage'] + 1,
            'last_page' => $newsfeed_data['lastPage']
        ]);
    }
}
