<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Models\ConnectionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    protected $user;
    protected $connnectionrequest;
    /**
     * AdminUserController constructor.
     */

    public function __construct(User $user,ConnectionRequest $connnectionrequest)
    {
        $this->user = $user;
        $this->connnectionrequest = $connnectionrequest;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['homeTab'] = 'active open';
        $connectionUser = $this->connnectionrequest->userConnection();
        $data['userSection'] = $section = $this->user->getUsersForSidebars($connectionUser);
        $left_section_html = view('user.layouts.leftsection',compact('section'))->render();
        $data['left_section_html'] = $left_section_html;
        return view('user.dashboard',$data);
    }
}
