<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostReply;
use App\Models\PostLike;
use App\Models\ConnectionRequest;
use DB;
use Auth;
use Redirect;
use App\Models\Notification;

class PostController extends Controller
{

    protected $user;
    protected $post;
    protected $comment;
    protected $reply;
    protected $like;
    protected $connectionrequest;
    protected $notification;
    /**
     * AdminUserController constructor.
     */

    public function __construct(User $user,Post $post,PostComment $comment, PostReply $reply,PostLike $like,ConnectionRequest $connnectionrequest, Notification $notification)
    {
        $this->user = $user;
        $this->post = $post;
        $this->comment = $comment;
        $this->reply = $reply;
        $this->like = $like;
        $this->connnectionrequest = $connnectionrequest;
        $this->notification = $notification;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $content = [];
        $newsfeed_data = [];
        $data['homeTab'] = 'active open';
        $post_list = $this->post->getPostList();
        $login_user_id = $user_id = Auth::user()->id;
        $like_count = $dislike_count = $is_like = 0;
        if(count($post_list) > 0){
            $newsfeed_data['total'] = $post_list->total();
            $newsfeed_data['perPage'] = $post_list->perPage();
            $newsfeed_data['currentPage'] = $post_list->currentPage();
            $newsfeed_data['lastPage'] = $post_list->lastPage();
            $comment_html = '';
            $flag = 0;
            foreach ($post_list as $post_k => $post_v){
                $post_html = '';
                $comment_html = '';
                $post_id = $post_v->id;
                $comment_count = $this->comment->getCommentCountByPostId($post_v->id);
                if($comment_count > 0){
                    $comment_data = $this->comment->getCommentByPostId($post_v->id);
                    $post_data['comment_data']['details'] = $comment_data;
                    $post_data['comment_data']['total'] = $comment_data->total();
                    $post_data['comment_data']['perPage'] = $comment_data->perPage();
                    $post_data['comment_data']['currentPage'] = $comment_data->currentPage();
                    $post_data['comment_data']['lastPage'] = $comment_data->lastPage();
                    foreach ($comment_data as $comment_k => $comment_v){
                        $reply_html= '';
                        $reply_count_on_comment = $this->reply->getReplyCountByCommentId($comment_v->id);
                        if($reply_count_on_comment > 0){
                            $reply_data = $this->reply->getReplyByCommentId($comment_v->id);
                            $post_data['comment_data']['reply']['details'] = $reply_data;
                            $reply_html = view('user.blocks.reply',compact('reply_data','login_user_id'))->render();
                            $post_data['comment_data']['reply']['total'] = $reply_data->total();
                            $post_data['comment_data']['reply']['perPage'] = $reply_data->perPage();
                            $post_data['comment_data']['reply']['currentPage'] = $reply_data->currentPage();
                            $post_data['comment_data']['reply']['lastPage'] = $reply_data->lastPage();

                        }
                        $comment_html.= view('user.blocks.comment',compact('comment_v','reply_html','post_id','post_data','login_user_id'))->render();
                    }
                }
                $reply_count = $this->reply->getReplyCountByPostId($post_v->id);
                $like_count = $this->like->getLikeCountByPostId($post_v->id);
                $dislike_count = $this->like->getDislikeCountByPostId($post_v->id);
                $is_like = $this->like->checkUserStatus($user_id,$post_v->id);
                $post_data['details'] = $post_v;
                $comment_count = $comment_count + $reply_count;
                if(!empty($is_like)){
                    $is_like = $is_like->status;
                }else{
                    $is_like = 0;
                }
                $postadd_html = '';
                if($flag == 0){
                    $postadd_html = view('user.blocks.postadd')->render();
                }
                $post_html = view('user.blocks.post',compact('post_v','comment_html','flag','postadd_html','post_id','is_like'
                ,'comment_count','like_count','dislike_count','post_data','login_user_id'))->render();
                $flag = 1;
                $newsfeed_data['post_html'][] = $post_html;

            }
                $newsfeed_data['postadd_html'] = view('user.blocks.postadd')->render();
            }//posst
        $postadd_html = '';
        $postadd_html = view('user.blocks.postadd')->render();
        $newsfeed_data['postadd_html'] = view('user.blocks.postadd')->render();
        $recentComment = $this->comment->getRecentComment(Auth::user()->id);

        $connectionUser = $this->connnectionrequest->userConnection();
        $section = $this->user->getUsersForSidebars($connectionUser);

        $left_section_html = view('user.layouts.leftsection',compact('section','recentComment'))->render();
        $newsfeed_data['left_section_html'] = $left_section_html;
        return view('user.newsfeed',$newsfeed_data);
        //return view('user.blocks.postadd',$data);
    }

    public function store(Request $request){
        DB::beginTransaction();
          try{
              $post = $this->post->addPost($request->all());
              DB::commit();
          } catch (\Exception $e) {
              //exception handling
              DB::rollback();
              echo $errorMessage = $e->getMessage();
              exit;
              $request->session()->flash('msg', $errorMessage);
              return redirect('/news-feed')->withInput();
          }

          if ($post) {
            $request->session()->flash('msg', 'Post is added successfully');
            return Redirect::back();
          } else {
            $request->session()->flash('msg', 'Something went wrong');
            return Redirect::back();
          }
        return view('user.newsfeed',$data);
    }

    public function edit(Request $request){
        $details = $this->post->getPostByField($request->id,'id');
        if ($details) {
          return $details;
        } else {
          return false;
        }
    }

    public function deletePost(Request $request){
        $deletePost = $this->post->deletePost($request->id);
        if ($deletePost) {
          $data['status'] = 200;
          $request->session()->flash('alert-success', __('app.default_delete_success',["module" => __('app.post')]));
          $data['msg'] = __('app.default_delete_success',["module" => __('app.post')]);
          return $data;
        } else {
          $data['status'] = 401;
          $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.post'),"action"=>__('app.delete')]));
          $data['msg'] = __('app.default_error',["module" => __('app.post'),"action"=>__('app.delete')]);
          return $data;
        }
      }


    public function addPostLike(Request $request){

        $requestData = $request->all();
        $requestData['user_id'] = Auth::user()->id;
        $requestData['post_id'] = $request->post_id;
        $requestData['status'] = $request->is_like;
        DB::beginTransaction();
        try{
            $post = $this->like->addPostLike($requestData);
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
        if ($post) {
            return response()->json([
                'message' => 'sucess',
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
    }

    public function morePost(Request $request){
        $content = [];
        $newsfeed_data = [];
        $data['homeTab'] = 'active open';
        $post_list = $this->post->getPostList();
        $login_user_id = $user_id = Auth::user()->id;
        $like_count = $dislike_count = $is_like = 0;
        $post_html = '';
        if(count($post_list) > 0){
            $newsfeed_data['total'] = $post_list->total();
            $newsfeed_data['perPage'] = $post_list->perPage();
            $newsfeed_data['currentPage'] = $post_list->currentPage();
            $newsfeed_data['lastPage'] = $post_list->lastPage();
            $comment_html = '';

            $flag = 0;
            foreach ($post_list as $post_k => $post_v){
                $comment_html = '';
                $post_id = $post_v->id;
                $comment_count = $this->comment->getCommentCountByPostId($post_v->id);
                if($comment_count > 0){
                    $comment_data = $this->comment->getCommentByPostId($post_v->id);
                    $post_data['comment_data']['details'] = $comment_data;
                    $post_data['comment_data']['total'] = $comment_data->total();
                    $post_data['comment_data']['perPage'] = $comment_data->perPage();
                    $post_data['comment_data']['currentPage'] = $comment_data->currentPage();
                    $post_data['comment_data']['lastPage'] = $comment_data->lastPage();
                    foreach ($comment_data as $comment_k => $comment_v){
                        $reply_html= '';
                        $reply_count_on_comment = $this->reply->getReplyCountByCommentId($comment_v->id);
                        if($reply_count_on_comment > 0){
                            $reply_data = $this->reply->getReplyByCommentId($comment_v->id);
                            $post_data['comment_data']['reply']['details'] = $reply_data;
                            $reply_html = view('user.blocks.reply',compact('reply_data','login_user_id'))->render();
                            $post_data['comment_data']['reply']['total'] = $reply_data->total();
                            $post_data['comment_data']['reply']['perPage'] = $reply_data->perPage();
                            $post_data['comment_data']['reply']['currentPage'] = $reply_data->currentPage();
                            $post_data['comment_data']['reply']['lastPage'] = $reply_data->lastPage();

                        }
                        $comment_html.= view('user.blocks.comment',compact('comment_v','reply_html','post_id','post_data','login_user_id'))->render();
                    }
                }
                $reply_count = $this->reply->getReplyCountByPostId($post_v->id);
                $like_count = $this->like->getLikeCountByPostId($post_v->id);
                $dislike_count = $this->like->getDislikeCountByPostId($post_v->id);
                $is_like = $this->like->checkUserStatus($user_id,$post_v->id);
                $post_data['details'] = $post_v;
                $comment_count = $comment_count + $reply_count;
                if(!empty($is_like)){
                    $is_like = $is_like->status;
                }else{
                    $is_like = 0;
                }
                $postadd_html = '';
                if($flag == 0){
                    $postadd_html = view('user.blocks.postadd')->render();
                }
                $post_html .= view('user.blocks.post',compact('post_v','comment_html','flag','postadd_html','post_id','is_like'
                    ,'comment_count','like_count','dislike_count','post_data','login_user_id'))->render();
            }
        }//posst

            return response()->json([
                'message' => 'sucess',
                'status' => 200,
                'post_html' => $post_html,
                'next_page' => $newsfeed_data['currentPage'] + 1,
                'last_page' => $newsfeed_data['lastPage']
            ]);
        }


    public function singlePost(Request $request)
    {
        $content = [];
        $newsfeed_data = [];
        $data['homeTab'] = 'active open';
        $noti_id = $request->noti_id;

        $notiArr['is_read'] = 1;
        $notiArr['id'] = $noti_id;
        $noti_update = $this->notification->changeStatus($notiArr);
        $post_v = $this->post->getPostById($request->post_id);
        $login_user_id = $user_id = Auth::user()->id;
        $like_count = $dislike_count = $is_like = 0;
        if(!empty($post_v)){
            $comment_html = '';
            $flag = 0;
            $post_html = '';
            $comment_html = '';
            $post_id = $post_v->id;
            $comment_count = $this->comment->getCommentCountByPostId($post_v->id);
            if($comment_count > 0){
                $comment_data = $this->comment->getCommentByPostId($post_v->id);
                $post_data['comment_data']['details'] = $comment_data;
                $post_data['comment_data']['total'] = $comment_data->total();
                $post_data['comment_data']['perPage'] = $comment_data->perPage();
                $post_data['comment_data']['currentPage'] = $comment_data->currentPage();
                $post_data['comment_data']['lastPage'] = $comment_data->lastPage();
                foreach ($comment_data as $comment_k => $comment_v){
                    $reply_html= '';
                    $reply_count_on_comment = $this->reply->getReplyCountByCommentId($comment_v->id);
                    if($reply_count_on_comment > 0){
                        $reply_data = $this->reply->getReplyByCommentId($comment_v->id);
                        $post_data['comment_data']['reply']['details'] = $reply_data;
                        $reply_html = view('user.blocks.reply',compact('reply_data','login_user_id'))->render();
                        $post_data['comment_data']['reply']['total'] = $reply_data->total();
                        $post_data['comment_data']['reply']['perPage'] = $reply_data->perPage();
                        $post_data['comment_data']['reply']['currentPage'] = $reply_data->currentPage();
                        $post_data['comment_data']['reply']['lastPage'] = $reply_data->lastPage();

                    }
                    $comment_html.= view('user.blocks.comment',compact('comment_v','reply_html','post_id','post_data','login_user_id'))->render();
                }
            }
            $reply_count = $this->reply->getReplyCountByPostId($post_v->id);
            $like_count = $this->like->getLikeCountByPostId($post_v->id);
            $dislike_count = $this->like->getDislikeCountByPostId($post_v->id);
            $is_like = $this->like->checkUserStatus($user_id,$post_v->id);
            $post_data['details'] = $post_v;
            $comment_count = $comment_count + $reply_count;
            if(!empty($is_like)){
                $is_like = $is_like->status;
            }else{
                $is_like = 0;
            }
            $postadd_html = '';
            if($flag == 0){
                $postadd_html = view('user.blocks.postadd')->render();
            }
            $post_html = view('user.blocks.post',compact('post_v','comment_html','flag','postadd_html','post_id','is_like'
                ,'comment_count','like_count','dislike_count','post_data','login_user_id'))->render();
            $flag = 1;
            $newsfeed_data['post_html'][] = $post_html;

        }
        $newsfeed_data['postadd_html'] = view('user.blocks.postadd')->render();
        $postadd_html = '';
        $postadd_html = view('user.blocks.postadd')->render();
        $newsfeed_data['postadd_html'] = view('user.blocks.postadd')->render();
        $recentComment = $this->comment->getRecentComment(Auth::user()->id);

        $connectionUser = $this->connnectionrequest->userConnection();
        $section = $this->user->getUsersForSidebars($connectionUser);

        $left_section_html = view('user.layouts.leftsection',compact('section','recentComment'))->render();
        $newsfeed_data['left_section_html'] = $left_section_html;
        return view('user.blocks.singlepost',$newsfeed_data);
        //return view('user.blocks.postadd',$data);
    }
}
