<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostReply;
use Auth;
use DB;

class ReplyController extends Controller
{

    protected $user;
    protected $post;
    protected $comment;
    protected $reply;
    /**
     * AdminUserController constructor.
     */

    public function __construct(User $user,Post $post,PostComment $comment, PostReply $reply)
    {
        $this->user = $user;
        $this->post = $post;
        $this->comment = $comment;
        $this->reply = $reply;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $content = [];
        $data['homeTab'] = 'active open';
        $post_html = view('user.blocks.post',$content)->render();
        $data['post_html'] = $post_html;
        return view('user.profile.timeline',$data);
    }
    public function store(Request $request){

        $requestData = $request->all();
        $login_user_id = $requestData['user_id'] = Auth::user()->id;
        DB::beginTransaction();
        try{
            $value = $this->reply->addPostReply($requestData);
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            return response()->json([
                'message' => 'failed111',
                'status' => 400
            ]);
        }
        if ($value) {
            $value['username'] = Auth::user()->name;
            $value['user_name'] = Auth::user()->username;
            $value['profile_image'] = Auth::user()->profile_image;
            $reply_html = '';
            $comment_html = view('user.blocks.reply-block',compact('value','login_user_id'))->render();
            return response()->json([
                'message' => 'sucess',
                'status' => 200,
                'comment_html' => $comment_html
            ]);
        } else {
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
    }

    public function moreReply(Request $request){

        $reply_data = $this->reply->getReplyByCommentId($request->comment_id);
        $login_user_id = Auth::user()->id;
        $post_data['comment_data']['reply']['details'] = $reply_data;
        $reply_html = view('user.blocks.reply',compact('reply_data','login_user_id'))->render();
        $post_data['comment_data']['reply']['total'] = $reply_data->total();
        $post_data['comment_data']['reply']['perPage'] = $reply_data->perPage();
        $post_data['comment_data']['reply']['currentPage'] = $reply_data->currentPage();
        $post_data['comment_data']['reply']['lastPage'] = $reply_data->lastPage();

        return response()->json([
            'message' => 'sucess',
            'status' => 200,
            'reply_html' => $reply_html,
            'next_page' => $post_data['comment_data']['reply']['currentPage'] + 1,
            'last_page' => $post_data['comment_data']['reply']['lastPage']
        ]);
    }

    public function delete(Request $request){

        $id = $request->id;
        DB::beginTransaction();
        try{
            $post = $this->reply->deletePostReply($id);
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
        if ($post) {
            return response()->json([
                'message' => 'sucess',
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
    }
}
