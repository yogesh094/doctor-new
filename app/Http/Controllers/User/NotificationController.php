<?php

namespace App\Http\Controllers\User;

use App\Models\Notification;
use App\Models\User;
use App\Models\ConnectionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class NotificationController extends Controller
{

    protected $user;
    protected $notification;
    /**
     * AdminUserController constructor.
     */

    public function __construct(User $user,Notification $notification)
    {
        $this->user = $user;
        $this->notification = $notification;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $where['user_to'] = Auth::user()->id;
        $where['is_read'] = 0;
        $update['is_read'] = 1;
        $this->notification->updateData($where,$update);
        
        $data['details'] = $getData = $this->notification->getNotification(Auth::user()->id);
        if(isset($getData)){
            $data['noti_total'] = $getData->total();
            $data['noti_perPage'] = $getData->perPage();
            $data['noti_currentPage'] = $getData->currentPage();
            $data['noti_lastPage'] = $getData->lastPage();
        }
        return view('user.profile.notification',$data);
    }

    public function getPushNotificationMessage(Request $request){
        $noti_html = $noti_blockhtml ='';
        $login_user_id = $user_id = Auth::user()->id;
        $noti_count = $notificationData = $this->notification->getUnreadCount($login_user_id);;
        $notificationData = $this->notification->getNotificationforTop($login_user_id);
        if(!empty($notificationData)){
            foreach ($notificationData as $key => $value){
                // if($value->type == 1){
                //     $description = 'Comment On this';
                //     //comment
                // }elseif ($value->type == 2){
                //     $description = 'Reply On this';
                //     //reply
                // }elseif ($value->type == 3){
                //     $description = 'Like On this';
                // }else{
                //     $description = 'Update On this';
                //     //other
                // }
                $description = $value->description;
                $noti_blockhtml .= view('user.blocks.notification-block',compact('value','description'))->render();
            }
            return response()->json([
                'message' => 'sucess',
                'status' => 200,
                'notification' => $noti_blockhtml,
                'noti_count' => $noti_count,
            ]);
        }else{
            return response()->json([
                'message' => 'no data',
                'status' => 401,
                'notification' => '',
                'noti_count' => $noti_count,
            ]);
        }
    }

    public function moreNotification(Request $request){
        $data['details'] = $getData = $this->notification->getNotification(Auth::user()->id);
        if(isset($getData)){
            $data['noti_total'] = $getData->total();
            $data['noti_perPage'] = $getData->perPage();
            $data['noti_currentPage'] = $getData->currentPage();
            $data['noti_lastPage'] = $getData->lastPage();
        }
        $post_html = '';
        if(isset($getData) && count($getData) > 0){
            foreach($getData as $key => $value){

                if(!empty($value->profile_image) && $value->profile_image != ''){
                    $image_url = config('services.upload_url').'/image/user/'.$value->profile_image;
                }else{
                    $image_url = url('images/frontend-image/resources/user-avatar.jpg');
                }
               
                $post_html .= '<li>
                        <figure>
                          <a href="'.url('profile/'.$value->username).'" title="">
                              <img src="'.$image_url.'">
                          </a>
                        </figure>
                        <div class="notifi-meta">
                            <p>'.$value->description.'</p>
                            <span>'. time_ago($value->created_at).'</span>
                        </div>
                    </li>';
            }
          }
        return response()->json([
            'message' => 'sucess',
            'status' => 200,
            'post_html' => $post_html,
            'next_page' => $data['noti_currentPage'] + 1,
            'last_page' => $data['noti_lastPage']
        ]);
    }
}
