<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Models\UserEducation;
use App\Models\UserExperience;
use App\Models\UserInterest;
use App\Models\Country;
use App\Models\Notification;
use App\Models\ConnectionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Session;
use Hash;

class ProfileController extends Controller
{

    protected $user;
    protected $country;
    protected $usereducation;
    protected $userexperience;
    protected $userinterest;
    protected $connectionrequest;
    protected $notification;
    /**
     * AdminUserController constructor.
     */

    public function __construct(User $user,Country $country,UserEducation $usereducation,
      UserExperience $userexperience,UserInterest $userinterest,ConnectionRequest $connectionrequest,Notification $notification)
    {
        $this->user = $user;
        $this->usereducation = $usereducation;
        $this->userexperience = $userexperience;
        $this->userinterest = $userinterest;
        $this->country = $country;
        $this->connectionrequest = $connectionrequest;
        $this->notification = $notification;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $data['country'] = $this->country->getCollection();
        $data['details'] = $this->user->getUserByField($userId,'id');
        return view('user.profile.editprofile',$data);
    }

    public function userDetail($username,$noti_id = 0){

        if(isset($noti_id) && $noti_id != 0){
          $noti_id = $noti_id;
          $notiArr['is_read'] = 1;
          $notiArr['id'] = $noti_id;
          $noti_update = $this->notification->changeStatus($notiArr);
        }
      
        $data['country'] = $this->country->getCollection();
        $userData = $this->user->getUserByField($username,'username');
        if(!empty($userData)){
          $profile_intro_html = view('user.blocks.profileintro',compact('userData'))->render();
          $data['profile_intro_html'] = $profile_intro_html;
          $data['educationData'] = $this->usereducation->getEducationById($userData->id);
          $data['experienceData'] = $this->userexperience->getExperienceById($userData->id);
          $data['connectionrequest'] = $this->connectionrequest->checkRequestSent(Auth::user()->id,$userData->id);

          // pp($data['connectionrequest']);
          $interestData = $this->userinterest->getInterestById($userData->id);
          $interest = [];
          if(isset($interestData)){
            $interest = explode(',',$interestData->title);
          }
          $data['interestData'] = $interest;
          $data['details'] = $userData;
          return view('user.profile.profile',$data);
        }else{
          echo "No data Found";
          exit;
        }
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // Start Communicate with database
        DB::beginTransaction();
        try{
            $addUser = $this->user->addUser($request->all());
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            $errorMessage = $e->getMessage();
            $request->session()->flash('alert-danger', $errorMessage);
            return redirect('profile/')->withInput();

        }

        if ($addUser) {
            $request->session()->flash('alert-success', __('app.default_edit_success',["module" => __('app.user')]));
            return redirect('profile/');
        } else {
            $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.user'),"action"=>__('app.update')]));
            return redirect('profilet/')->withInput();
        }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @desc update the profile image
    */
    public function updateProfilePicture(Request $request){

      DB::beginTransaction();
      try{
          $user = $this->user->updateProfilePicture($request->all());
          DB::commit();
      } catch (\Exception $e) {
          //exception handling
          DB::rollback();
          $errorMessage = $e->getMessage();
          $request->session()->flash('error', $errorMessage);
          return redirect('/profile')->withInput();
      }

      if ($user) {
        $request->session()->flash('alert-success', 'Profile image is updated successfully');
        return redirect('/profile');
      } else {
        $request->session()->flash('alert-danger', 'Something went wrong');
        return redirect('/profile')->withInput();
      }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @desc update the profile image
    */
    public function updateCoverPicture(Request $request){

      DB::beginTransaction();
      try{
          $user = $this->user->updateCoverPicture($request->all());
          DB::commit();
      } catch (\Exception $e) {
          //exception handling
          DB::rollback();
          $errorMessage = $e->getMessage();
          $request->session()->flash('error', $errorMessage);
          return redirect('/profile')->withInput();
      }

      if ($user) {
        $request->session()->flash('alert-success', 'Cover image is updated successfully');
        return redirect('/profile');
      } else {
        $request->session()->flash('alert-danger', 'Something went wrong');
        return redirect('/profile')->withInput();
      }
    }

    public function showEducation(){
        $userId = Auth::user()->id;
        $data['educationData'] = $this->usereducation->getEducationById($userId);
        $data['experienceData'] = $this->userexperience->getExperienceById($userId);
        $data['details'] = $this->user->getUserByField($userId,'id');
        return view('user.profile.education',$data);
    }

    public function showInterest(){
        $userId = Auth::user()->id;
        $interestData = $this->userinterest->getInterestById($userId);
        $interest = [];
        if(isset($interestData)){
          $interest = explode(',',$interestData->title);
        }
        $data['interestData'] = $interest;
        $data['details'] = $this->user->getUserByField($userId,'id');
        return view('user.profile.interest',$data);
    }

    public function showChangePassword(){
      $userId = Auth::user()->id;
      $data['details'] = $this->user->getUserByField($userId,'id');
      return view('user.profile.changepassword',$data);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @desc change password of user
     */
    public function changePassword(Request $request){
        $request_data = $request->all();
        $current_password = Auth::User()->password;
        $data =[];
        if(Hash::check($request_data['current-password'], $current_password))
        {
            $where['id'] = Auth::user()->id;
            $update = [];
            $update['password'] = Hash::make($request_data['password']);
            // Start Communicate with database
            DB::beginTransaction();
            try{
                $addUser = $this->user->updateData($where,$update);
                DB::commit();
            } catch (\Exception $e) {
                //exception handling
                DB::rollback();
                $request->session()->flash('alert-danger', 'Something went wrong. Please contact to support team at help@doctor.com');
            }
        }else{
            $request->session()->flash('alert-danger', 'Please enter correct current password');
        }
        if (isset($addUser)) {
            $request->session()->flash('alert-success', 'Password changed successfully');
        } else {
            $request->session()->flash('alert-danger', 'Please enter correct current password');
        }
        return redirect('/changepassword');
    }

  public function addEducation(Request $request){
      $request['user_id'] = Auth::user()->id;
      DB::beginTransaction();
      try{
          $user = $this->usereducation->addUserEducation($request->all());
          DB::commit();
      } catch (\Exception $e) {
          //exception handling
          DB::rollback();
          $errorMessage = $e->getMessage();
          $data['status'] = 401;
          $data['msg'] = $errorMessage;
          return $data;
      }
      if($user){
        $data['status'] = 200;
        if(isset($request->id)){
          $request->session()->flash('alert-success', 'Education is updated successfully');
        }else{
          $request->session()->flash('alert-success', 'Education is added successfully');
        }
        $data['msg'] = 'Successfully added.';
        return $data;
      }else{
        $data['status'] = 401;
        $request->session()->flash('alert-danger', 'Something went wrong. Please contact to support team');
        $data['msg'] = 'Something went wrong. Please contact to support team';
        return $data;
      }
  }

  public function addExperience(Request $request){
      $request['user_id'] = Auth::user()->id;
      DB::beginTransaction();
      try{
          $user = $this->userexperience->addUserExperience($request->all());
          DB::commit();
      } catch (\Exception $e) {
          //exception handling
          DB::rollback();
          $errorMessage = $e->getMessage();
          $data['status'] = 401;
          $request->session()->flash('alert-danger', 'Something went wrong. Please contact to support team');
          $data['msg'] = $errorMessage;
          return $data;
      }
      if($user){
        $data['status'] = 200;
        if(isset($request->id)){
          $request->session()->flash('alert-success', 'Experience is updated successfully');
        }else{
          $request->session()->flash('alert-success', 'Experience is added successfully');
        }
        $data['msg'] = 'Experience is added successfully';
        return $data;
      }else{
        $data['status'] = 401;
        $request->session()->flash('alert-danger', 'Something went wrong. Please contact to support team');
        $data['msg'] = 'Something went wrong. Please contact to support team';
        return $data;
      }
  }

  public function addInterest(Request $request){
      $request['user_id'] = Auth::user()->id;
      $interestData = $this->userinterest->getInterestById(Auth::user()->id);
      if(!empty($interestData)){
          $request['id'] = $interestData->id;
      }
      DB::beginTransaction();
      try{
          $user = $this->userinterest->addUserInterest($request->all());
          DB::commit();
      } catch (\Exception $e) {
          //exception handling
          DB::rollback();
          $errorMessage = $e->getMessage();
          $data['status'] = 401;
          $data['msg'] = $errorMessage;
          return $data;
      }
      if($user){
        $data['status'] = 200;
        if(isset($request->id)){
          $request->session()->flash('alert-success', 'Interest is updated successfully');
        }else{
          $request->session()->flash('alert-success', 'Interest is added successfully');
        }
        $data['msg'] = 'Successfully added.';
        return $data;
      }else{
        $data['status'] = 401;
        $request->session()->flash('alert-danger', 'Something went wrong. Please contact to support team');
        $data['msg'] = 'Something went wrong. Please contact to support team';
        return $data;
      }
  }

  public function deleteEducation(Request $request){
    $deleteEducation = $this->usereducation->deleteUserEducation($request->id);
    if ($deleteEducation) {
      $data['status'] = 200;
      $request->session()->flash('alert-success', __('app.default_delete_success',["module" => __('app.education')]));
      $data['msg'] = __('app.default_delete_success',["module" => __('app.education')]);
      return $data;
    } else {
      $data['status'] = 401;
      $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.education'),"action"=>__('app.delete')]));
      $data['msg'] = __('app.default_error',["module" => __('app.education'),"action"=>__('app.delete')]);
      return $data;
    }
  }

  public function deleteExperience(Request $request){
    $deleteExperience = $this->userexperience->deleteUserExperience($request->id);
    if ($deleteExperience) {
      $data['status'] = 200;
      $request->session()->flash('alert-success', __('app.default_delete_success',["module" => __('app.experience')]));
      $data['msg'] = __('app.default_delete_success',["module" => __('app.experience')]);
      return $data;
    } else {
      $data['status'] = 401;
      $request->session()->flash('alert-danger', __('app.default_error',["module" => __('app.experience'),"action"=>__('app.delete')]));
      $data['msg'] = __('app.default_error',["module" => __('app.experience'),"action"=>__('app.delete')]);
      return $data;
    }
  }

  public function editEducation(Request $request){
    $details = $this->usereducation->getUserEducationByField($request->id,'id');
    if ($details) {
      return $details;
    } else {
      return false;
    }
  }

  public function editExperience(Request $request){
    $details = $this->userexperience->getUserExperienceByField($request->id,'id');
    if ($details) {
      return $details;
    } else {
      return false;
    }
  }

}
