<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostReply;
use Illuminate\Support\Facades\Auth;
use DB;

class CommentController extends Controller
{

    protected $user;
    protected $post;
    protected $comment;
    protected $reply;
    /**
     * AdminUserController constructor.
     */

    public function __construct(User $user,Post $post,PostComment $comment, PostReply $reply)
    {
        $this->user = $user;
        $this->post = $post;
        $this->comment = $comment;
        $this->reply = $reply;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $content = [];
        $data['homeTab'] = 'active open';
        $post_html = view('user.blocks.post',$content)->render();
        $data['post_html'] = $post_html;
        return view('user.profile.timeline',$data);
    }

    public function store(Request $request){

        $requestData = $request->all();
        $login_user_id = $requestData['user_id'] = Auth::user()->id;
        DB::beginTransaction();
        try{
            $post = $this->comment->addPostComment($requestData);
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
        if ($post) {
            $comment_v = $post;
            $comment_v['username'] = Auth::user()->name;
            $comment_v['user_name'] = Auth::user()->username;
            $comment_v['profile_image'] = Auth::user()->profile_image;
            $reply_html = '';
            $comment_html = view('user.blocks.comment',compact('comment_v','reply_html','login_user_id'))->render();
            return response()->json([
                'message' => 'sucess',
                'status' => 200,
                'comment_html' => $comment_html
            ]);
        } else {
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
    }

    public function moreComment(Request $request){
        $comment_data = $this->comment->getCommentByPostId($request->post_id);
        $post_id = $request->post_id;
        $login_user_id = Auth::user()->id;
        $comment_html = '';
        if(!empty($comment_data)){
            $post_data['comment_data']['details'] = $comment_data;
            $post_data['comment_data']['total'] = $comment_data->total();
            $post_data['comment_data']['perPage'] = $comment_data->perPage();
            $post_data['comment_data']['currentPage'] = $comment_data->currentPage();
            $post_data['comment_data']['lastPage'] = $comment_data->lastPage();
            foreach ($comment_data as $comment_k => $comment_v){
                $reply_html= '';
                $reply_count_on_comment = $this->reply->getReplyCountByCommentId($comment_v->id);
                if($reply_count_on_comment > 0){
                   // echo $comment_v->id;
                    $reply_data = $this->reply->getLimitedReplyByCommentId($comment_v->id);
                    //pp($reply_data);
                    $post_data['comment_data']['reply']['details'] = $reply_data;
                    $reply_html = view('user.blocks.reply',compact('reply_data','login_user_id'))->render();
//                    $post_data['comment_data']['reply']['total'] = $reply_data->total();
//                    $post_data['comment_data']['reply']['perPage'] = $reply_data->perPage();
                    $post_data['comment_data']['reply']['currentPage'] = 1;
                    $reply_pagelimit=config('services.pagination.reply');
                    $lastPage = ceil($reply_count_on_comment / $reply_pagelimit);

                    $post_data['comment_data']['reply']['lastPage'] = $lastPage;
                }
                $comment_html.= view('user.blocks.comment',compact('comment_v','reply_html','post_id','post_data','login_user_id'))->render();
            }

            return response()->json([
                'message' => 'sucess',
                'status' => 200,
                'comment_html' => $comment_html,
                'next_page' => $post_data['comment_data']['currentPage'] + 1,
                'last_page' => $post_data['comment_data']['lastPage']
            ]);
        }
    }

    public function delete(Request $request){

        $id = $request->id;
        DB::beginTransaction();
        try{
            $post = $this->comment->deletePostComment($id);
            DB::commit();
        } catch (\Exception $e) {
            //exception handling
            DB::rollback();
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
        if ($post) {
            return response()->json([
                'message' => 'sucess',
                'status' => 200,
            ]);
        } else {
            return response()->json([
                'message' => 'failed',
                'status' => 400
            ]);
        }
    }
}
