<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class UserEducation extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_users_education';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all UserEducation getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return UserEducation::get();
    }

    /**
     * Get all UserEducation with role and ParentUserEducation relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  UserEducation::select('tbl_users_education.*');
    }

    /**
     * Query to get userEducation total count
     *
     * @param $dbObject
     * @return integer $userEducationCount
     */
    public static function getUserEducationCount($dbObject)
    {
        $userEducationCount = $dbObject->count();
        return $userEducationCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetUserEducationData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.userEducationFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/UserEducation
     * get filterred userEducation
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortUserEducationData($query, $request)
    {
        return $query->orderBy(config('constant.userEducationDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update UserEducation addUserEducation
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addUserEducation(array $models = [])
    {
        if (isset($models['id'])) {
            $userEducation = UserEducation::find($models['id']);

        } else {
            $userEducation = new UserEducation;
            $userEducation->created_at = date('Y-m-d H:i:s');
        }
        $userEducation->user_id = $models['user_id'];
        $userEducation->title = $models['title'];
        $userEducation->degree = $models['degree'];
        $userEducation->field_of_study = $models['field_of_study'];
        $userEducation->grade = $models['grade'];
        $userEducation->start_year = $models['start_year'];
        $userEducation->end_year = $models['end_year'];
        $userEducation->extra_activities = isset($models['extra_activities'])?$models['extra_activities']:null;
        $userEducation->updated_at = date('Y-m-d H:i:s');
        $userEducationId = $userEducation->save();
        if ($userEducationId) {
            return $userEducation;
        } else {
            return false;
        }
    }

    /**
     * get UserEducation By fieldname getUserEducationByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getUserEducationByField($id, $field_name)
    {
        return UserEducation::where($field_name, $id)->first();
    }

    /**
     * Delete UserEducation
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteUserEducation($id)
    {
        $delete = UserEducation::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($userEducation = '')
    {
        return UserEducation::select('discount')->where('code',$userEducation)->first();
    }

    /**
     * @param $status
     * @return bool
     * @desc change the userEducation status
     */
    public function changeStatus($status)
    {
        $status = UserEducation::where("deleted_at",null)->update(["status" => $status]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @return bool
     * @desc check status is active
     */
    public function checkStatus(){
        $status = UserEducation::where("deleted_at",null)->where("status",1)->first();
        if (!empty($status))
            return true;
        else
            return false;
    }

    public function getEducationById($user_id){
        return UserEducation::where("deleted_at",null)->where("user_id",$user_id)->get();
    }

}
