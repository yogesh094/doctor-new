<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Spatie\Permission\Models\Role;
use Spatie\permission\models\Permission;

class CustomRole extends Model
{

    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
    /**
     * Get all Role getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return CustomRole::get();
    }

    /**
     * Get all Role with role and ParentRole relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return CustomRole::select('roles.*');
    }

    /**
     * Query to get permission total count
     *
     * @param $dbObject
     * @return integer $permissionCount
     */
    public static function getRoleCount($dbObject)
    {
        $permissionCount = $dbObject->count();
        return $permissionCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetRoleData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.permissionFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/Role
     * get filterred permission
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortRoleData($query, $request)
    {

        return $query->orderBy(config('constant.permissionDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update Role addRole
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addRole(array $models = [])
    {
        if (isset($models['id'])) {
            $role = Role::find($models['id']);
            $role->name = $models['name'];
            $role->save();
        } else {
            $role = Role::create(['name' => $models['name']]);
        }
        $role->syncPermissions($models['permission']);
        $roleId = $role->id;
        if ($roleId) {
            return $role;
        } else {
            return false;
        }
    }

    /**
     * get Role By fieldname getRoleByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getRoleByField($id, $field_name)
    {
        return CustomRole::where($field_name, $id)->first();
    }

    /**
     * @return array
     * @desc get role permission
     */
    public function getRolePermissions($id)
    {
        return DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
    }


    /**
     * Delete Role
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteRole($id)
    {
        $delete = CustomRole::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }



}
