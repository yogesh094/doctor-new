<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class PostComment extends Model
{
    protected $table = 'tbl_post_comment';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all PostComment getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return PostComment::get();
    }

    /**
     * Get all PostComment with role and ParentPostComment relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  PostComment::select('tbl_post_comment.*');
    }

    /**
     * Query to get PostComment total count
     *
     * @param $dbObject
     * @return integer $PostCommentCount
     */
    public static function getPostCommentCount($dbObject)
    {
        $PostCommentCount = $dbObject->count();
        return $PostCommentCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetPostCommentData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.PostCommentFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/PostComment
     * get filterred PostComment
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortPostCommentData($query, $request)
    {
        return $query->orderBy(config('constant.PostCommentDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update PostComment addPostComment
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addPostComment(array $models = [])
    {
        if (isset($models['id'])) {
            $PostComment = PostComment::find($models['id']);
        } else {
            $PostComment = new PostComment;
            $PostComment->created_at = date('Y-m-d H:i:s');
        }
        $PostComment->user_id = $models['user_id'];
        $PostComment->description = $models['description'];
        $PostComment->post_id = $models['post_id'];
        $PostComment->updated_at = date('Y-m-d H:i:s');
        $PostCommentId = $PostComment->save();
        if ($PostCommentId) {
            $postObj =  new Post;
            $postData = $postObj->getPostByField($models['post_id'],'id');
            if($postData->user_id !=  $models['user_id']){
                $notificationArr['user_to'] = $postData->user_id;
                $notificationArr['user_from'] = $models['user_id'];
                $notificationArr['post_id'] = $models['post_id'];
                $notificationArr['description'] = Auth::user()->name.' commented on your post';
                $notificationArr['type'] = 1;
                $notiObj = new Notification;
                $notiObj->addNotification($notificationArr);
            }
            return $PostComment;
        } else {
            return false;
        }
    }

    /**
     * get PostComment By fieldname getPostCommentByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getPostCommentByField($id, $field_name)
    {
        return PostComment::where($field_name, $id)->first();
    }

    /**
     * Delete PostComment
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deletePostComment($id)
    {
        $delete = PostComment::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($PostComment = '')
    {
        return PostComment::select('discount')->where('code',$PostComment)->first();
    }

    /**
     * @param $status
     * @return bool
     * @desc change the PostComment status
     */
    public function changeStatus($status)
    {
        $status = PostComment::where("deleted_at",null)->update(["status" => $status]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @return bool
     * @desc check status is active
     */
    public function checkStatus(){
        $status = PostComment::where("deleted_at",null)->where("status",1)->first();
        if (!empty($status))
            return true;
        else
            return false;
    }

    public function getCommentByPostId($post_id){
        $pageLimit = 2;
        return PostComment::leftjoin('users', 'tbl_post_comment.user_id', '=', 'users.id')
            ->select('tbl_post_comment.*','users.name as username','users.profile_image','users.username as user_name')
            ->where("tbl_post_comment.deleted_at",null)->where("tbl_post_comment.post_id",$post_id)
            ->orderBy("tbl_post_comment.created_at", "ASC")
            ->paginate($pageLimit);
    }

    public function getCommentCountByPostId($post_id){
        return PostComment::where("deleted_at",null)->where("post_id",$post_id)->count();
    }

    public function getRecentComment($user_id){
        return PostComment::select('description','created_at')->where("deleted_at",null)->where('user_id',$user_id)->limit('5')->get();
    }

}
