<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model 
{

    protected $table = 'tbl_states';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * Get all State getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return State::get();
    }
    /**
     * Get all State By Country
     *
     * @return mixed
     */
    public function getCollectionByCounty($field_name,$id)
    {
        $state = State::select('tbl_states.*')->where($field_name,$id);
        return $state->get();
    }


    /**
     * Get single record by field
     * @param $field_name
     * @param $id
     * @return mixed
     */
    public function getCollectionByField($field_name,$id)
    {
        return State::select('tbl_states.*')->where($field_name,$id)->first();
    }


}