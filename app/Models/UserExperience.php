<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class UserExperience extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_users_experience';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all UserExperience getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return UserExperience::get();
    }

    /**
     * Get all UserExperience with role and ParentUserExperience relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  UserExperience::select('tbl_users_experience.*');
    }

    /**
     * Query to get userExperience total count
     *
     * @param $dbObject
     * @return integer $userExperienceCount
     */
    public static function getUserExperienceCount($dbObject)
    {
        $userExperienceCount = $dbObject->count();
        return $userExperienceCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetUserExperienceData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.userExperienceFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/UserExperience
     * get filterred userExperience
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortUserExperienceData($query, $request)
    {
        return $query->orderBy(config('constant.userExperienceDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update UserExperience addUserExperience
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addUserExperience(array $models = [])
    {
        if (isset($models['id'])) {
            $userExperience = UserExperience::find($models['id']);

        } else {
            $userExperience = new UserExperience;
            $userExperience->created_at = date('Y-m-d H:i:s');
        }
        $userExperience->user_id = $models['user_id'];
        $userExperience->title = $models['title'];
        $userExperience->employment_type = $models['employment_type'];
        $userExperience->company = $models['company'];
        $userExperience->location = $models['location'];
        $userExperience->is_currently_working = isset($models['is_currently_working'])?$models['is_currently_working']:0;
        $userExperience->start_month = isset($models['start_month'])?$models['start_month']:null;
        $userExperience->start_year = isset($models['start_year'])?$models['start_year']:null;
        $userExperience->end_month = isset($models['end_month'])?$models['end_month']:null;
        $userExperience->end_year = isset($models['end_year'])?$models['end_year']:null;
        $userExperience->description = isset($models['description'])?$models['description']:null;
        $userExperience->updated_at = date('Y-m-d H:i:s');
        $userExperienceId = $userExperience->save();
        if ($userExperienceId) {
            return $userExperience;
        } else {
            return false;
        }
    }

    /**
     * get UserExperience By fieldname getUserExperienceByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getUserExperienceByField($id, $field_name)
    {
        return UserExperience::where($field_name, $id)->first();
    }

    /**
     * Delete UserExperience
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteUserExperience($id)
    {
        $delete = UserExperience::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($userExperience = '')
    {
        return UserExperience::select('discount')->where('code',$userExperience)->first();
    }

    /**
     * @param $status
     * @return bool
     * @desc change the userExperience status
     */
    public function changeStatus($status)
    {
        $status = UserExperience::where("deleted_at",null)->update(["status" => $status]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @return bool
     * @desc check status is active
     */
    public function checkStatus(){
        $status = UserExperience::where("deleted_at",null)->where("status",1)->first();
        if (!empty($status))
            return true;
        else
            return false;
    }

    public function getExperienceById($user_id){
        return UserExperience::where("deleted_at",null)->where("user_id",$user_id)->get();
    }

}
