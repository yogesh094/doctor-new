<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class Country extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_country';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','code'
    ];


    /**
     * Get all Country getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return Country::get();
    }

    /**
     * Get all Country with role and ParentCountry relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  Country::select('tbl_country.*');
    }

    /**
     * Query to get country total count
     *
     * @param $dbObject
     * @return integer $countryCount
     */
    public static function getCountryCount($dbObject)
    {
        $countryCount = $dbObject->count();
        return $countryCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetCountryData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.countryFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/Country
     * get filterred country
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortCountryData($query, $request)
    {
        return $query->orderBy(config('constant.countryDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update Country addCountry
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addCountry(array $models = [])
    {
        if (isset($models['id'])) {
            $country = Country::find($models['id']);

        } else {
            $country = new Country;
            $country->created_at = date('Y-m-d H:i:s');
        }
        $country->name = $models['name'];
        $country->code = $models['code'];
        $country->phonecode = $models['phonecode'];
        $country->currency = $models['currency'];
        $country->payment_gateway = $models['payment_gateway'];
        $country->updated_at = date('Y-m-d H:i:s');

        if (isset($models['status'])) {
            $country->status = $models['status'];
        } else {
            $country->status = 0;
        }
        $countryId = $country->save();
        if ($countryId) {
            return $country;
        } else {
            return false;
        }
    }

    /**
     * get Country By fieldname getCountryByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getCountryByField($id, $field_name)
    {
        return Country::where($field_name, $id)->first();
    }

    /**
     * Delete Country
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteCountry($id)
    {
        $delete = Country::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($country = '')
    {
        return Country::select('discount')->where('code',$country)->first();
    }


    /**
     * Get all phoneCode by order
     * @return mixed
     */
    public function getPhoneCodeCollection()
    {
        return Country::select('phonecode')->groupBy('phonecode')->orderBy('phonecode','ASC')->get();
    }

    /**
     * @param $field_name
     * @param $id
     * @return mixed
     */

    public function getCollectionByField($field_name,$id)
    {
        return Country::select('tbl_country.*')->where($field_name,$id)->first();
    }

    /**
     * @param $id
     * @return mixed
     * @desc get country name by id
     */
    public function getCountryName($id){
        return Country::select('name','code')->where('id',$id)->first();
    }

    /**
     * @param $id
     * @return mixed
     * @desc get country id by name
     */
    public function getCountryIdByName($name){
        return Country::select('id')->where('name',$name)->first();
    }

    /**
     * update Country Status
     *
     * @param array $models
     * @return boolean true | false
     */
    public function updateStatus(array $models = [])
    {
        $country = Country::find($models['id']);
        $country->status = $models['status'];
        $country->updated_at = date('Y-m-d H:i:s');
        $countryId = $country->save();
        if ($countryId)
            return true;
        else
            return false;

    }

    /**
     * Get all Country getCollection
     *
     * @return mixed
     */
    public function getActiveCollection()
    {
        return Country::where('status',1)->get();
    }
}
