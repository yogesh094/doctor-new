<?php

namespace App\Models;

use App\Helpers\LaraHelpers;
use App\Models\ConnectionRequest;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;


class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username','email', 'password','gender','phone_code','mobile_no',
        'profile_image','country','state','is_login','status','dob','profile_type','provider','provider_id','description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     /**
     * Get all User getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return User::get();
    }

    /**
     * Get all User getCollection of active gateway
     *
     * @return mixed
     */
    public function getActiveCollection()
    {
        return User::where('status',1)->get();
    }

    /**
     * Get all User with role and ParentUser relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  User::select('user.*');
    }

    /**
     * Query to get user total count
     *
     * @param $dbObject
     * @return integer $userCount
     */
    public static function getUserCount($dbObject)
    {
        $userCount = $dbObject->count();
        return $userCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetUserData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get();
    }

    /**
     * scopeGetFilteredData from App/Models/User
     * get filterred user
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortUserData($query, $request)
    {

        return $query->orderBy(config('constant.userDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update User addUser
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addUser(array $models = [])
    {
        if (isset($models['id'])) {
            $user = User::find($models['id']);

        } else {
            $user = new User;
            $user->created_at = date('Y-m-d H:i:s');
        }

        $models['dob'] = date('Y-m-d',strtotime($models['day'].'-'.$models['month'].'-'.$models['year']));

        $user->name = isset($models['name'])?$models['name']:null;
        $user->phone_code = isset($models['phone_code'])?$models['phone_code']:null;
        $user->mobile_no = isset($models['mobile_no'])?$models['mobile_no']:null;
        $user->gender = isset($models['gender'])?$models['gender']:null;
        $user->country = isset($models['country'])?$models['country']:1;
        $user->state = isset($models['state'])?$models['state']:1;
        $user->city = isset($models['city'])?$models['city']:null;
        $user->dob = isset($models['dob'])?$models['dob']:null;
        $user->profile_type = isset($models['profile_type'])?$models['profile_type']:0;
        $user->description = isset($models['description'])?$models['description']:null;
        $user->updated_at = date('Y-m-d H:i:s');
        $userId = $user->save();
        if ($userId) {
            return $user;
        } else {
            return false;
        }
    }

    /**
     * get User By fieldname getUserByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getUserByField($id, $field_name)
    {
        return User::where($field_name, $id)->where('status',1)->first();
    }

    /**
     * update User Status
     *
     * @param array $models
     * @return boolean true | false
     */
    public function updateStatus(array $models = [])
    {
        $user = User::find($models['id']);
        $user->status = $models['status'];
        $user->updated_at = date('Y-m-d H:i:s');
        $userId = $user->save();
        if ($userId)
            return true;
        else
            return false;

    }

    /**
     * Delete User
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteUser($id)
    {
        $delete = User::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * Update Profile Picture
     * @param array $models
     * @return boolean true | false
     */
    public function updateProfilePicture(array $models = [])
    {
        $filepath = 'storage/image/user'. DIRECTORY_SEPARATOR;
        if (isset($models['id'])) {
            $user = User::find($models['id']);
            // if(isset($models['profile_image'])) {
            //     LaraHelpers::remove_image($filepath."/".$user->profile_image);
            // }
            if(isset($models['profile_image'])){
                $file_name = LaraHelpers::upload_image_local($filepath, $models['profile_image'], "");
                $user->profile_image = $file_name;
            }
            $userId = $user->save();
            if ($userId) {
                return $user;
            }
        }else {
            return false;
        }
    }

        /**
     * Update Profile Picture
     * @param array $models
     * @return boolean true | false
     */
    public function updateCoverPicture(array $models = [])
    {
        $filepath = 'storage/image/user'. DIRECTORY_SEPARATOR;
        if (isset($models['id'])) {
            $user = User::find($models['id']);
            // if(isset($models['profile_image'])) {
            //     LaraHelpers::remove_image($filepath."/".$user->profile_image);
            // }
            if(isset($models['cover_image'])){
                $file_name = LaraHelpers::upload_image_local($filepath, $models['cover_image'], "");
                $user->cover_image = $file_name;
            }
            $userId = $user->save();
            if ($userId) {
                return $user;
            }
        }else {
            return false;
        }
    }

    /**
     * @param array $where
     * @param array $update
     * @return mixed
     * @desc update the data as per requirement
     */
    public function updateData($where = array(),$update = array()){
        return User::where($where)->update($update);
    }
    /**
     * @param array $connectionUser
     * @return mixed
     * @desc Recommeneded friends
     */
    public function getUsersForSidebars($connectionUser)
    {
        return  User::where('users.id','!=',Auth::user()->id)->whereNotIn('id',$connectionUser)->select('users.id','users.name','users.username','users.profile_image')->limit('5')->get();
    }

    public function getSpecificUserData($select){
        return User::where('users.id','!=',Auth::user()->id)->select($select)->get();
    }

    /**
     * @param array $ids
     * @return mixed
     * @desc Request accepted friends for the chat
     */
    public function getUserData($ids){
        return User::whereIn('id',$ids)->select('name','profile_image','email','username')->get();
    }
}
