<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class UserInterest extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_users_interest';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all UserInterest getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return UserInterest::get();
    }

    /**
     * Get all UserInterest with role and ParentUserInterest relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  UserInterest::select('tbl_users_interest.*');
    }

    /**
     * Query to get userInterest total count
     *
     * @param $dbObject
     * @return integer $userInterestCount
     */
    public static function getUserInterestCount($dbObject)
    {
        $userInterestCount = $dbObject->count();
        return $userInterestCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetUserInterestData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.userInterestFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/UserInterest
     * get filterred userInterest
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortUserInterestData($query, $request)
    {
        return $query->orderBy(config('constant.userInterestDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update UserInterest addUserInterest
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addUserInterest(array $models = [])
    {
        if (isset($models['id'])) {
            $userInterest = UserInterest::find($models['id']);

        } else {
            $userInterest = new UserInterest;
            $userInterest->created_at = date('Y-m-d H:i:s');
        }
        $userInterest->user_id = $models['user_id'];
        $userInterest->title = $models['title'];
        $userInterest->updated_at = date('Y-m-d H:i:s');
        $userInterestId = $userInterest->save();
        if ($userInterestId) {
            return $userInterest;
        } else {
            return false;
        }
    }

    /**
     * get UserInterest By fieldname getUserInterestByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getUserInterestByField($id, $field_name)
    {
        return UserInterest::where($field_name, $id)->first();
    }

    /**
     * Delete UserInterest
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteUserInterest($id)
    {
        $delete = UserInterest::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($userInterest = '')
    {
        return UserInterest::select('discount')->where('code',$userInterest)->first();
    }

    /**
     * @param $status
     * @return bool
     * @desc change the userInterest status
     */
    public function changeStatus($status)
    {
        $status = UserInterest::where("deleted_at",null)->update(["status" => $status]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @return bool
     * @desc check status is active
     */
    public function checkStatus(){
        $status = UserInterest::where("deleted_at",null)->where("status",1)->first();
        if (!empty($status))
            return true;
        else
            return false;
    }

    public function getInterestById($user_id){
        return UserInterest::where("deleted_at",null)->where("user_id",$user_id)->first();
    }

}
