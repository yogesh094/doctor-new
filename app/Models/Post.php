<?php

namespace App\Models;

use App\Helpers\LaraHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class Post extends Model
{
    protected $table = 'tbl_post';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all Post getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return Post::get();
    }

    /**
     * Get all Post with role and ParentPost relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  Post::select('tbl_users_education.*');
    }

    /**
     * Query to get Post total count
     *
     * @param $dbObject
     * @return integer $PostCount
     */
    public static function getPostCount($dbObject)
    {
        $PostCount = $dbObject->count();
        return $PostCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetPostData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.PostFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/Post
     * get filterred Post
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortPostData($query, $request)
    {
        return $query->orderBy(config('constant.PostDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update Post addPost
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addPost(array $models = [])
    {
        $filepath = 'storage/image/user/'.Auth::user()->id.'/';
        if (isset($models['id'])) {
            $post = Post::find($models['id']);
        } else {
            $post = new Post;
            $post->created_at = date('Y-m-d H:i:s');
        }
        $post->user_id = $models['user_id'];
        $post->description = isset($models['description'])?$models['description']:'';
        if(isset($models['media']) && !empty($models['media'])){
            $file_name = LaraHelpers::upload_image_local($filepath, $models['media'], "");
            $post->media = $file_name;
            $extension = explode('.',$file_name);
            if(in_array(strtolower($extension[1]), ['jpg','png','svg','gif','jpeg'])){
                $post->media_type = 1;
            }else{
                $post->media_type = 2;
            }
        }else{
            $post->media_type = isset($models['media_type'])?$models['media_type']:0;
        }
        $post->status = 1;
        $post->updated_at = date('Y-m-d H:i:s');
        $postId = $post->save();
        if ($postId) {
            return $post;
        } else {
            return false;
        }
    }

    /**
     * get Post By fieldname getPostByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getPostByField($id, $field_name)
    {
        return Post::where($field_name, $id)->first();
    }

    /**
     * Delete Post
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deletePost($id)
    {
        $delete = Post::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($Post = '')
    {
        return Post::select('discount')->where('code',$Post)->first();
    }

    /**
     * @param $status
     * @return bool
     * @desc change the Post status
     */
    public function changeStatus($status)
    {
        $status = Post::where("deleted_at",null)->update(["status" => $status]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @return bool
     * @desc check status is active
     */
    public function checkStatus(){
        $status = Post::where("deleted_at",null)->where("status",1)->first();
        if (!empty($status))
            return true;
        else
            return false;
    }

    public function getPostList($user_id = 0){

        if($user_id > 0){
            $pageLimit =config('services.pagination.post');
            return Post::leftjoin('users', 'tbl_post.user_id', '=', 'users.id')
                ->select('tbl_post.*','users.name as username','users.profile_image','users.username as user_name')
                ->where('tbl_post.status',1)
                ->where('tbl_post.user_id',$user_id)
                ->orderBy("tbl_post.created_at", "DESC")
                ->paginate($pageLimit);
        }else{
            $pageLimit =config('services.pagination.post');
            return Post::leftjoin('users', 'tbl_post.user_id', '=', 'users.id')
                ->select('tbl_post.*','users.name as username','users.profile_image','users.username as user_name')
                ->where('tbl_post.status',1)
                ->orderBy("tbl_post.created_at", "DESC")
                ->paginate($pageLimit);
        }
    }
    public function getPostById($post_id = 0){
        return Post::leftjoin('users', 'tbl_post.user_id', '=', 'users.id')
            ->select('tbl_post.*','users.name as username','users.profile_image','users.username as user_name')
            ->where('tbl_post.status',1)
            ->where('tbl_post.id',$post_id)
            ->first();
    }

    public function getTotalPostCount($user_id = 0){
        return Post::where('status',1)->where('user_id',$user_id)->count();
    }

}
