<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class PostReply extends Model
{
    protected $table = 'tbl_post_reply';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all PostReply getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return PostReply::get();
    }

    /**
     * Get all PostReply with role and ParentPostReply relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  PostReply::select('tbl_post_reply.*');
    }

    /**
     * Query to get PostReply total count
     *
     * @param $dbObject
     * @return integer $PostReplyCount
     */
    public static function getPostReplyCount($dbObject)
    {
        $PostReplyCount = $dbObject->count();
        return $PostReplyCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetPostReplyData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.PostReplyFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/PostReply
     * get filterred PostReply
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortPostReplyData($query, $request)
    {
        return $query->orderBy(config('constant.PostReplyDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update PostReply addPostReply
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addPostReply(array $models = [])
    {
        if (isset($models['id'])) {
            $PostReply = PostReply::find($models['id']);

        } else {
            $PostReply = new PostReply;
            $PostReply->created_at = date('Y-m-d H:i:s');
        }
        $PostReply->user_id = $models['user_id'];
        $PostReply->description = $models['description'];
        $PostReply->post_id = $models['post_id'];
        $PostReply->post_comment_id = $models['post_comment_id'];
        $PostReply->updated_at = date('Y-m-d H:i:s');
        $PostReplyId = $PostReply->save();
        if ($PostReplyId) {
            $postObj =  new Post;
            $postData = $postObj->getPostByField($models['post_id'],'id');
            if($postData->user_id !=  $models['user_id']){
                $notificationArr['user_to'] = $postData->user_id;
                $notificationArr['user_from'] = $models['user_id'];
                $notificationArr['post_id'] = $models['post_id'];
                $notificationArr['description'] = Auth::user()->name.' reply on your post';
                $notificationArr['type'] = 2;
                $notiObj = new Notification;
                $notiObj->addNotification($notificationArr);
            }
            return $PostReply;
        } else {
            return false;
        }
    }

    /**
     * get PostReply By fieldname getPostReplyByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getPostReplyByField($id, $field_name)
    {
        return PostReply::where($field_name, $id)->first();
    }

    /**
     * Delete PostReply
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deletePostReply($id)
    {
        $delete = PostReply::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($PostReply = '')
    {
        return PostReply::select('discount')->where('code',$PostReply)->first();
    }

    /**
     * @param $status
     * @return bool
     * @desc change the PostReply status
     */
    public function changeStatus($status)
    {
        $status = PostReply::where("deleted_at",null)->update(["status" => $status]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @return bool
     * @desc check status is active
     */
    public function checkStatus(){
        $status = PostReply::where("deleted_at",null)->where("status",1)->first();
        if (!empty($status))
            return true;
        else
            return false;
    }


    public function getReplyByCommentId($comment_id){
        $pageLimit =config('services.pagination.reply');
        return PostReply::leftjoin('users', 'tbl_post_reply.user_id', '=', 'users.id')
            ->select('tbl_post_reply.*','users.name as username','users.profile_image','users.username as user_name')
            ->where("tbl_post_reply.deleted_at",null)
            ->where("tbl_post_reply.post_comment_id",$comment_id)
            ->orderBy("tbl_post_reply.created_at", "ASC")
            ->paginate($pageLimit);
    }

    public function getReplyCountByPostId($post_id){
        return PostReply::where("deleted_at",null)->where("post_id",$post_id)->count();
    }

    public function getReplyCountByCommentId($comment_id){
        return PostReply::where("deleted_at",null)->where("post_comment_id",$comment_id)->count();
    }

    public function getLimitedReplyByCommentId($comment_id){
        $pageLimit =config('services.pagination.reply');
        return PostReply::leftjoin('users', 'tbl_post_reply.user_id', '=', 'users.id')
            ->select('tbl_post_reply.*','users.name as username','users.profile_image','users.username as user_name')
            ->where("tbl_post_reply.deleted_at",null)
            ->where("tbl_post_reply.post_comment_id",$comment_id)
            ->orderBy("tbl_post_reply.created_at", "ASC")
            ->limit($pageLimit)
            ->get();
    }

}
