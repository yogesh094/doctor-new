<?php

namespace App\Models;

use App\Helpers\LaraHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Models\Notification;


class ConnectionRequest extends Model
{
    protected $table = 'tbl_connection_request';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all ConnectionRequest getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return ConnectionRequest::get();
    }

    /**
     * Get all ConnectionRequest with role and ParentConnectionRequest relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  ConnectionRequest::select('tbl_connection_request.*');
    }

    /**
     * Query to get ConnectionRequest total count
     *
     * @param $dbObject
     * @return integer $ConnectionRequestCount
     */
    public static function getConnectionRequestCount($dbObject)
    {
        $ConnectionRequestCount = $dbObject->count();
        return $ConnectionRequestCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetConnectionRequestData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.ConnectionRequestFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/ConnectionRequest
     * get filterred ConnectionRequest
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortConnectionRequestData($query, $request)
    {
        return $query->orderBy(config('constant.ConnectionRequestDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update ConnectionRequest addConnectionRequest
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addConnectionRequest(array $models = [])
    {
        $filepath = 'storage/image/user/'.Auth::user()->id.'/';
        if (isset($models['id'])) {
            $connectionRequest = ConnectionRequest::find($models['id']);
        } else {
            $connectionRequest = new ConnectionRequest;
            $connectionRequest->created_at = date('Y-m-d H:i:s');
        }
        $connectionRequest->request_from = $models['request_from'];
        $connectionRequest->request_to = $models['request_to'];
        $connectionRequest->type = 0;
        $connectionRequest->updated_at = date('Y-m-d H:i:s');
        $connectionRequestId = $connectionRequest->save();
        if ($connectionRequestId) {
            $notificationArr['user_to'] = $models['request_to'];
            $notificationArr['user_from'] = $models['request_from'];
            $notificationArr['post_id'] = 0;
            $notificationArr['type'] = 0;
            $notificationArr['description'] = Auth::user()->name.' sent the request';
            $notiObj = new Notification;
            $notiObj->addNotification($notificationArr);
            return $connectionRequest;
        } else {
            return false;
        }
    }

    /**
     * get ConnectionRequest By fieldname getConnectionRequestByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getConnectionRequestByField($id, $field_name)
    {
        return ConnectionRequest::where($field_name, $id)->first();
    }

    /**
     * Delete ConnectionRequest
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteConnectionRequest($id)
    {
        $delete = ConnectionRequest::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($ConnectionRequest = '')
    {
        return ConnectionRequest::select('discount')->where('code',$ConnectionRequest)->first();
    }

    /**
     * @param $status
     * @return bool
     * @desc change the ConnectionRequest status
     */
    public function changeStatus($status)
    {
        $status = ConnectionRequest::where("deleted_at",null)->update(["status" => $status]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @return bool
     * @desc check status is active
     */
    public function checkStatus(){
        $status = ConnectionRequest::where("deleted_at",null)->where("status",1)->first();
        if (!empty($status))
            return true;
        else
            return false;
    }

    /**
     * @param array $where
     * @param array $update
     * @return mixed
     * @desc update the data as per requirement
     */
    public function updateData($where = array(),$update = array()){
        return ConnectionRequest::where($where)->update($update);
    }

    public function checkRequestSent($request_from,$request_to){
        return ConnectionRequest::where('type','!=',2)
                    ->where(function($query) use ($request_from,$request_to) {
                        return $query->where('request_from',$request_to)
                        ->orWhere('request_to',$request_to);
                    })
                    ->where(function($query) use ($request_from,$request_to) {
                        return $query->where('request_from',$request_from)
                        ->orWhere('request_to',$request_from);
                    })
                    ->first();
    }

    public function getUserFriend(){
        $pending_request = ConnectionRequest::where('type',1)->where('request_to',Auth::user()->id)
                    ->pluck('request_from')->toArray();
        $my_requests = ConnectionRequest::where('type',1)->where('request_from',Auth::user()->id)
                    ->pluck('request_to')->toArray();  
        $ids = array_merge($pending_request,$my_requests);
        return $ids;
    }

    public function getTotalFriends(){
        $pageLimit =config('services.pagination.friend');
        $data['my_friends'] = ConnectionRequest::where('type',1)
                    ->where(function($query) {
                        return $query->where('request_from',Auth::user()->id)
                        ->orWhere('request_to',Auth::user()->id);
                    })->paginate($pageLimit);
        $data['pending_request'] = ConnectionRequest::where('type',0)
                    ->where('request_to',Auth::user()->id)
                    ->paginate($pageLimit);
        return $data;
    }

    public function getConnectionCount(){
        return ConnectionRequest::where('type',1)
                    ->where(function($query) {
                        return $query->where('request_from',Auth::user()->id)
                        ->orWhere('request_to',Auth::user()->id);
                    })->count();
    }

    public function getMyFriends(){
        $pageLimit =config('services.pagination.friend');
        return ConnectionRequest::where('type',1)
                    ->where(function($query) {
                        return $query->where('request_from',Auth::user()->id)
                        ->orWhere('request_to',Auth::user()->id);
                    })->paginate($pageLimit);
    }

    public function getPendingRequest(){
        $pageLimit =config('services.pagination.friend');
        return ConnectionRequest::where('type',0)
                    ->where('request_to',Auth::user()->id)
                    ->paginate($pageLimit);
    }

    public function userConnection(){
        $pending_request = ConnectionRequest::where('request_to',Auth::user()->id)
                    ->pluck('request_from')->toArray();
        $my_requests = ConnectionRequest::where('request_from',Auth::user()->id)
                    ->pluck('request_to')->toArray();  
        $ids = array_merge($pending_request,$my_requests);
        return $ids;
    }
}
