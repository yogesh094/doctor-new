<?php

namespace App\Models;

use App\Models\InstituteStudentMockExam;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Hash;
use Auth;
use DB;
//Trait for sending notifications in laravel
use Illuminate\Notifications\Notifiable;
//Notification for Seller
use App\Notifications\AdminUserResetPasswordNotification;
use App\Models\StudentMockExam;

class AdminUser extends Authenticatable
{
    use Notifiable;
    protected $table = 'tbl_admin';
    public $timestamps = true;

    use SoftDeletes;
    use HasRoles;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name','username', 'email', 'password','position'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guard = 'admin';

    /**
     * Get all Admin User getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        $adminUser = AdminUser::select('tbl_admin.*');
        return $adminUser->get();
    }

    /**
     * Get all User with role and ParentUser relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  AdminUser::select('tbl_admin.*')
                            ->where('id','!=',Auth::user()->id);
    }

    /**
     * Query to get user total count
     *
     * @param $dbObject
     * @return integer $userCount
     */
    public static function getUserCount($dbObject)
    {
        $userCount = $dbObject->count();
        return $userCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetUserData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.userDataTableFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/SaleData
     * get filterred saledatas
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortUserData($query, $request)
    {

        return $query->orderBy(config('constant.userDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Get All Roles Collection
     * @return mixed
     */
    public function getRoles(){
        $roles = Role::get();
        return $roles;
    }

    /**
     * Add & update Admin Users
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addAdminUser(array $models = [])
    {
        if (isset($models['id'])) {
            $adminUser = AdminUser::find($models['id']);
        } else {
            $adminUser = new AdminUser;
        }
        $adminUser->email = $models['email'];
        $adminUser->username = $models['username'];
        $adminUser->name = $models['name'];
        $adminUser->position = $models['position'];
        $adminUser->status = 1;
        if(!empty($models['password'])){
            $adminUser->password = Hash::make($models['password']);
        }
        $adminUserId = $adminUser->save();
        if ($adminUserId) {
            if(isset($models['id']) && !empty($models['id'])){
                DB::table('model_has_roles')->where('model_id',$models['id'])->delete();
            }
            $adminUser->assignRole($models['roles']);
            return $adminUser;
        } else {
            return false;
        }
    }

    /**
     * @param string $token
     * @desc Send Password Reset Notification
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminUserResetPasswordNotification($token));
    }

    /**
     * get User By fieldname getUserByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getUserByField($id, $field_name)
    {
        return AdminUser::where($field_name, $id)->first();
    }

    /**
     * update User Status
     *
     * @param array $models
     * @return boolean true | false
     */
    public function updateStatus(array $models = [])
    {
        $user = AdminUser::find($models['id']);
        $user->status = $models['status'];
        $user->updated_at = date('Y-m-d H:i:s');
        $userId = $user->save();
        if ($userId)
            return true;
        else
            return false;

    }

    /**
     * Delete User
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteUser($id)
    {
        $delete = AdminUser::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * Get User Role using Role Model
     * @param $id
     * @param $field_name
     * @return mixed
     */
    public  function getRoleById($id)
    {
        $userRole = DB::table('model_has_roles')->select('role_id')->where('model_id',$id)->first();
        //$userRole = Role::where($field_name, $id)->first();
        return $userRole;

    }


    /**
     * Get Data For Dashboard Count only
     * @param
     * @return array
     */
    public function dashboard(){

        $adminDataArr= [];

        // Get Enquiry Data
        $enquiryObj = new StudentEnquiry;
        $enquiryData = $enquiryObj->getEnquiryCount();
        $adminDataArr['enquiry']['total'] = $enquiryData['total'];
        $adminDataArr['enquiry']['today'] = $enquiryData['today'];
        $adminDataArr['enquiry']['week'] = $enquiryData['week'];
        $adminDataArr['enquiry']['month'] = $enquiryData['month'];
        // Get Enquiry Data end

        // Get Institute Enquiry Data
        $ienquiryObj = new InstituteEnquiry;
        $ienquiryData = $ienquiryObj->getEnquiryCount();
        $adminDataArr['ienquiry']['total'] = $ienquiryData['total'];
        $adminDataArr['ienquiry']['today'] = $ienquiryData['today'];
        $adminDataArr['ienquiry']['week'] = $ienquiryData['week'];
        $adminDataArr['ienquiry']['month'] = $ienquiryData['month'];
        // Get Institute Enquiry Data end


        // Get Student Complain Data
        $complainObj = new StudentComplain;
        $complainData = $complainObj->getComplainCount();
        $adminDataArr['complain']['total'] = $complainData['total'];
        $adminDataArr['complain']['today'] = $complainData['today'];
        $adminDataArr['complain']['week'] = $complainData['week'];
        $adminDataArr['complain']['month'] = $complainData['month'];
        // Get Student Complain end


        // Get Institute Data
        $instituteObj = new InstituteAdmin;
        $instituteData = $instituteObj->getInstituteCount();
        $adminDataArr['institute']['total'] = $instituteData['total'];
        $adminDataArr['institute']['today'] = $instituteData['today'];
        $adminDataArr['institute']['week'] = $instituteData['week'];
        $adminDataArr['institute']['month'] = $instituteData['month'];
        // Get Institute end

        // Get PCs count Data
        $pcsObj = new InstituteAdmin;
        $pcsData = $pcsObj->getPCsCount();
        $adminDataArr['pcs']['total'] = $pcsData['total'];
        $adminDataArr['pcs']['today'] = $pcsData['today'];
        $adminDataArr['pcs']['week'] = $pcsData['week'];
        $adminDataArr['pcs']['month'] = $pcsData['month'];
        // Get PCs count end

        // Get Payment Data
        $paymentObj = new StudentPayment;
        $paymentData = $paymentObj->getPaymentCount();
        $adminDataArr['payment']['total'] = $paymentData['total'];
        $adminDataArr['payment']['today'] = $paymentData['today'];
        $adminDataArr['payment']['week'] = $paymentData['week'];
        $adminDataArr['payment']['month'] = $paymentData['month'];
        // Get Payment end

        // Get Intitute Payment Data
        $ipaymentObj = new InstitutePayment;
        $ipaymentData = $ipaymentObj->getPaymentCount();
        $adminDataArr['ipayment']['total'] = $ipaymentData['total'];
        $adminDataArr['ipayment']['today'] = $ipaymentData['today'];
        $adminDataArr['ipayment']['week'] = $ipaymentData['week'];
        $adminDataArr['ipayment']['month'] = $ipaymentData['month'];
        // Get Intitute Payment end

        // Get Intitute Order Data
        $iorderObj = new InstituteOrder;
        $iorderData = $iorderObj->getInstituteOrder();
        if(isset($iorderData)){
            $orderData = [];
            foreach ($iorderData as $o => $order){
                $orderData[$o]['institute_name'] = $order->institute_name;
                $orderData[$o]['pc_count'] = $order->pc_count;
                $orderData[$o]['total_amt'] = $order->total_amt;
                $orderData[$o]['package_end'] = $order->package_end;
            }
        }
        $adminDataArr['institute_order'] = $orderData;

        // Get Intitute Order end

        // Get Student Sum of Revenue Data
        $totalpaymentObj = new StudentPayment;
        $examCountArr = $totalpaymentObj->getExamCount();
        $adminDataArr['exam_count']['total'] = $examCountArr['total'];
        $adminDataArr['exam_count']['today'] = $examCountArr['today'];
        $adminDataArr['exam_count']['week'] = $examCountArr['week'];
        $adminDataArr['exam_count']['month'] = $examCountArr['month'];

        $totalpaymentData = $totalpaymentObj->getTotalPaymentData();

        if(isset($totalpaymentData)){
           $inr_amount = $usd_amount = [];
           foreach ($totalpaymentData['total'] as $p => $payment){
                if($payment->currency == 'INR'){
                    $inr_amount[$p] = $payment->payment_amount;
                }else{
                    $usd_amount[$p] = $payment->payment_amount;
                }
           }

            $today_inr_amount = $today_usd_amount = [];
            foreach ($totalpaymentData['today'] as $t => $tpayment){
                if($tpayment->currency == 'INR'){
                    $today_inr_amount[$t] = $tpayment->payment_amount;
                }else{
                    $today_usd_amount[$t] = $tpayment->payment_amount;
                }
            }

            $week_inr_amount = $week_usd_amount = [];
            foreach ($totalpaymentData['week'] as $w => $wpayment){
                if($wpayment->currency == 'INR'){
                    $week_inr_amount[$w] = $wpayment->payment_amount;
                }else{
                    $week_usd_amount[$w] = $wpayment->payment_amount;
                }
            }

            $month_inr_amount = $month_usd_amount = [];
            foreach ($totalpaymentData['month'] as $m => $mpayment){
                if($mpayment->currency == 'INR'){
                    $month_inr_amount[$m] = $mpayment->payment_amount;
                }else{
                    $month_usd_amount[$m] = $mpayment->payment_amount;
                }
            }
        }

        $adminDataArr['total_payment']['inr'] = array_sum($inr_amount);
        $adminDataArr['total_payment']['usd'] = array_sum($usd_amount);
        $adminDataArr['total_payment']['today']['inr'] = array_sum($today_inr_amount);
        $adminDataArr['total_payment']['today']['usd'] = array_sum($today_usd_amount);
        $adminDataArr['total_payment']['week']['inr'] = array_sum($week_inr_amount);
        $adminDataArr['total_payment']['week']['usd'] = array_sum($week_usd_amount);
        $adminDataArr['total_payment']['month']['inr'] = array_sum($month_inr_amount);
        $adminDataArr['total_payment']['month']['usd'] = array_sum($month_usd_amount);
        // Get Student Sum of Revenue end

        // Get Student Sum of Revenue Data
        $totalinstitutepaymentObj = new InstitutePayment;
        $totalinstitutepaymentData = $totalinstitutepaymentObj->getTotalPaymentData();

        if(isset($totalinstitutepaymentData)){

            $intitute_inr_amount = $institute_usd_amount = [];
            foreach ($totalinstitutepaymentData['total'] as $i => $ipayment){
                if($ipayment->currency == 'INR'){
                    $intitute_inr_amount[$i] = $ipayment->payment_amount;
                }else{
                    $institute_usd_amount[$i] = $ipayment->payment_amount;
                }
            }

            $intitute_today_inr_amount = $institute_today_usd_amount = [];
            foreach ($totalinstitutepaymentData['today'] as $it => $itpayment){
                if($itpayment->currency == 'INR'){
                    $intitute_today_inr_amount[$it] = $itpayment->payment_amount;
                }else{
                    $institute_today_usd_amount[$it] = $itpayment->payment_amount;
                }
            }

            $intitute_week_inr_amount = $institute_week_usd_amount = [];
            foreach ($totalinstitutepaymentData['week'] as $iw => $iwpayment){
                if($iwpayment->currency == 'INR'){
                    $intitute_week_inr_amount[$iw] = $iwpayment->payment_amount;
                }else{
                    $institute_week_usd_amount[$iw] = $iwpayment->payment_amount;
                }
            }

            $intitute_month_inr_amount = $institute_month_usd_amount = [];
            foreach ($totalinstitutepaymentData['month'] as $im => $impayment){
                if($impayment->currency == 'INR'){
                    $intitute_month_inr_amount[$im] = $impayment->payment_amount;
                }else{
                    $institute_month_usd_amount[$im] = $impayment->payment_amount;
                }
            }
        }
        $adminDataArr['total_institute_payment']['inr'] = array_sum($intitute_inr_amount);
        $adminDataArr['total_institute_payment']['usd'] = array_sum($institute_usd_amount);

        $adminDataArr['total_institute_payment']['today']['inr'] = array_sum($intitute_today_inr_amount);
        $adminDataArr['total_institute_payment']['today']['usd'] = array_sum($institute_today_usd_amount);

        $adminDataArr['total_institute_payment']['week']['inr'] = array_sum($intitute_week_inr_amount);
        $adminDataArr['total_institute_payment']['week']['usd'] = array_sum($institute_week_usd_amount);

        $adminDataArr['total_institute_payment']['month']['inr'] = array_sum($intitute_month_inr_amount);
        $adminDataArr['total_institute_payment']['month']['usd'] = array_sum($institute_month_usd_amount);
        // Get Student Sum of Revenue end


        // Get Retail Students Data
        $studentObj = new RetailStudents;
        $studentData = $studentObj->getStudentCount();
        $adminDataArr['student']['total'] = $studentData['total'];
        $adminDataArr['student']['today'] = $studentData['today'];
        $adminDataArr['student']['week'] = $studentData['week'];
        $adminDataArr['student']['month'] = $studentData['month'];
        // Get Retail Students end

        // Get Retail Students Exam Data
        $studentmockObj = new StudentMockExam;
        $studentData = $studentmockObj->getCompleteMockExamCount();
        $adminDataArr['retail_mock']['total'] = $studentData['total'];
        $adminDataArr['retail_mock']['today'] = $studentData['today'];
        $adminDataArr['retail_mock']['week'] = $studentData['week'];
        $adminDataArr['retail_mock']['month'] = $studentData['month'];
        // Get Retail Students Exam data end

        // Get Retail Students  Completedt Exam Data
        $studentmockObj = new InstituteStudentMockExam;
        $studentData = $studentmockObj->getCompleteMockExamCount();
        $adminDataArr['institute_mock']['total'] = $studentData['total'];
        $adminDataArr['institute_mock']['today'] = $studentData['today'];
        $adminDataArr['institute_mock']['week'] = $studentData['week'];
        $adminDataArr['institute_mock']['month'] = $studentData['month'];
        // Get Retail Students Exam data end



        // Get Retail Students Exam Started Data
        $studentmockObj = new StudentMockExam;
        $studentData = $studentmockObj->getStartedMockExamCount();
        $adminDataArr['retail_mock_start']['total'] = $studentData['total'];
        $adminDataArr['retail_mock_start']['today'] = $studentData['today'];
        $adminDataArr['retail_mock_start']['week'] = $studentData['week'];
        $adminDataArr['retail_mock_start']['month'] = $studentData['month'];
        // Get Retail Students Exam data end

        // Instituite mock started
        $studentmockObj = new InstituteStudentMockExam;
        $studentData = $studentmockObj->getStartedMockExamCount();
        $adminDataArr['institute_mock_start']['total'] = $studentData['total'];
        $adminDataArr['institute_mock_start']['today'] = $studentData['today'];
        $adminDataArr['institute_mock_start']['week'] = $studentData['week'];
        $adminDataArr['institute_mock_start']['month'] = $studentData['month'];
        // Get Retail Students Exam data end


        return $adminDataArr;
    }

}