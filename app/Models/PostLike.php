<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class PostLike extends Model
{
    protected $table = 'tbl_post_like';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all PostLike getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return PostLike::get();
    }

    /**
     * Get all PostLike with role and ParentPostLike relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  PostLike::select('tbl_post_like.*');
    }

    /**
     * Query to get PostLike total count
     *
     * @param $dbObject
     * @return integer $PostLikeCount
     */
    public static function getPostLikeCount($dbObject)
    {
        $PostLikeCount = $dbObject->count();
        return $PostLikeCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetPostLikeData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.PostLikeFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/PostLike
     * get filterred PostLike
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortPostLikeData($query, $request)
    {
        return $query->orderBy(config('constant.PostLikeDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update PostLike addPostLike
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addPostLike(array $models = [])
    {
        $is_exist = PostLike::where("deleted_at",null)->where("user_id",$models['user_id'])->where("post_id",$models['post_id'])->first();
        if (isset($models['id']) || !empty($is_exist)) {
            $models['id'] = isset($models['id'])?$models['id']:$is_exist->id;
            $PostLike = PostLike::find($models['id']);
        } else {
            $PostLike = new PostLike;
            $PostLike->created_at = date('Y-m-d H:i:s');
        }
        $PostLike->user_id = $models['user_id'];
        $PostLike->status = $models['status'];
        $PostLike->post_id = $models['post_id'];
        $PostLike->updated_at = date('Y-m-d H:i:s');
        $PostLikeId = $PostLike->save();
        if ($PostLikeId) {
            $postObj =  new Post;
            $postData = $postObj->getPostByField($models['post_id'],'id');
            if($postData->user_id !=  $models['user_id']){
                $notificationArr['user_to'] = $postData->user_id;
                $notificationArr['user_from'] = $models['user_id'];
                $notificationArr['post_id'] = $models['post_id'];
                $notificationArr['description'] = Auth::user()->name.' like your post';
                $notificationArr['type'] = 3;
                $notiObj = new Notification;
                $notiObj->addNotification($notificationArr);
            }
            return $PostLike;
        } else {
            return false;
        }
    }

    /**
     * get PostLike By fieldname getPostLikeByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getPostLikeByField($id, $field_name)
    {
        return PostLike::where($field_name, $id)->first();
    }

    /**
     * Delete PostLike
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deletePostLike($id)
    {
        $delete = PostLike::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }

    /**
     * @desc discount price
     * @param $package
     * @param $exam
     * @return int
     */
    public function discountPrice($PostLike = '')
    {
        return PostLike::select('discount')->where('code',$PostLike)->first();
    }

    /**
     * @param $status
     * @return bool
     * @desc change the PostLike status
     */
    public function changeStatus($status)
    {
        $status = PostLike::where("deleted_at",null)->update(["status" => $status]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @return bool
     * @desc check status is active
     */
    public function checkStatus(){
        $status = PostLike::where("deleted_at",null)->where("status",1)->first();
        if (!empty($status))
            return true;
        else
            return false;
    }


    public function getLikeCountByPostId($post_id){
        return PostLike::where("deleted_at",null)->where("post_id",$post_id)->where("status",1)->count();
    }

    public function getDislikeCountByPostId($post_id){
        return PostLike::where("deleted_at",null)->where("post_id",$post_id)->where("status",2)->count();
    }

    public function checkUserStatus($user_id,$post_id){
        return PostLike::where("deleted_at",null)->where("user_id",$user_id)->where("post_id",$post_id)->first();
    }

}
