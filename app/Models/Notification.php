<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class Notification extends Model
{
    protected $table = 'tbl_notification';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];


    /**
     * Get all Notification getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return Notification::get();
    }

    /**
     * Get all Notification with role and ParentNotification relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return  Notification::select('tbl_notification.*');
    }

    /**
     * Query to get Notification total count
     *
     * @param $dbObject
     * @return integer $NotificationCount
     */
    public static function getNotificationCount($dbObject)
    {
        $NotificationCount = $dbObject->count();
        return $NotificationCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetNotificationData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.NotificationFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/Notification
     * get filterred Notification
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortNotificationData($query, $request)
    {
        return $query->orderBy(config('constant.NotificationDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);
    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update Notification addNotification
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addNotification(array $models = [])
    {
        if (isset($models['id'])) {
            $Notification = Notification::find($models['id']);

        } else {
            $Notification = new Notification;
            $Notification->created_at = date('Y-m-d H:i:s');
        }
        $Notification->user_from = $models['user_from'];
        $Notification->user_to = $models['user_to'];
        $Notification->post_id = isset($models['post_id'])?$models['post_id']:0;
        $Notification->description = isset($models['description'])?$models['description']:'';
        $Notification->type = isset($models['type'])?$models['type']:0;//0-other, 1- comment,2 - reply,3 - like
        $Notification->updated_at = date('Y-m-d H:i:s');
        $NotificationId = $Notification->save();
        if ($NotificationId) {
            return $Notification;
        } else {
            return false;
        }
    }

    /**
     * get Notification By fieldname getNotificationByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getNotificationByField($id, $field_name)
    {
        return Notification::where($field_name, $id)->first();
    }

    /**
     * Delete Notification
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deleteNotification($id)
    {
        $delete = Notification::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }


    /**
     * @param $status
     * @return bool
     * @desc change the Notification status
     */
    public function changeStatus($models = [])
    {
        $status = Notification::where("deleted_at",null)->where("id",$models['id'])->update(["is_read" => $models['is_read']]);
        if ($status)
            return true;
        else
            return false;

    }

    /**
     * @param $status
     * @return bool
     * @desc change the Notification status
     */
    public function getNotificationforTop($user_id)
    {
        return  Notification::leftjoin('users', 'tbl_notification.user_from', '=', 'users.id')
            ->select('tbl_notification.*','users.name as username','users.username as user_name','users.profile_image')
            ->where("tbl_notification.deleted_at",null)->where("tbl_notification.user_to",$user_id)->orderBy('tbl_notification.created_at','DESC')->limit(3)->get();
    }

    public function getUnreadCount($user_id)
    {
        return  Notification::where("tbl_notification.deleted_at",null)->where("tbl_notification.is_read",0)->where("tbl_notification.user_to",$user_id)->count();
    }

    public function getNotification($user_id){
        $pageLimit =config('services.pagination.notification');
        return  Notification::leftjoin('users', 'tbl_notification.user_from', '=', 'users.id')
            ->select('tbl_notification.*','users.name as username','users.username as user_name','users.profile_image')
            ->where("tbl_notification.deleted_at",null)->where("tbl_notification.user_to",$user_id)
            ->orderBy('tbl_notification.id','desc')->paginate($pageLimit);
    }

    /**
     * @param array $where
     * @param array $update
     * @return mixed
     * @desc update the data as per requirement
     */
    public function updateData($where = array(),$update = array()){
        return Notification::where($where)->update($update);
    }
}
