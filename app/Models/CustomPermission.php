<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class CustomPermission extends Model
{

    protected $table = 'permissions';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
    /**
     * Get all Permission getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return CustomPermission::get();
    }

    /**
     * Get all Permission with role and ParentPermission relationship
     *
     * @return mixed
     */
    public function getDatatableCollection()
    {
        return CustomPermission::select('permissions.*');
    }

    /**
     * Query to get permission total count
     *
     * @param $dbObject
     * @return integer $permissionCount
     */
    public static function getPermissionCount($dbObject)
    {
        $permissionCount = $dbObject->count();
        return $permissionCount;
    }

    /**
     * Scope a query to get all data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetPermissionData($query, $request)
    {
        return $query->skip($request->start)->take($request->length)->get(config('constant.permissionFieldArray'));
    }

    /**
     * scopeGetFilteredData from App/Models/Permission
     * get filterred permission
     *
     * @param  object $query
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function scopeGetFilteredData($query, $request)
    {
        $filter = $request->filter;
        $Datefilter = $request->filterDate;
        $Datefilter1 = $request->filterDate1;
        $filterSelect = $request->filterSelect;

        /**
         * @param string $filter  text type value
         * @param string $Datefilter  date type value
         * @param string $filterSelect select value
         *
         * @return mixed
         */
        return $query->Where(function ($query) use ($filter, $Datefilter, $filterSelect,$Datefilter1) {
            if (count((array)$filter) > 0) {
                foreach ($filter as $key => $value) {
                    if ($value != "") {
                        $query->where($key, 'LIKE', '%' . trim($value) . '%');
                    }
                }
            }

            /* if (count($Datefilter) > 0) {
                 foreach ($Datefilter as $dtkey => $dtvalue) {
                     if ($dtvalue != "") {
                         $query->where($dtkey, 'LIKE', '%' . date('Y-m-d', strtotime(trim($dtvalue))) . '%');
                     }
                 }
             }*/

            if (count((array)$Datefilter) > 0) {
                foreach ($Datefilter as $dtkey => $dtvalue) {
                    foreach ($Datefilter1 as $dtvalue1){
                        if ($dtvalue != "" && $dtvalue1 !="") {
                            $start_date = date('Y-m-d 00:00:00', strtotime(trim($dtvalue)));
                            $end_date = date('Y-m-d 23:59:59', strtotime(trim($dtvalue1)));
                            $query->whereBetween($dtkey,[$start_date,$end_date]);
                        }
                    }
                }
            }

            if (count((array)$filterSelect) > 0) {
                foreach ($filterSelect as $Sekey => $Sevalue) {
                    if ($Sevalue != "") {
                        $query->whereRaw('FIND_IN_SET(' . trim($Sevalue) . ',' . $Sekey . ')');
                    }
                }
            }

        });

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortPermissionData($query, $request)
    {

        return $query->orderBy(config('constant.permissionDataTableFieldArray')[$request->order['0']['column']], $request->order['0']['dir']);

    }

    /**
     * Scope a query to sort data
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $column
     * @param  string $dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortDefaultDataByRaw($query, $column, $dir)
    {
        return $query->orderBy($column, $dir);
    }

    /**
     * Add & update Permission addPermission
     *
     * @param array $models
     * @return boolean true | false
     */
    public function addPermission(array $models = [])
    {
        if (isset($models['id'])) {
            $permission = CustomPermission::find($models['id']);
            $permission->name = $models['name'];
            $permission->model_name = $models['model_name'];
            $permission->model_slug = $models['model_slug'];
            $permission->save();
        } else {
            $permission = new CustomPermission;
            $permission->created_at = date('Y-m-d H:i:s');
            $model_slug = strtolower($models['model_name']);
            $list   = $model_slug.'-list';
            $create = $model_slug.'-create';
            $edit   = $model_slug.'-edit';
            $delete = $model_slug.'-delete';
            $permissionArray = [$list,$create,$edit,$delete];
            $count = 0;
            foreach ($permissionArray as $value){
                $count++;
                $permission = new CustomPermission;
                $permission->name = $value;
                $permission->guard_name = 'admin';
                $permission->model_name = $models['model_name'];
                $permission->model_slug = $model_slug;
                $permission->created_at = date('Y-m-d H:i:s');
                $permission->save();
            }
        }
        $permissionId = $permission->id;
        if ($permissionId) {
            return $permission;
        } else {
            return false;
        }
    }

    /**
     * get Permission By fieldname getPermissionByField
     *
     * @param mixed $id
     * @param string $field_name
     * @return mixed
     */
    public function getPermissionByField($id, $field_name)
    {
        return CustomPermission::where($field_name, $id)->first();
    }

    /**
     * update Permission Status
     *
     * @param array $models
     * @return boolean true | false
     */
    public function updateStatus(array $models = [])
    {
        $permission = CustomPermission::find($models['id']);
        $permission->status = $models['status'];
        $permission->updated_at = date('Y-m-d H:i:s');
        $permissionId = $permission->save();
        if ($permissionId)
            return true;
        else
            return false;

    }

    /**
     * Delete Permission
     *
     * @param int $id
     * @return boolean true | false
     */
    public function deletePermission($id)
    {
        $delete = CustomPermission::where('id', $id)->delete();
        if ($delete)
            return true;
        else
            return false;

    }



}
