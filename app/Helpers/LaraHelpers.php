<?php

namespace App\Helpers;

use App\Mail\SendPrivateMail;
use App\Models\Action;
use File;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Storage;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Filesystem\Filesystem;
use App\Models\Country;
use App\Models\State;
use App\Models\ConnectionRequest;
use App\Models\User;
use Log;

class LaraHelpers
{

    /*
     *  Upload file
     *
     *  @param string $filepath
     *  @param array $image_name
     *  @param mixed $unlink_image
     *  @return mixed
     * */
    public static function upload_image_local($filepath, $image_name, $unlink_image = '')
    {
        if (!is_dir($filepath)) {
            File::makeDirectory($filepath, 755,true,true);
        }
        if ($image_name != "") {

            $file = $image_name;
            $extension = "";
            $extension = '.' . $file->getClientOriginalExtension();
            $file_name = rand(1,100000000000);
            $filename = $file_name.$extension;
            $publicPath = $filepath;
            $file->move($publicPath, $filename);

            if (isset($unlink_image) && $unlink_image != "") {
                if(file_exists($filepath . $unlink_image)){
                    unlink($filepath . $unlink_image);
                }
            }
            return $filename;
        }
        return $unlink_image;
    }


    /*
     *  Upload file in sS3 Bucket
     *
     *  @param string $filepath
     *  @param array $image_name
     *  @param mixed $unlink_image
     *  @return mixed
     * */
    public static function upload_image($filepath, $image_name, $unlink_image = '')
    {
        if ($image_name != "") {

            $file = $image_name;
            $extension = '.' . $file->getClientOriginalExtension();
            $file_name = rand(1,100000000000);
            $filename = $file_name.$extension;
            $s3 = \Storage::disk('s3');
            $kk =  $s3->put($filepath.$filename, file_get_contents($file), 'public');
            if($kk){
                return $filename;
            }else{
                return $unlink_image;
            }
        }
        return $unlink_image;
    }

        /*
     *  Remove file in sS3 Bucket
     *
     *  @param string $filepath
     *  @param array $image_name
     *  @param mixed $unlink_image
     *  @return mixed
     * */
    public static function remove_image($filepath)
    {
        if ($filepath != "") {
            if(Storage::disk('s3')->exists($filepath)) {
                Storage::disk('s3')->delete($filepath);
            }
        }
    }

        /**
     * get data using Ip by package
     * @param string $ip
     * @return mixed
     */

    public static function getDataByIp($ip = ''){

        $state_id = $country_id = 1;
        $city = '';
        $locationArr = [];
        if($ip == ''){
            if (!empty($_SERVER['HTTP_CLIENT_IP']))
            {
                $client_ip = $_SERVER['HTTP_CLIENT_IP'];
            }
            //whether ip is from proxy
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            {
                $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            //whether ip is from remote address
            else
            {
                $client_ip = $_SERVER['REMOTE_ADDR'];
            }
            $getLocation = \Location::get($client_ip);
        }else{
            $client_ip = $ip;// for remote check
            $getLocation = \Location::get($client_ip);
        }
        if(!empty($getLocation)){
            $countryObj = new Country;
            $country = $countryObj->getCollectionByField('code',$getLocation->countryCode);
            if(!empty($country)){
                $country_id = $country->id;
                $stateObj = new State;
                $state = $stateObj->getCollectionByField('name',$getLocation->regionName);
                if(!empty($state)){
                    $state_id = $state->id;
                }else{
                    $state_id = 32;
                }

            }else{
                $country_id = 101; // india
                $state_id = 1;
            }
            if($getLocation->countryCode == 'IN'){
                $locationArr['currency'] = 'INR';
            }else{
                $locationArr['currency'] = 'USD';
            }

            $locationArr['country'] =$getLocation->countryName;
            $locationArr['country_id'] =$country_id;
            $locationArr['country_code'] =$getLocation->countryCode;
            $locationArr['state'] = $getLocation->regionName;
            $locationArr['state_id'] = $state_id;
            $locationArr['city'] = $getLocation->cityName;
            $locationArr['ip'] = $client_ip;
        }else{
            $locationArr['country'] = 'India';
            $locationArr['country_id'] =101;
            $locationArr['country_code'] = 'IN';
            $locationArr['state'] = 'Andaman and Nicobar Islands';
            $locationArr['state_id'] = 1;
            $locationArr['city'] = 'Sawai';
            $locationArr['ip'] = '139.59.17.55';

        }
        return $locationArr;

    }


    public static function friendConnected(){
        $conn = new ConnectionRequest;
        $user_ids = $conn->getUserFriend();
        $user = new User;
        $userdata = $user->getUserData($user_ids);
        return $userdata;
    }
}