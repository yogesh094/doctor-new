<?php

function pp($data){
    echo "<prE>";
    print_r($data);
    exit;
}

function jsonToArray($data){
    return json_decode($data,true);
}

function strlower($str){
    return strtolower($str);
}

function create_slug($string){
    $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    return $slug;
}

function getNameById($id){
   $getData =  App\Models\User::where('id',$id)->select('name','username','profile_image')->first();
   if(!empty($getData)){
        return $getData;
   }
   return false;
}

function getConnectionCount(){
    $request =  new App\Models\ConnectionRequest;
    $dataCount = $request->getConnectionCount();
    if(!empty($dataCount)){
        return $dataCount;
    }
    return 0;
}

function getPostCount(){
    $user_id = Auth::user()->id;
    $post =  new App\Models\Post;
    $dataCount = $post->getTotalPostCount($user_id);
    if(!empty($dataCount)){
        return $dataCount;
    }
    return 0;
}



/**
 * @return string
 * @desc Create random password
 */
function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function get_client_data()
{
    $clientData = [];
    $PublicIP = $_SERVER['REMOTE_ADDR'];
    $json     = file_get_contents("http://ipinfo.io/$PublicIP/geo");
    $json     = json_decode($json, true);

    $clientData['country']  = $country  = $json['country'];
    $clientData['region'] = $region   = $json['region'];
    $clientData['city'] = $city     = $json['city'];
    $clientData['ip'] = $PublicIP;

    if(!empty($clientData) && $clientData['country'] == 'IN'){
        $clientData['currency'] = 'INR';
    }else{
        $clientData['currency'] = 'USD';
    }

    return $clientData;
}

function time_ago($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function foo($seconds) {
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
}


function getInstituteData($id){

    $select = ['logo','name'];
    $instituteObj = App\Models\InstituteAdmin::getSelectedData($id,$select);
}

function utctoIndia($gmdate){
    return date('Y-m-d H:i:s',strtotime('+330 minutes', strtotime($gmdate)));
}

function getCurrencyIcon($currency = 'USD'){
    if($currency == 'INR'){
        echo '₹';
    }else{
        echo '$';
    }
}

function getProfileTypeName($type_id){
    if($type_id == '1'){
        return 'UG Medical Student';
    }elseif($type_id == '2'){
        return 'HealthCare Institute';
    }else{
        return 'Doctor / PG Student';
    }
}

/**
 * @param $curlURL
 * @param array $payload
 * @param string $method
 * @return bool|string
 * @desc Api for affiliate
 */
function affiliateCurlCall($curlURL, $payload=array(),$method = 'POST'){
    $ch = curl_init();
    $url=$curlURL;
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_HTTPHEADER => FALSE,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => json_encode($payload),
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "api-token: @ffili@te",
        ),
    ));
    $response = curl_exec($ch);
    return $response;
}

function does_url_exists($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($code == 200) {
        $status = true;
    } else {
        $status = false;
    }
    curl_close($ch);
    return $status;
}

function searchFriend(){
    $user = new App\Models\User;
    $data = $user->getSpecificUserData(['username','name']);
    $userDataMain = [];
    $userData = [];
    if(!empty($data)){
        foreach ($data as $key => $value) {
            $userData['label'] = $value->name;
            $userData['value'] = $value->username;
            $userDataMain[] = $userData;
        }
    }
    return json_encode($userDataMain);
}

?>
