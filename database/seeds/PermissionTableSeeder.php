<?php

use Illuminate\Database\Seeder;
use App\Models\CustomPermission;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder {

	public function run()
	{
		DB::table('permissions')->delete();

        $modules = ['User','Role','Permission'];
        $type = ['list','create','edit','delete'];

        foreach($modules as $m){
            foreach($type as $t){
                $slug = strtolower($m);
                $table = (array(
                    'name' => $slug."-".$t,
                    'guard_name' => 'admin',
                    'model_slug' => $slug,
                    'model_name' => $m
                ));
                DB::table('permissions')->insert($table);
            }
        }
	}
}
