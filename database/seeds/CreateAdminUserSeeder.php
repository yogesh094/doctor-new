<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\AdminUser;
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $role = Role::create(['name' => 'Admin','guard_name' => 'admin']);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user = AdminUser::create(array(
            'name' => 'Super Admin',
            'username' => 'admin',
            'password' => bcrypt('123456789'),
            'email' => 'admin@doctor.com',
            'position' => 'Super Admin',
            'status' => 1
        ));
        $user->assignRole('Admin');

    }
}