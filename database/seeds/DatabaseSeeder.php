<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder {

	public function run()
	{
		$this->call([
                CountryTableSeeder::class,
                StateTableSeeder::class,
                PermissionTableSeeder::class,
                CreateAdminUserSeeder::class,
        ]);
	}
}
