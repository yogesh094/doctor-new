<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtherFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username', 50)->unique()->nullable()->after('name');
            $table->string('phone_code', 10)->nullable()->after('password');
            $table->string('mobile_no', 20)->nullable()->after('phone_code');
            $table->tinyInteger('gender')->nullable()->comment('1=>Male,2=>Female')->after('mobile_no');
            $table->date('dob')->nullable()->after("gender");
            $table->string('profile_image', 100)->nullable()->after("gender");
            $table->string('cover_image', 100)->nullable()->after("profile_image");
            $table->integer('country')->unsigned()->index()->default('1')->after("cover_image");
            $table->integer('state')->unsigned()->index()->default('1')->after("country");
            $table->string('city',100)->nullable()->after("state");
            $table->integer('profile_type')->nullable()->after("city");
            $table->string('provider',25)->nullable()->after("profile_type");
            $table->string('provider_id',250)->nullable()->after("provider");
            $table->longtext('description')->nullable()->after("provider_id");
            $table->tinyInteger('is_login')->default('0')->after("description");
            $table->tinyInteger('status')->default('0')->after("is_login");
              $table->softDeletes();
        });

        Schema::table('users', function(Blueprint $table) {
        $table->foreign('country')->references('id')->on('tbl_country')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
        Schema::table('users', function(Blueprint $table) {
            $table->foreign('state')->references('id')->on('tbl_states')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_country_foreign');
        });
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_state_foreign');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['username','phone_code','mobile_no',
                'gender','dob','profile_image','country','state','city','cover_image','is_login','status','provider','provider_id','description','profile_type','deleted_at']);
        });
    }
}
