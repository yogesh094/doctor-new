<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblPostReply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tbl_post_reply', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index()->comment("reply user id");
            $table->integer('post_id')->unsigned()->index();
            $table->integer('post_comment_id')->unsigned()->index();
            $table->longtext('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tbl_post_reply', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('tbl_post_reply', function(Blueprint $table) {
            $table->foreign('post_comment_id')->references('id')->on('tbl_post_comment')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('tbl_post_reply', function(Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('tbl_post')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_post_reply', function(Blueprint $table) {
            $table->dropForeign('tbl_post_reply_user_id_foreign');
        });
        Schema::table('tbl_post_reply', function(Blueprint $table) {
            $table->dropForeign('tbl_post_reply_post_comment_id_foreign');
        });
        Schema::table('tbl_post_reply', function(Blueprint $table) {
            $table->dropForeign('tbl_post_reply_post_id_foreign');
        });
        Schema::dropIfExists('tbl_post_reply');
    }
}
