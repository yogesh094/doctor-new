<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblPostLikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_post_like', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index()->comment("user id");
            $table->integer('post_id')->unsigned()->index();
            $table->tinyInteger('status')->default(0)->comment('0=>no response,1=>like,2=>dislike');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tbl_post_like', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('tbl_post_like', function(Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('tbl_post')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_post_like', function(Blueprint $table) {
            $table->dropForeign('tbl_post_like_user_id_foreign');
        });
        Schema::table('tbl_post_like', function(Blueprint $table) {
            $table->dropForeign('tbl_post_like_post_id_foreign');
        });
        Schema::dropIfExists('tbl_post_like');
    }
}
