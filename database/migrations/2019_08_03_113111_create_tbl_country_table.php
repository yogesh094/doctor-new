<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblCountryTable extends Migration {

	public function up()
	{
		Schema::create('tbl_country', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
		        $table->string('code', 10);
            		$table->integer('phonecode')->default('0');
			$table->tinyInteger('status')->default('0');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('tbl_country');
	}
}
