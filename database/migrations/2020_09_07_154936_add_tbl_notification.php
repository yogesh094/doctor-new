<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTblNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_notification', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_from')->index();
            $table->unsignedBigInteger('user_to')->index();
            $table->tinyInteger('post_id')->default('0')->comment("0->no post,1->for post");
            $table->integer('type')->default('0')->comment("0->other,1->comment,2->reply,3->like");
            $table->longtext('description')->nullable();
            $table->tinyInteger('is_read')->default('0')->comment("0->no,1->yes");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tbl_notification', function(Blueprint $table) {
            $table->foreign('user_from')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('tbl_notification', function(Blueprint $table) {
            $table->foreign('user_to')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_notification', function(Blueprint $table) {
            $table->dropForeign('tbl_notification_user_from_foreign');
        });
        Schema::table('tbl_notification', function(Blueprint $table) {
            $table->dropForeign('tbl_notification_user_to_foreign');
        });
        Schema::dropIfExists('tbl_notification');
    }
}
