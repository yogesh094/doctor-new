<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblStatesTable extends Migration {

	public function up()
	{
		Schema::create('tbl_states', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('country_id')->unsigned()->index();
			$table->string('name', 50);
			$table->tinyInteger('status')->default('0');
			$table->timestamps();
			$table->softDeletes();
		});
        Schema::table('tbl_states', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('tbl_country')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{

        Schema::table('tbl_states', function(Blueprint $table) {
			$table->dropForeign('tbl_states_country_id_foreign');
		});
        Schema::drop('tbl_states');
	}
}