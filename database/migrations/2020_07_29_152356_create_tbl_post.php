<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
        public function up()
    {
        Schema::create('tbl_post', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->string('media',150)->nullable();
            $table->tinyInteger('media_type')->default('0')->comment("0->text,1->image,2->video");
            $table->longtext('description')->nullable();
            $table->tinyInteger('status')->default('0')->comment("1->active");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tbl_post', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_post', function(Blueprint $table) {
            $table->dropForeign('tbl_post_user_id_foreign');
        });
        Schema::dropIfExists('tbl_post');
    }
}
