<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblConnectionRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_connection_request', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('request_from')->index();
            $table->unsignedBigInteger('request_to')->index();
            $table->tinyInteger('type')->default('0')->comment("0->pending,1->accepted,2->rejected");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tbl_connection_request', function(Blueprint $table) {
            $table->foreign('request_from')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('tbl_connection_request', function(Blueprint $table) {
            $table->foreign('request_to')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_connection_request', function(Blueprint $table) {
            $table->dropForeign('tbl_connection_request_request_from_foreign');
        });
        Schema::table('tbl_connection_request', function(Blueprint $table) {
            $table->dropForeign('tbl_connection_request_request_to_foreign');
        });
        Schema::dropIfExists('tbl_connection_request');
    }
}
