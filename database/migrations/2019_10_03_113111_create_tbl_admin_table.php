<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblAdminTable extends Migration {

	public function up()
	{
		Schema::create('tbl_admin', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('username', 50)->unique();
			$table->string('password', 100);
			$table->string('email', 50)->unique();
			$table->string('position', 50)->nullable();
			$table->tinyInteger('status')->default('0');
			$table->string('remember_token', 250)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('tbl_admin');
	}
}