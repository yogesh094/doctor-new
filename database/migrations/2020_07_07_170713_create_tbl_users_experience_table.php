<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblUsersExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_users_experience', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->string('title', 250)->nullable();
            $table->tinyInteger('employment_type')->nullable()->comment('1 => Full Time, 2 => Part Time');
            $table->string('company', 500)->nullable();
            $table->string('location', 500)->nullable();
            $table->tinyInteger('is_currently_working')->default('0');
            $table->string('start_month', 10)->nullable();
            $table->string('start_year', 10)->nullable();
            $table->string('end_month', 10)->nullable();
            $table->string('end_year', 10)->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tbl_users_experience', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_users_experience', function(Blueprint $table) {
            $table->dropForeign('tbl_users_experience_user_id_foreign');
        });
        Schema::dropIfExists('tbl_users_experience');
    }
}
