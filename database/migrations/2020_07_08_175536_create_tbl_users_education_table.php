<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblUsersEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_users_education', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->string('title', 250)->nullable();
            $table->string('degree', 250)->nullable();
            $table->string('field_of_study', 250)->nullable();
            $table->string('grade', 250)->nullable();
            $table->string('start_year', 10)->nullable();
            $table->string('end_year', 10)->nullable();
            $table->longText('extra_activities')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tbl_users_education', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_users_education', function(Blueprint $table) {
            $table->dropForeign('tbl_users_education_user_id_foreign');
        });
        Schema::dropIfExists('tbl_users_education');
    }
}
