@if($module == 'pendingresult')
<div style="cursor: pointer">
    @if($status == 1)
        <span class="badge badge-round badge-success badge-lg switch-button" formaction="{{$url}}" rel="0" id="{{$id}}">Yes</span>
    @else
        <span class="badge badge-round badge-default badge-lg switch-button" formaction="{{$url}}" rel="1" id="{{$id}}">No</span>
    @endif
</div>
@else
<div style="cursor: pointer">
    @if($status == 1)
        <span class="badge badge-round badge-success badge-lg switch-button" formaction="{{$url}}" rel="0" id="{{$id}}">Active</span>
    @else
        <span class="badge badge-round badge-default badge-lg switch-button" formaction="{{$url}}" rel="1" id="{{$id}}">Inactive</span>
    @endif
</div>
@endif




