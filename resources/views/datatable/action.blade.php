{{-- Edit Action--}}
@can($module.'-edit')
    @if($module == 'jobhistory' || $module == 'pendingresult' || $module == 'institutependingresult')

    @else
        @if($module == 'instituteorder' && $status == 0)
            <a href="{{url('/'.$url.'/edit/'.$id)}}" title="Edit Details" class="btn btn-icon btn-primary btn-outline">
                <i class="icon wb-pencil" aria-hidden="true"></i>
            </a>
        @elseif($module != 'instituteorder')
            <a href="{{url('/'.$url.'/edit/'.$id)}}" title="Edit Details" class="btn btn-icon btn-primary btn-outline">
                <i class="icon wb-pencil" aria-hidden="true"></i>
            </a>
        @endif
    @endif

@endcan

{{--Delete Action--}}
@can($module.'-delete')

    @if($module == 'pendingresult' || $module == 'institutependingresult')

    @else
        @if($module == 'jobhistory')
            <a href="javascript:void(0);" class="deleteRecord btn btn-icon btn-warning btn-outline" title="Delete" formaction="{{$url}}" rel="{{$id}}">
                <i class="icon wb-trash" aria-hidden="true"></i>
            </a>
        @else
            <a href="javascript:void(0);" class="deleteRecord btn btn-icon btn-warning btn-outline" title="Delete" formaction="{{$url}}" rel="{{$id}}">
                <i class="icon wb-trash" aria-hidden="true"></i>
            </a>
        @endif
    @endif

@endcan

{{--Institute Admin--}}
@if($module == 'instituteadmin')
    <a href="{{ $viewUrl }}" class="btn btn-icon btn-info btn-outline" title="View Details"><i class="icon wb-eye" aria-hidden="true"></i></a>
    <a href="{{ $magicLoginUrl }}" class="btn btn-icon btn-dark btn-outline" title="Login" target="_blank"><i class="icon fa-key" aria-hidden="true"></i></a>
    <a href="{{ $usageUrl }}" class="btn btn-icon btn-primary btn-outline" title="Usage" target="_blank">View Usage</a>
@endif


{{-- Mock and Practice Exam--}}
@if($module == 'mockexam' || $module == 'practiceexam')
    <a href="{{ $questionUrl }}" class="btn btn-primary"
       style="margin-left: 10px;"><i class="icon wb-edit" aria-hidden="true"></i>Questions</a>
@endif

{{-- Materials--}}
@if($module == 'materials')
    <a href="{{ $slideUrl }}" class="btn btn-primary"
       style="margin-left: 10px;"><i class="icon wb-edit" aria-hidden="true"></i>Add Slide</a>

    <a href="{{ $viewUrl }}" class="btn btn-primary"
       style="margin-left: 10px;" target="_blank"><i class="icon wb-eye" aria-hidden="true"></i>View</a>
@endif


@if($module == 'faqcategory')
    <a href="{{ $contentUrl }}" class="btn btn-primary"
       style="margin-left: 10px;"><i class="icon wb-edit" aria-hidden="true"></i>Add Content</a>
@endif

@if($module == 'faqcontent')
    <a href="{{ $contentUrl }}" class="btn btn-primary"
       style="margin-left: 10px;"><i class="icon wb-edit" aria-hidden="true"></i>Add SubContent</a>
@endif

{{-- Single Institute Students--}}
@if($module == 'singleinstitutestudents')
    <a href="{{ $magicLoginUrl }}" class="btn btn-icon btn-dark btn-outline" title="Login" target="_blank"><i class="icon fa-key" aria-hidden="true"></i></a>
    @if($is_login == 1)
        <a href="{{ $logoutUrl }}" class="btn btn-danger" title="Logout"><i class="icon wb-power mr-10" aria-hidden="true"></i>Logout</a>
    @endif

@endif


{{-- Retail Student--}}
@if($module == 'retailstudent')
    <a href="{{ $magicLoginUrl }}" class="btn btn-icon btn-dark btn-outline" title="Login" target="_blank"><i class="icon fa-key" aria-hidden="true"></i></a>
    <a href="{{ $examUrl }}" class="btn btn-icon btn-dark btn-outline" title="Exam-list" target="_blank"><i class="icon wb-rubber" aria-hidden="true"></i></a>
@endif


{{-- Institute Student--}}
@if($module == 'institutestudents')
    <a href="{{ $magicLoginUrl }}" class="btn btn-icon btn-dark btn-outline" title="Login" target="_blank"><i class="icon fa-key" aria-hidden="true"></i></a>
    <a href="{{ $examUrl }}" class="btn btn-icon btn-dark btn-outline" title="Exam-list" target="_blank"><i class="icon wb-rubber" aria-hidden="true"></i></a>
@endif

{{-- Job History --}}
@if($module == 'jobhistory')
    <a href="{{ $requeuejobUrl }}" class="btn btn-icon btn-dark btn-outline" title="Requeue-job" ><i class="icon wb-loop" aria-hidden="true"></i></a>
@endif

{{-- Retail Student--}}
@if($module == 'pendingresult' || $module == 'institutependingresult')
    <a href="{{ $magicLoginUrl }}" class="btn btn-icon btn-dark btn-outline" title="Login" target="_blank"><i class="icon fa-key" aria-hidden="true"></i></a>
    <a href="javascript:void(0);" class="deleteRecord btn btn-icon btn-warning btn-outline" title="Delete Review" formaction="{{$deleteUrl}}" rel="{{$student_mockexam_id}}">
        <i class="icon wb-trash" aria-hidden="true"></i>
    </a>
@endif

@if($module == 'pendingpracticeresult' || $module == 'institutependingpracticeresult')
    <a href="{{ $magicLoginUrl }}" class="btn btn-icon btn-dark btn-outline" title="Login" target="_blank"><i class="icon fa-key" aria-hidden="true"></i></a>
@endif
