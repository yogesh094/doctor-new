<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('app.app_name') }} - Login</title>
    <link rel='shortcut icon' href='{{ config('services.website.favicon') }}' type='image/x-icon' />
    <!-- Stylesheets -->

    <link rel="stylesheet" href="{{url('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/site.min.css')}}">

    <!-- Plugins -->

    <link rel="stylesheet" href="{{url('global/vendor/animsition/animsition.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/intro-js/introjs.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="{{url('assets/examples/css/pages/login-v3.css')}}">

    <!-- DataTable CSS Ends-->

    <!-- Fonts -->
    <link rel="stylesheet" href="{{url('global/fonts/web-icons/web-icons.min.css')}}">
    <link rel="stylesheet" href="{{url('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>


    <!-- Scripts -->
    <script src="{{url('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
        Breakpoints();
    </script>

@stack('externalCssLoad')
@stack('internalCssLoad')

<!-- Scripts -->
    <script type="text/javascript">
        var baseUrl = "{{ url('/') }}/";
        var csrf_token = "<?php echo csrf_token(); ?>";

        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body class="animsition page-login-v3 layout-full">

<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
        <div class="panel">
            <div class="panel-body">
                <div class="brand">
                    {{--<img class="brand-img" src="../../assets//images/logo-colored.png" alt="...">--}}
                    <h2 class="brand-text font-size-18">Doctor Admin Panel</h2>
                </div>
                @if (Session::has('message'))
                    <div class="alert alert-danger">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif
                <form method="POST" action="{{ route('admin-check-login') }}">
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input id="login_name" type="text" class="form-control @error('login_name') is-invalid @enderror" name="login_name" value="{{ old('login_name') }}" required autocomplete="login_name" autofocus>
                        <label for="email" class="floating-labe">{{ __('E-Mail Address OR UserName') }}</label>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <label for="password" class="floating-label">{{ __('Password') }}</label>
                    </div>
                    <div class="form-group clearfix">
                        <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    @csrf
                    <button type="submit" class="btn btn-primary btn-block btn-lg mt-40">
                        {{ __('Login') }}
                    </button>
                </form>
            </div>
        </div>

        <footer class="page-copyright page-copyright-inverse">
        </footer>
    </div>
</div>

<script src="{{url('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="{{url('global/vendor/jquery/jquery.js')}}"></script>
<script src="{{url('global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="{{url('global/vendor/bootstrap/bootstrap.js')}}"></script>
<script src="{{url('global/vendor/animsition/animsition.js')}}"></script>
<script src="{{url('global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{url('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{url('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="{{url('global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>

<!-- Plugins -->
<script src="{{url('global/vendor/switchery/switchery.js')}}"></script>
<script src="{{url('global/vendor/intro-js/intro.js')}}"></script>
<script src="{{url('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{url('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{url('global/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>


<!-- Scripts -->
<script src="{{url('global/js/Component.js')}}"></script>
<script src="{{url('global/js/Plugin.js')}}"></script>
<script src="{{url('global/js/Base.js')}}"></script>
<script src="{{url('global/js/Config.js')}}"></script>

<script src="{{url('assets/js/Section/Menubar.js')}}"></script>
<script src="{{url('assets/js/Section/GridMenu.js')}}"></script>
<script src="{{url('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{url('assets/js/Section/PageAside.js')}}"></script>
<script src="{{url('assets/js/Plugin/menu.js')}}"></script>

<script src="{{url('global/js/config/colors.js')}}"></script>
<script src="{{url('assets/js/config/tour.js')}}"></script>
<script>Config.set('assets', '../assets');</script>
<!-- Page -->
<script src="{{url('assets/js/Site.js')}}"></script>
<script src="{{url('global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{url('global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{url('global/js/Plugin/switchery.js')}}"></script>
<script src="{{url('global/js/Plugin/material.js')}}"></script>



<!--Extra Config Js Ends-->

<script>
    (function(document, window, $){
        'use strict';

        var Site = window.Site;
        $(document).ready(function(){
            Site.run();
        });
    })(document, window, jQuery);
</script>
</body>
</html>