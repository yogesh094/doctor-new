<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Confirm Password</title>
    <link rel="icon" href="{{ url('images/frontend-image/fav.png') }}" type="image/png" sizes="16x16"> 
    
    <link rel="stylesheet" href="{{ url('css/frontend-css/main.min.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ url('css/frontend-css/style.css') }}">
    <link rel="stylesheet" href="{{ url('css/frontend-css/color.css') }}">
    <link rel="stylesheet" href="{{ url('css/frontend-css/responsive.css') }}">
    <link rel="stylesheet" href="{{url('global/vendor/toastr/toastr.css')}}">
    <link rel="stylesheet" href="{{url('assets/examples/css/advanced/toastr.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend-css/custom.css')}}">

    @stack('externalCssLoad')
    @stack('internalCssLoad')

    <script type="text/javascript">
        var baseUrl = "{{ url('/') }}/";
        var csrf_token = "<?php echo csrf_token(); ?>";

        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div class="theme-layout">
        <div class="responsive-header">
            <div class="mh-head first Sticky">
                    <span class="mh-btns-left">
                        <a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
                    </span>
                    <span class="mh-text">
                        <a href="{{ url('/') }}" title=""><img src="{{ url('images/frontend-image/logo2.png')}}" alt=""></a>
                    </span>
            </div>
        </div><!-- responsive header -->

        <div class="topbar stick">
            <div class="logo" style="margin-top:10px;">
                <a title="" href="{{ url('/') }}"><img src="{{ url('images/frontend-image/logo.png')}}" alt=""></a>
            </div>
        </div><!-- topbar -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Confirm Password') }}</div>

                        <div class="card-body">
                            {{ __('Please confirm your password before continuing.') }}

                            <form method="POST" action="{{ route('password.confirm') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Confirm Password') }}
                                        </button>

                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script data-cfasync="false" src="{{url('js/frontend-js/email-decode.min.js')}}"></script>
    <script src="{{url('js/frontend-js/main.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ url('js/frontend-js/script.js') }}"></script>
    <script src="{{url('global/vendor/toastr/toastr.js')}}"></script>
    <script src="{{url('js/admin-js/jsconfig.js')}}"></script>
    <script>
        // (function(document, window, $){
        //     'use strict';

        //     var Site = window.Site;
        //     $(document).ready(function(){
        //         Site.run();
        //     });
        // })(document, window, jQuery);

        // For showing the toaster JS Session messages
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        @if($msg == 'success')
            toastr.success("<?php echo Session::get('alert-' . $msg);?>");
        @endif
        @if($msg == 'danger')
            toastr.error("<?php echo Session::get('alert-' . $msg);?>");
        @endif
        @if($msg == 'warning')
            toastr.warning("<?php echo Session::get('alert-' . $msg);?>");
        @endif
        @endif
        @endforeach

    </script>
    @stack('externalJsLoad')
    @stack('internalJsLoad')
</body>
</html>
