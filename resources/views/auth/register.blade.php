<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Title-->
    <title>{{ __('app.app_name') }} - Register</title>
    <!-- Favicon-->
    <link rel="icon" href="{{url('images/frontend-image/core-img/favicon.png')}}">
    <!-- Core Stylesheet-->
    <link rel="stylesheet" href="{{url('css/frontend-css/main.css')}}">
</head>
<body>
<!-- Preloader-->
<div class="preloader" id="preloader">
    <div class="spinner-grow text-light" role="status"><span class="sr-only">Loading...</span></div>
</div>
<!-- Header Area-->
<!-- Header Area-->
<header class="header-area header2">
    <div class="container">
        <div class="classy-nav-container breakpoint-off">
            <nav class="classy-navbar navbar2 justify-content-between" id="saasboxNav">
                <a class="nav-brand mr-5" href="http://talkndiscus.com"><img src="{{url('images/frontend-image/core-img/Asset 1@2x.png')}}" alt="" width="200px" style="padding-bottom:25px;"></a>

                <div class="classy-navbar-toggler"><span class="navbarToggler"><span></span><span></span><span></span><span></span></span></div>
                <!-- Menu-->
                <div class="classy-menu">
                    <!-- close btn-->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>
                    <!-- Nav Start-->
                </div>
            </nav>
        </div>
    </div>
</header>

<!-- Register Area-->
<div class="register-area section-padding-120-70">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <!-- Register Thumbnail-->
            <div class="col-12 col-lg-6">
                <div class="register-thumbnail mb-50"><img src="{{url('images/frontend-image/bg-img/hero-3.png')}}" alt=""></div>
            </div>
            <!-- Register Card-->
            <div class="col-12 col-lg-6">
                <div class="card register-card bg-gray p-1 p-sm-4 mb-50">
                    <div class="card-body">
                        <h4>Create your free account</h4>
                        <p>Already have an account?<a class="ml-2" href="{{ url('login') }}">Log In</a></p>

                        @if($errors->any())
                            <div class="alert alert-danger">
                                {{ $errors->first() }}
                            </div>
                        @endif
                        @if (Session::has('message'))
                            <div class="mt-4 alert alert-success">{{ Session::get('message') }}</div>
                        @endif
                        <!-- Register Form-->
                        <div class="register-form my-5">
                            <form action="{{ route('register') }}" method="post">
                                @csrf
                                <div class="form-group mb-3">
                                    <input class="form-control rounded-0 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" type="text" placeholder="Full Name" required autocomplete="off">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <input class="form-control rounded-0 @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" type="text" placeholder="UserName" required autocomplete="off">
                                    @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <input class="form-control rounded-0 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" type="email" placeholder="Email Address" required autocomplete="off">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    {{--<label class="label-psswd" for="registerPassword"><span class="hide">HIDE</span><span class="show">SHOW</span></label>--}}
                                    <input class="input-psswd form-control rounded-0 @error('password') is-invalid @enderror" name="password" id="password" type="password" placeholder="Password" psswd-shown="false" required autocomplete="off">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    {{--<label class="label-psswd" for="registerPassword"><span class="hide">HIDE</span><span class="show">SHOW</span></label>--}}
                                    <input class="input-psswd form-control rounded-0" name="password_confirmation" id="password-confirm" type="password" placeholder="Confirm Password" psswd-shown="false" required autocomplete="off">
                                </div>
                                <div class="form-group mb-3">
                                    <select name="profile_type" class="form-control rounded-0" id="profile_type" required>
                                        <option value="">Select Profile</option>
                                        <option value="0">Doctor / PG Student</option>
                                        <option value="1">UG Medical Student</option>
                                        <option value="2">HealthCare Institute</option>
                                    </select>

                                    @error('profile_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <button class="btn saasbox-btn white-btn w-100" type="submit">Register Now</button>
                            </form>
                        </div>
                        <!-- Sign in via others-->
                        <div class="signin-via-others">
                            <p class="mb-0">Or Sign in with</p><a class="btn saasbox-btn btn-sm mt-3 mr-3" href="#"><i class="fa fa-facebook mr-2"></i>Facebook</a><a class="btn saasbox-btn btn-sm mt-3 mr-3" href="#"><i class="fa fa-twitter mr-2"></i>Twitter</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="border-top"></div>
</div>
<!-- Footer Area-->
<footer class="footer-area footer2 section-padding-20">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-5">
                <div class="footer--content-text">
                    <p class="mb-0">All rights reserved by <a href="https://nirumawebx.com/" target="_blank">Nirumawebx</a></p>
                </div>
            </div>
            <div class="col-12 col-lg-2">
                <!-- Default dropup button-->
                <div class="language-dropdown text-center text-lg-right mt-4 mt-lg-0">
                    <div class="footer-social-icon d-flex align-items-center"><a href="#" data-toggle="tooltip" data-placement="top" title="Facbook"><i class="fa fa-facebook"></i></a><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a><a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram"></i></a><a href="#" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a><a href="#" data-toggle="tooltip" data-placement="top" title="Youtube"><i class="fa fa-youtube"></i></a></div>
                </div>
            </div>
            <!--  </div>-->
        </div>
    </div>
</footer>
<!-- All JavaScript Files-->
<script src="{{url('js/frontend-js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('js/frontend-js/jquery.min.js')}}"></script>
<script src="{{url('js/frontend-js/default/classy-nav.min.js')}}"></script>
<script src="{{url('js/frontend-js/waypoints.min.js')}}"></script>
<script src="{{url('js/frontend-js/jquery.easing.min.js')}}"></script>
<script src="{{url('js/frontend-js/default/jquery.scrollup.min.js')}}"></script>
<script src="{{url('js/frontend-js/owl.carousel.min.js')}}"></script>
<script src="{{url('js/frontend-js/wow.min.js')}}"></script>
<script src="{{url('js/frontend-js/default/jquery.passwordstrength.js')}}"></script>
<script src="{{url('js/frontend-js/default/active.js')}}"></script>
<script type="text/javascript">
    var baseUrl = "{{ url('/') }}/";
    var csrf_token = "<?php echo csrf_token(); ?>";

    window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
    ]); ?>
</script>
</body>
</html>