@if (count($errors) > 0)
    <div class="card card-inverse bg-danger">
        <div class="card-block">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><p class="card-text">{{ $error }}</p></li>
                @endforeach
            </ul>
        </div>
    </div>
@endif