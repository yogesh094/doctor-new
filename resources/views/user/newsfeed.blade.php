@extends('user.layouts.app')
@section('pageTitle')
    {{ __('app.app_name') }} | {{ __("News Feed") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
	<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row merged20" id="page-contents">
							{!! $left_section_html !!}
							<div class="col-lg-6">
								{!! isset($postadd_html)?$postadd_html:'' !!}
								<div id="new-post">
                                @if(!empty($post_html))
                                    @foreach($post_html as $key => $value)
                                        {!! isset($value)?$value:'' !!}
                                    @endforeach
									@else
									<div class="central-meta">
                                    	<div class="editing-info">
											<div class="onoff-options">
			                                    <div class="setting-row">
			                                      <h6>No records to display</h6>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
									@endif

                            </div>
								@if(isset($currentPage) && isset($lastPage) && $currentPage < $lastPage)
								<input type="hidden" value="{{ isset($currentPage)?$currentPage + 1:0 }} " id="next-page-post" />
								<button id="load-more-post" class="btn-view btn-load-more load-more"></button>
								@endif
							</div><!-- centerl meta -->
							@include('user.layouts.rightsection')
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@push('externalJsLoad')
    <script src="{{url('js/frontend-js/newsfeed.js')}}"></script>
@endpush
@push('internalJsLoad')
<script type="text/javascript">
</script>
@endpush
