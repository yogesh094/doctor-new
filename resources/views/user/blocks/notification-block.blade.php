 <li>
    @if($value->type == 0)
        <a href="{{ url('profile/'.$value->user_name.'/'.$value->id) }}" title="">
    @else
        <a href="{{ url('post/'.$value->post_id.'/'.$value->id)}}" title="">
    @endif
         @if(!empty($value->profile_image) && $value->profile_image != '')
             <img src="{{ config('services.upload_url') }}/image/user/{{ $value->profile_image }}" alt="">
         @else
             <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
         @endif
         <div class="mesg-meta">
             <h6>{{ $value->username }}</h6>
             <span>{{ $description }}</span>
             <i>{{ time_ago($value->updated_at) }}</i>
         </div>
     </a>
     @if($value->is_read == 0)
        <span class="tag blue">Unseen</span>
     @endif
 </li>