<aside class="sidebar static">
	<div class="widget">
		<h4 class="widget-title">Profile intro</h4>
		<ul class="short-profile">
			<li>
				<span>About</span>
				@if(isset($userData->id) && $userData->id != Auth::user()->id)
					<p>{{ isset($userData->description)?$userData->description:'-' }} </p>
				@else
					<p>{{ Auth::user()->description }}</p>
				@endif
			</li>
			<li>
				<span>Email</span>
				<p>{{ isset($userData->email)?$userData->email:Auth::user()->email }}</p>
			</li>
		</ul>
	</div><!-- profile intro widget -->									
</aside>