<li id="comment-block-{{ $comment_v->id }}">
    <div class="comet-avatar">
        @if(!empty($comment_v->profile_image) && $comment_v->profile_image != '')
            <img src="{{ config('services.upload_url') }}/image/user/{{ $comment_v->profile_image }}" alt="">
        @else
            <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
        @endif
    </div>
    <div class="we-comment">
        <div class="coment-head">
            <h5><a href="{{ url('profile/'.$comment_v->user_name) }}" title="">{{ $comment_v->username }}</a></h5>
            <span>{{ time_ago($comment_v->created_at) }}</span>
            <a class="we-repl" title="Reply" ><i data-commentId ="{{ $comment_v->id }}" class="fa fa-reply reply-box-added"></i></a>
            @if($login_user_id == $comment_v->user_id)
                <a class="we-repl" title="delete" ><i data-commentId ="{{ $comment_v->id }}" data-postId ="{{ $comment_v->post_id }}" class="fa fa-trash delete-comment"></i></a>
            @endif
            
        </div>
        <p>{!! $comment_v->description !!}</p>
    </div>
    @if(isset($reply_html) && $reply_html != '' )
        <ul id="append-reply_{{  $comment_v->id }}">
            {!! isset($reply_html)?$reply_html:'' !!}
            <li>
                @if($post_data['comment_data']['reply']['currentPage'] < $post_data['comment_data']['reply']['lastPage'])
                <input type="hidden" value="{{ $post_data['comment_data']['reply']['currentPage']+1 }} " id="next-page-{{ $comment_v->id }}" />
                <button data-postId = "{{ $comment_v->post_id }}" data-commentId = "{{ $comment_v->id }}" title="" id="load-more-reply-{{ $comment_v->id }}" class="btn-view btn-load-more load-more"></button>
                @endif
                
            </li>
            <li>
            <div class="row" id="reply-box_{{ $comment_v->id }}" style="display: none;">
                <div class="input-group comment_txtbox">
                    <input type="hidden" name="comment_id" value="{{ $comment_v->id }}" id="comment_id" />
                    <input type="text" class="form-control comment_box" id="reply_text_{{ $comment_v->id }}" placeholder="Leave a Reply here...">
                    <a class="input-group-addon btn_post btn btn-primary" data-postId = "{{ $comment_v->post_id }}" id="new_reply_{{ $comment_v->id }}" href="#">Send</a>
                </div>
            </div>
            </li>
        </ul>
        @else
        <ul style="display: none;" id="append-reply_{{  $comment_v->id }}">
            <li>
            <div style="display: none;" class="row" id="reply-box_{{ $comment_v->id }}">
                <div class="input-group comment_txtbox">
                    <input type="hidden" name="comment_id" value="{{ $comment_v->id }}" id="comment_id" />
                    <input type="text" class="form-control comment_box" id="reply_text_{{ $comment_v->id }}" placeholder="Leave a Reply here...">
                    <a class="input-group-addon btn_post btn btn-primary" data-postId = "{{ $comment_v->post_id }}" id="new_reply_{{ $comment_v->id }}" href="#">Send</a>
                </div>
            </div>
            </li>
        </ul>
        @endif
</li>


