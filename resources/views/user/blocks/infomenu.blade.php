<aside class="sidebar static">
    <div class="widget stick-widget">
        <h4 class="widget-title">Edit info</h4>
        <ul class="naves">
            <li class="{{ (Request::segment(1) == 'profile')?'active':''}}">
                <i class="ti-info-alt"></i>
                <a href="{{ url('/profile') }}" title="">Basic info</a>
            </li>
            <li class="{{ (Request::segment(1) == 'education')?'active':''}}">
                <i class="fa fa-graduation-cap"></i>
                <a href="{{ url('/education') }}" title="">Education & Experience</a>
            </li>
            <li class="{{ (Request::segment(1) == 'interest')?'active':''}}">
                <i class="fa fa-book"></i>
                <a href="{{ url('/interest') }}" title="">My interests/Skills</a>
            </li>
            <!-- <li class="{{ (Request::segment(1) == 'profile')?'active':''}}">
                <i class="ti-settings"></i>
                <a href="edit-account-setting.html" title="">account setting</a>
            </li> -->
            <li class="{{ (Request::segment(1) == 'changepassword')?'active':''}}">
                <i class="ti-lock"></i>
                <a href="{{ url('/changepassword') }}" title="">Change password</a>
            </li>
        </ul>
    </div><!-- settings widget -->
</aside>
