<div class="central-meta new-pst" >
    <div class="new-postbox">
        <figure>
            @if(!empty(Auth::user()->profile_image) && Auth::user()->profile_image != '')
                <img src="{{ config('services.upload_url') }}/image/user/{{ Auth::user()->profile_image }}" alt="">
            @else
                <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
            @endif
        </figure>
        <div class="newpst-input">
            <form class="postForm" method="post" action="{{ url('/add/post') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="status" value="1">
                <textarea class="post-desc" rows="2" placeholder="write something" name="description"></textarea>
                <img id="post_upload_img" src="#" style="display:none;"/>
                <video id="post_upload_video" width="auto" height="240" src="#" controls style="display:none;">
                  <!-- <source  src="#"> -->
                </video>
                <div class="attachments">
                    <ul>
                        <li>
                            <i class="fa fa-image"></i>
                            <label class="fileContainer">
                                <input type="file" id="postImgUpload" name="media">
                            </label>
                        </li>
                        <li>
                            <button type="submit">Post</button>
                        </li>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</div>
