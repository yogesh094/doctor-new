 <li id="reply-block-{{ $value->id }}">
    <div class="comet-avatar">
        @if(!empty($value->profile_image) && $value->profile_image != '')
            <img src="{{ config('services.upload_url') }}/image/user/{{ $value->profile_image }}" alt="">
        @else
            <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
        @endif
    </div>
    <div class="we-comment">
        <div class="coment-head">
            <h5><a href="{{ url('profile/'.$value->user_name) }}" title="">{{ $value->username }}</a></h5>
            <span>{{ time_ago($value->created_at) }}</span>
            <a class="we-reply" title="Reply"><i data-commentId ="{{ $value->post_comment_id }}" class="fa fa-reply reply-box-added"></i></a>
            
              @if($login_user_id == $value->user_id)
                <a class="we-repl" title="delete" ><i data-replyId ="{{ $value->id }}" data-postId ="{{ $value->post_id }}" class="fa fa-trash delete-reply"></i></a>
            @endif
        </div>
        <p>{!! $value->description !!}</p>
    </div>
</li>