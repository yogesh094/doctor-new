<div class="central-meta item" style="display: inline-block;">
	<div class="user-post">
		<div class="friend-info">
			<figure>
				@if(!empty($post_v->profile_image) && $post_v->profile_image != '')
                    <img src="{{ config('services.upload_url') }}/image/user/{{ $post_v->profile_image }}" alt="">
                @else
                    <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
                @endif
			</figure>
			@if($login_user_id == $post_v->user_id)
			<ul class="toolbox">
			  <li class="dropdown">
				<a href="#" class="dropdown-toggle dropdown-ctm" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
				<ul class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -74px, 0px); top: 0px; left: 0px; will-change: transform;">
				  <li class="post-edit"><a href="#" data-id="{{ $post_v->id }}">Edit</a>
				  </li>
				  <li class="post-delete"><a href="#" data-id="{{ $post_v->id }}">Delete</a>
				  </li>
				</ul>
			  </li>
			</ul>
			@endif
			<div class="friend-name">
				<ins><a href="{{ url('profile/'.$post_v->user_name) }}" title="">{{ $post_v->username }}</a></ins>
                <span>{{ time_ago($post_v->created_at) }}</span>
			</div>
			<div class="post-meta">
				<div class="description">
					<p>
						{!! $post_v->description !!}
					</p>
				</div>
				@if($post_v->media_type == 1)
					<img src="{{ config('services.upload_url') }}/image/user/{{ $post_v->user_id }}/{{ $post_v->media }}" alt="">
				@endif
				@if($post_v->media_type == 2)
					<video width="320" height="240" controls>
					  <source src="{{ config('services.upload_url') }}/image/user/{{ $post_v->user_id }}/{{ $post_v->media }}" type="video/mp4">
					</video>
				@endif
				<div class="we-video-info">
					<ul>
						<span style="display: none;" id= islike_{{ $post_v->id }}>{{isset($is_like)?$is_like:0}}</span>
						<li>
							<span class="like" id="postlike_{{ $post_v->id }}"   data-toggle="tooltip" title="" data-original-title="like">
								@if(isset($is_like) && $is_like == 1)
									<i id="likeclass_{{ $post_v->id }}" class="fa fa-thumbs-up"></i>
								@else
									<i id="likeclass_{{ $post_v->id }}" class="fa fa-thumbs-o-up"></i>
								@endif
								<ins id="likecount_{{ $post_v->id }}">{{ isset($like_count)?$like_count:0 }}</ins>
							</span>
						</li>
						<li>
							<span class="dislike" id="postdislike_{{ $post_v->id }}" data-islike="{{ isset($is_like)?$is_like:0 }}" data-toggle="tooltip" title="" data-original-title="dislike">
								@if(isset($is_like) && $is_like == 2)
									<i id="dislikeclass_{{ $post_v->id }}" class="fa fa-thumbs-down"></i>
								@else
									<i id="dislikeclass_{{ $post_v->id }}" class=" fa fa-thumbs-o-down"></i>
								@endif
								<ins id="dislikecount_{{ $post_v->id }}">{{ isset($dislike_count)?$dislike_count:0 }}</ins>
							</span>
						</li>
						<li>
							<span class="comment" data-toggle="tooltip" title="" data-original-title="Comments">
								<i class="fa fa-comments-o"></i>
								<ins id="commentcount_{{ $post_v->id }}">{{ isset($comment_count)?$comment_count:0 }}</ins>
							</span>
						</li>
					</ul>
				</div>

			</div>
		</div>
		<div class="coment-area">
            <ul class="we-comet" id="append-comment_<?= $post_id ?>">
					{!! isset($comment_html)?$comment_html:'' !!}
				@if(isset($comment_html) && $comment_html != '')
				<li>
					 @if($post_data['comment_data']['currentPage'] < $post_data['comment_data']['lastPage'])
						<input type="hidden" value="{{ isset($post_data['comment_data']['currentPage'])?$post_data['comment_data']['currentPage']+1:1 }} " id="next-page-{{ $post_id }}" />
						<button data-postId = {{ $post_id }} title="" id="load-more-comment-{{ $post_id }}" class="btn-view btn-load-more load-more"></button>
						@endif
				</li>
					@else
					<li style="display: none;">
						<input type="hidden" value="{{ isset($post_data['comment_data']['currentPage'])?$post_data['comment_data']['currentPage']+1:1 }} " id="next-page-{{ $post_id }}" />
						<button data-postId = {{ $post_id }} title="" id="load-more-comment-{{ $post_id }}" class="btn-view btn-load-more load-more"></button>
					</li>

				@endif
                <li class="post-comment">
                    <div class="comet-avatar">
                        @if(!empty(Auth::user()->profile_image) && Auth::user()->profile_image != '')
		                    <img src="{{ config('services.upload_url') }}/image/user/{{ Auth::user()->profile_image }}" alt="">
		                @else
		                    <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
		                @endif
                    </div>
                    <div class="post-comt-box">
                        <div class="row">
                            <div class="input-group comment_txtbox">
                                <input type="hidden" name="campaignId" value="{{ $post_v->id }}" id="postId" />
                                <input type="text" class="form-control comment_box" id="comment_{{ $post_v->id }}" placeholder="Leave a comment here...">
                                <input type="hidden" name="commentcount" id="comment_count_hidden_121" value="17">
                                <a class="input-group-addon btn_post btn btn-primary" id="new_comment_{{ $post_v->id }}" href="#">Send</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
		</div>
	</div>
</div>
