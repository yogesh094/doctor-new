<div class="postoverlay"></div>
@if(!Auth::user()->email_verified_at)
    <div class="responsive-header">
        <div class="mh-head first Sticky">
            <span class="mh-btns-left">
                <a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
            </span>
            <span class="mh-text">
                <a href="{{ url('/') }}" title=""><img src="{{ url('images/frontend-image/logo2.png')}}" alt=""></a>
            </span>
        </div>
    </div><!-- responsive header -->

    <div class="topbar stick">
        <div class="logo">
            <a title="" href="{{ url('/') }}"><img src="{{ url('images/frontend-image/logo.png')}}" alt=""></a>
        </div>
        <div class="top-area">
            <div class="user-img">
                <img src="{{ url('images/frontend-image/resources/admin.jpg')}}" alt="">
                <span class="status f-online"></span>
                <div class="user-setting">
                    <a href="#" title=""><span class="status f-online"></span>online</a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" title=""><i class="ti-power-off"></i>log out</a>
                </div>
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
            <!-- <span class="ti-menu main-menu" data-ripple=""></span> -->
        </div>
    </div><!-- topbar -->
@else
    <div class="responsive-header">
        <div class="mh-head first Sticky">
            <span class="mh-btns-left">
                <a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
            </span>
            <span class="mh-text">
                <a href="{{ url('/news-feed') }}" title=""><img src="{{ url('images/frontend-image/logo2.png')}}" alt=""></a>
            </span>
            <span class="mh-btns-right">
                <a class="fa fa-sliders" href="#shoppingbag"></a>
            </span>
        </div>
        <div class="mh-head second">
            <form class="mh-form">
                <input placeholder="search" />
                <a href="#/" class="fa fa-search"></a>
            </form>
        </div>
        <!-- <nav id="menu" class="res-menu">
            <ul>
                <li><a href="{{ url('/dashboard') }}" title="">Home</a></li>
                <li><a href="about.html" title="">about</a></li>
                <li><a href="about-company.html" title="">About Us2</a></li>
                <li><a href="contact.html" title="">contact</a></li>
                <li><a href="contact-branches.html" title="">Contact Us2</a></li>
                <li><a href="widgets.html" title="">Widgts</a></li>
            </ul>
        </nav> -->
    </div><!-- responsive header -->

    <div class="topbar stick">
        <div class="logo">
            <a title="" href="{{ url('/news-feed') }}"><img src="{{ url('images/frontend-image/logo.png')}}" alt=""></a>
        </div>

        <div class="top-area">
            <!-- <ul class="main-menu">
                <li>
                    <a href="{{ url('/dashboard') }}" title="">Home</a>
                </li>
            </ul> -->
            <ul class="setting-area">
                <li>
                    <a href="#" title="Home" data-ripple=""><i class="ti-search"></i></a>
                    <div class="searched">
                        <form method="post" class="form-search">
                            <input type="text" id="search-friends" placeholder="Search Friend">
                            <button data-ripple><i class="ti-search"></i></button>
                        </form>
                    </div>
                </li>
                 <li>
                    <a href="#" title="Notification" data-ripple="">
                        <i class="ti-bell"></i><span id="noti_count">0</span>
                    </a>
                   <div class="dropdowns">
                        <ul class="drops-menu" id="top-notification">
                        </ul>
                        <a href="{{ url('/notification') }}" title="" class="more-mesg">view more</a>
                    </div>
                </li>
                {{--<li>--}}
                    {{--<a href="#" title="Messages" data-ripple=""><i class="ti-comment"></i><span>12</span></a>--}}
                    {{--<div class="dropdowns">--}}
                        {{--<span>5 New Messages</span>--}}
                        {{--<ul class="drops-menu">--}}
                            {{--<li>--}}
                                {{--<a href="notifications.html" title="">--}}
                                    {{--<img src="{{ url('images/frontend-image/resources/thumb-1.jpg')}}" alt="">--}}
                                    {{--<div class="mesg-meta">--}}
                                        {{--<h6>sarah Loren</h6>--}}
                                        {{--<span>Hi, how r u dear ...?</span>--}}
                                        {{--<i>2 min ago</i>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                                {{--<span class="tag green">New</span>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="notifications.html" title="">--}}
                                    {{--<img src="{{ url('images/frontend-image/resources/thumb-2.jpg')}}" alt="">--}}
                                    {{--<div class="mesg-meta">--}}
                                        {{--<h6>Jhon doe</h6>--}}
                                        {{--<span>Hi, how r u dear ...?</span>--}}
                                        {{--<i>2 min ago</i>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                                {{--<span class="tag red">Reply</span>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="notifications.html" title="">--}}
                                    {{--<img src="{{ url('images/frontend-image/resources/thumb-3.jpg')}}" alt="">--}}
                                    {{--<div class="mesg-meta">--}}
                                        {{--<h6>Andrew</h6>--}}
                                        {{--<span>Hi, how r u dear ...?</span>--}}
                                        {{--<i>2 min ago</i>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                                {{--<span class="tag blue">Unseen</span>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="notifications.html" title="">--}}
                                    {{--<img src="{{ url('images/frontend-image/resources/thumb-4.jpg')}}" alt="">--}}
                                    {{--<div class="mesg-meta">--}}
                                        {{--<h6>Tom cruse</h6>--}}
                                        {{--<span>Hi, how r u dear ...?</span>--}}
                                        {{--<i>2 min ago</i>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                                {{--<span class="tag">New</span>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="notifications.html" title="">--}}
                                    {{--<img src="{{ url('images/frontend-image/resources/thumb-5.jpg')}}" alt="">--}}
                                    {{--<div class="mesg-meta">--}}
                                        {{--<h6>Amy</h6>--}}
                                        {{--<span>Hi, how r u dear ...?</span>--}}
                                        {{--<i>2 min ago</i>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                                {{--<span class="tag">New</span>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<a href="messages.html" title="" class="more-mesg">view more</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>
            <div class="user-img">
                @if(!empty(Auth::user()->profile_image) && Auth::user()->profile_image != '')
                    <img width="45px" height="45px"  src="{{ config('services.upload_url') }}/image/user/{{ Auth::user()->profile_image }}" alt="">
                @else
                    <img src="{{ url('images/frontend-image/resources/admin.jpg')}}" alt="">
                @endif
                <span class="status f-online"></span>
                <div class="user-setting">
                    <a href="#" title=""><span class="status f-online"></span>online</a>
                    <a href="{{ url('profile') }}" title=""><i class="ti-user"></i> view profile</a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" title=""><i class="ti-power-off"></i>log out</a>
                </div>
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
            <!-- <span class="ti-menu main-menu" data-ripple=""></span> -->
        </div>
    </div><!-- topbar -->
@endif    
@push('internalJsLoad')
<script>
  $( function() {

    var data = '<?php echo searchFriend(); ?>';
    
    $( "#search-friends" ).autocomplete({
      minLength: 0,
      source: JSON.parse(data),
      
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
            .append( "<div><a href='"+baseUrl+"profile/"+item.value+"'>" + item.label + "</a></div>" )
            .appendTo( ul );
        };
      });
</script>
@endpush