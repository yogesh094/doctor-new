<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle')</title>
    <link rel="icon" href="{{ url('images/frontend-image/fav.png') }}" type="image/png" sizes="16x16"> 
    
    <link rel="stylesheet" href="{{ url('css/frontend-css/main.min.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ url('css/frontend-css/style.css') }}">
    <link rel="stylesheet" href="{{ url('css/frontend-css/color.css') }}">
    <link rel="stylesheet" href="{{ url('css/frontend-css/responsive.css') }}">
    <link rel="stylesheet" href="{{url('global/vendor/toastr/toastr.css')}}">
    <link rel="stylesheet" href="{{url('assets/examples/css/advanced/toastr.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('css/frontend-css/custom.css')}}">

    @stack('externalCssLoad')
    @stack('internalCssLoad')

    <script type="text/javascript">
        var baseUrl = "{{ url('/') }}/";
        var storageUrl = "{{ config('services.upload_url') }}/";
        var csrf_token = "<?php echo csrf_token(); ?>";

        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div class="theme-layout">
        <div id="popupModal" class="modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-default" onClick="window.location.reload();">OK</button>
                        <!-- <a href="#" >
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        </a> -->
                    </div>
                </div>

            </div>
        </div>
        <div id="messageModal" class="modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                </div>

            </div>
        </div>
        @include('user.layouts.header')
        @yield('content')
    </div>
    <script data-cfasync="false" src="{{url('js/frontend-js/email-decode.min.js')}}"></script>
    <script src="{{url('js/frontend-js/main.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ url('js/frontend-js/script.js') }}"></script>
    <script src="{{url('global/vendor/toastr/toastr.js')}}"></script>
    <script src="{{url('js/admin-js/jsconfig.js')}}"></script>
    <script src="{{url('js/common.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.6.0/src/loadingoverlay.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.6.0/extras/loadingoverlay_progress/loadingoverlay_progress.min.js" ></script>
    <script>
        // (function(document, window, $){
        //     'use strict';

        //     var Site = window.Site;
        //     $(document).ready(function(){
        //         Site.run();
        //     });
        // })(document, window, jQuery);

        // For showing the toaster JS Session messages
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        @if($msg == 'success')
            toastr.success("<?php echo Session::get('alert-' . $msg);?>");
        @endif
        @if($msg == 'danger')
            toastr.error("<?php echo Session::get('alert-' . $msg);?>");
        @endif
        @if($msg == 'warning')
            toastr.warning("<?php echo Session::get('alert-' . $msg);?>");
        @endif
        @endif
        @endforeach

    </script>
    @stack('externalJsLoad')
    @stack('internalJsLoad')
</body>
</html>
