<div class="col-lg-3">
	<aside class="sidebar static">
		<div class="widget widget-profile">
			@if(!empty(Auth::user()->profile_image) && Auth::user()->profile_image != '')
				<img src="{{ config('services.upload_url') }}/image/user/{{ Auth::user()->profile_image }}" alt="">
			@else
				<img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
			@endif			
			<div class="your-page">
				<div class="page-meta">
					<a href="#" title="" class="underline">{{ Auth::user()->name }}</a>
					<span class="mb-15">{{ getProfileTypeName(Auth::user()->profile_type) }}</span>
				</div>
				<!-- <p class="mt-15 mb-0">Who viewed your profile <em>9</em></p> -->
				<p class="mt-15 mb-0">Total Connection <em>{{ getConnectionCount() }}</em></p>
				<p>Total Posts <em>{{ getPostCount() }}</em></p>
			</div>
		</div><!-- page like widget -->
		@if(isset($recentComment) && (count($recentComment) > 0))
		<div class="widget">
			<h4 class="widget-title">Recent Activity</h4>
			<ul class="activitiez">
				@foreach($recentComment as $v)
					<li>
						<div class="activity-meta">
							<i>{{ time_ago($v->created_at) }}</i>
							<span><a href="#" title="">You commented "{{ \Illuminate\Support\Str::limit($v->description ?? '',5,' ...') }}" on the post" </a></span>
							<!-- <h6>by <a href="time-line.html">black demon.</a></h6> -->
						</div>
					</li>
				@endforeach
			</ul>
		</div>
		<!-- recent activites -->
		@endif
		@if(isset($section) && (count($section) > 0))
		<div class="widget stick-widget">
			<h4 class="widget-title">Recommended Friends</h4>
			<ul class="followers">
				@foreach($section as $k => $value)
					<li>
						<figure>
							@if(!empty($value->profile_image) && $value->profile_image != '' && does_url_exists($value->profile_image) !== false)
                                <img src="{{ config('services.upload_url') }}/image/user/{{ $value->profile_image }}" alt="">
                            @else
                                <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
                            @endif
						</figure>
						<div class="friend-meta">
							<h4><a href="{{ url('profile/'.$value->username) }}" title="{{ $value->name }}">{{ $value->name }}</a></h4>
							<a href="#" data-to="{{ $value->id }}" class="underline addFriend">Add Friend</a>
						</div>
					</li>
				@endforeach
			</ul>
		</div><!-- who's following -->
		@endif
	</aside>
</div><!-- sidebar -->