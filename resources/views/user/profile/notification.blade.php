@extends('user.layouts.app')
@section('pageTitle')
    {{ __('app.app_name') }} | {{ __("Notification") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    @include('user.profile.topsection')
    <section>
        <div class="gap gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="col-lg-3">
                                @include('user.blocks.profileintro')
                            </div><!-- sidebar -->
                            <div class="col-lg-9">
                                <div class="central-meta">
                                    <div class="editing-interest">
                                        <h5 class="f-title"><i class="ti-bell"></i>All Notifications </h5>
                                        <div class="notification-box">
                                            <ul id="new-noti">
                                                @if(isset($details) && count($details) > 0)
                                                    @foreach($details as $key => $value)
                                                    <li>
                                                        <figure>
                                                            @if(!empty($value->profile_image) && $value->profile_image != '')
                                                                <img src="{{ config('services.upload_url') }}/image/user/{{ $value->profile_image }}" alt="">
                                                            @else
                                                                <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
                                                            @endif
                                                        </figure>
                                                        <div class="notifi-meta">
                                                            <p>{{ $value->description }}</p>
                                                            <span>{{ time_ago($value->created_at) }}</span>
                                                        </div>
                                                        <!-- <i class="del fa fa-close"></i> -->
                                                    </li>
                                                    @endforeach
                                                @else
                                                <li>
                                                    <div class="nearly-pepls">
                                                        <div class="pepl-info">
                                                            <h6>No Notifications</h6>
                                                        </div>
                                                    </div>
                                                </li>
                                                @endif
                                            </ul>
                                            <div class="lodmore">
                                                @if(isset($noti_currentPage) && isset($noti_lastPage) && $noti_currentPage < $noti_lastPage)
                                                    <button id="load-more-noti" class="btn-view btn-load-more"></button>
                                                    <input type="hidden" value="{{ isset($noti_currentPage)?$noti_currentPage + 1:0 }} " id="next-page-noti" />
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- centerl meta -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('externalJsLoad')
@endpush
@push('internalJsLoad')
<script type="text/javascript">
$('#load-more-noti').click(function (e) {
        e.preventDefault();
        var nextPage = $('#next-page-noti').val();
        $.LoadingOverlay("show");
        //$('#loader-'+postId).show();
        $.ajax({
            url: baseUrl + "more-noti/list"+ '?page=' + nextPage,
            type: 'POST',
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                $.LoadingOverlay("hide");
                if (data.status == 200) {
                    $('#new-noti').append(data.post_html);
                    $('#next-page-noti').val(data.next_page);
                    // $('#loader-'+postId).hide();
                    if (data.next_page > data.last_page) {
                        $('#load-more-noti').hide();
                    }

                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
    });
</script>
@endpush
