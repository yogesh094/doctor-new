@extends('user.layouts.app')
@section('pageTitle')
    {{ __('app.app_name') }} | {{ __("Interest") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    @include('user.profile.topsection')
    <section>
        <div class="gap gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="col-lg-3">
                                @include('user.blocks.infomenu')
                            </div><!-- sidebar -->
                            <div class="col-lg-9">
                                <div class="central-meta">
                                    <div class="editing-interest">
                                        <h5 class="f-title"><i class="fa fa-book"></i>My interests/Skills</h5>
                                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</p>
                                        <form method="post" id="interestForm">
                                            <label>Add interests: </label>
                                            <input type="text" id="addInterestInputId" placeholder="Add interest - Special carachters are not allowed"
                                                onenter="createAddedInterestLIElement(event)"
                                                onkeypress="validateAddInterestFieldValue(event)">
                                            <button onclick="createAddedInterestLIElement(event)">Add</button>
                                            <ol class="interest-added" id="interestListOlId">

                                                @if(isset($interestData))
                                                    @foreach($interestData as $key => $value)
                                                        <li><a href="javascript:void(0);" title="Interest">{{ $value }}</a><span class="remove" title="remove" onclick="removeInterest(event);"><i class="fa fa-close"></i></span></li>
                                                    @endforeach
                                                @endif
                                                <!--<li><a href="#" title="">bycicling</a><span class="remove" title="remove"><i class="fa fa-close"></i></span></li>
                                                <li><a href="#" title="">traveling</a><span class="remove" title="remove"><i class="fa fa-close"></i></span></li>
                                                <li><a href="#" title="">photography</a><span class="remove" title="remove"><i class="fa fa-close"></i></span></li>
                                                <li><a href="#" title="">shopping</a><span class="remove" title="remove"><i class="fa fa-close"></i></span></li>
                                                <li><a href="#" title="">eating</a><span class="remove" title="remove"><i class="fa fa-close"></i></span></li>
                                                <li><a href="#" title="">hoteling</a><span class="remove" title="remove"><i class="fa fa-close"></i></span></li>-->
                                            </ol>
                                            <input type="hidden" name="title" id="allInterestsInputId">
                                            <div class="submit-btns">
                                                <button type="button" class="mtr-btn" onclick="updateInterestListToDatabase(event)">
                                                    <span>Save</span>
                                                </button>
                                            </div>
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div><!-- centerl meta -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('externalJsLoad')
@endpush
@push('internalJsLoad')
<script type="text/javascript">
    //Check for valid interest input value - Special characters are not allowed
    function validateAddInterestFieldValue(event) {
        var specials = /[*|\":<>[\]{}`\\()';@&$~!#%^_\-+=,.?\/]/;
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if(specials.test(key)) {
            event.preventDefault();
            return false;
        } 
    }

    /**Prevent form from submission when enter key is pressed on input on 'Add' btn 
     * or 'Add' btn is clicked
     * Insted add the interest value to the interest list and create LI element
    */
    function createAddedInterestLIElement(event) {
        var interestValue = $('#addInterestInputId').val();

        //Check If string only contains whitespace (ie. spaces, tabs or line breaks)
        if(interestValue.replace(/\s/g, '').length) {
            $('#interestListOlId').append('<li>' +
                                            '<a href="javascript:void(0);" title="">'+ interestValue +'</a>' +
                                            '<span class="remove" title="remove" onclick="removeInterest(event);">' +
                                                '<i class="fa fa-close"></i>' +
                                            '</span>' + 
                                        '</li>');
        }

        $('#addInterestInputId').val('');
        event.preventDefault();
        return false;
    }

    /** Create semi colon separated intersts list value using li elements (all interests) 
     *  and set this to allInterestsInput field and then Update Interest List into the 
     *  database using that input value
     */
    function updateInterestListToDatabase(event) { 
        var allInterestsSeparatedBySemiColon = '';
        $("#interestListOlId li").each((id, liElem) => {
            allInterestsSeparatedBySemiColon += liElem.innerText;
            allInterestsSeparatedBySemiColon += ',';
        }); 
        
        //Remove last semicolon
        var allInterestsSeparatedBySemiColon = allInterestsSeparatedBySemiColon.substring(0, allInterestsSeparatedBySemiColon.length - 1);
        
        $('#allInterestsInputId').val(allInterestsSeparatedBySemiColon);

        if($('#allInterestsInputId').val().length) {
            //ajax call
            var formData = ($('#interestForm').serialize());
            var ajax_url = app.config.SITE_PATH + 'interest/store';
            $.ajax({
              url: ajax_url,
              type: 'POST',
              data: formData,
              cache: false,
              timeout: 20000,
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              success: function (data) {
                if(data.status == 200){
                  window.location.reload();
                }else{
                    $('#popupModal .modal-body p').text(data.msg);
                    $('#popupModal').modal("show");
                    return false;
                }
              }
            });
        } else {
            $('#popupModal .modal-body p').text('Nothing is there to update');
            $('#popupModal').modal("show");
            return false;
        }
    }

    // Remove interest from displayed list on UI
    function removeInterest(event) {
        if(event && event.currentTarget && event.currentTarget.parentNode 
            && event.currentTarget.parentNode.parentNode) {
            event.currentTarget.parentNode.parentNode.removeChild(event.currentTarget.parentNode);
        }
    }
</script>
@endpush
