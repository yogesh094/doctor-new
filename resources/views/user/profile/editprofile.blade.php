@extends('user.layouts.app')
@section('pageTitle')
    {{ __('app.app_name') }} | {{ __("Profile") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    @include('user.profile.topsection')
    <section>
        <div class="gap gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="col-lg-3">
                                @include('user.blocks.infomenu')
                            </div><!-- sidebar -->
                            <div class="col-lg-9">
                                <div class="central-meta">
                                    <div class="editing-info">
                                        <h5 class="f-title"><i class="ti-info-alt"></i> Edit Basic Information</h5>
                                        <form method="post" action="{{ url('update') }}">
                                            @csrf
                                            <input type="hidden" value="{{ Auth::user()->id }}" name="id">
                                            <div class="form-group">
                                                <input type="text" id="name" name="name" required="required" value="{{ isset($details->name)?$details->name:'' }}"/>
                                                <label class="control-label" for="input">Full Name</label><i class="mtrl-select"></i>
                                            </div>
                                            <div class="form-group half">
                                                <input style="color:#2a2a2a;" type="email" name="email" required="required" readonly value="{{ isset($details->email)?$details->email:'' }}"/>
                                                <label class="control-label" for="input"></label><i class="mtrl-select"></i>
                                            </div>
                                            <div class="form-group half">
                                                <input type="number" name="mobile_no" required="required" value="{{ isset($details->mobile_no)?$details->mobile_no:'' }}"/>
                                                <label class="control-label" for="input">Mobile No.</label><i class="mtrl-select"></i>
                                            </div>
                                            <div class="form-group">
                                                <select name="profile_type" required>
                                                    <option value="">Select Profile Type</option>
                                                    <option value="0" @if(0 == $details->profile_type){{"selected"}}@endif>Doctor / PG Student</option>
                                                    <option value="1" @if(1 == $details->profile_type){{"selected"}}@endif>UG Medical Student</option>
                                                    <option value="2" @if(2 == $details->profile_type){{"selected"}}@endif>HealthCare Institute</option>
                                                </select>
                                            </div>
                                            <div class="dob">
                                                <div class="form-group">
                                                    <select name="day" required>
                                                        <?php
                                                            if(isset($details->dob)){
                                                                $day = date('d',strtotime($details->dob));
                                                            }
                                                        ?>
                                                        <option value="">Birth Day</option>
                                                        @for ($x = 01; $x <= 31; $x++)
                                                          <option value="{{ $x }}" @if(isset($day) && $day == $x){{"selected"}}@endif>{{ $x }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select name="month" required>
                                                        <?php
                                                            if(isset($details->dob)){
                                                                $month = date('m',strtotime($details->dob));
                                                            }
                                                        ?>
                                                        <option value="">Birth Month</option>
                                                        @for ( $i = 1; $i <= 12; $i ++ )
                                                            <option value="{{ $i }}" @if(isset($month) && $month == $i){{"selected"}}@endif >{{ date( 'F', strtotime( "$i/12/10" ) ) }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select name="year" required>
                                                        <?php
                                                            if(isset($details->dob)){
                                                                $year = date('Y',strtotime($details->dob));
                                                            }
                                                        ?>
                                                        <option value="">Birth Year</option>
                                                        <?php
                                                            $earliest_year = 1950;
                                                            $latest_year = date('Y');
                                                            foreach ( range( $latest_year, $earliest_year ) as $i ) {
                                                            ?>
                                                                <option value="{{ $i }}" @if(isset($year) && $year == $i){{"selected"}}@endif>{{ $i }}</option>
                                                            <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-radio">
                                                @php $gender = isset($details->gender)?$details->gender:1; @endphp
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="gender" value="1" @if(1 == $gender){{"checked"}}@endif ><i class="check-box"></i>Male
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="gender" value="2" @if(2 == $gender){{"checked"}}@endif><i class="check-box"></i>Female
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <select name="country" class="country" id="country_id" required>
                                                    <option value="">Select Country</option>
                                                    @if(isset($country) && count($country) > 0)
                                                        @foreach($country as $row)
                                                            <option value="{{$row->id}}" @if(isset($details->country) && $details->country == $row->id){{"selected"}}@endif>{{$row->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group" id="state">
                                                <select name="state" id="state_id">
                                                    <option value="">Select State</option>
                                                </select>
                                                <input type="hidden" id="selected_state" name="selected_state" value="{{ $details->state }}" >
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="city" value="{{ isset($details->city)?$details->city:''}}" required="required"/>
                                                <label class="control-label" for="input">City</label><i class="mtrl-select"></i>
                                            </div>
                                            <div class="form-group">
                                                <textarea name="description" rows="4" id="textarea">{{ isset($details->description)?$details->description:''}}</textarea>
                                                <label class="control-label" for="textarea">About Yourself</label><i class="mtrl-select"></i>
                                            </div>
                                            
                                            <div class="submit-btns">
                                                <button type="submit" class="mtr-btn"><span>Save</span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div><!-- centerl meta -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('externalJsLoad')
@endpush
@push('internalJsLoad')
<script type="text/javascript">
        
</script>
@endpush
