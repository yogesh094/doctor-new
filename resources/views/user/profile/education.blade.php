@extends('user.layouts.app')
@section('pageTitle')
    {{ __('app.app_name') }} | {{ __("Education") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    @include('user.profile.topsection')
    <section>
        <div class="gap gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="col-lg-3">
                                @include('user.blocks.infomenu')
                            </div><!-- sidebar -->
                            <div class="col-lg-9">
                                <div class="central-meta">
                                    <div class="editing-info">
                                        <h5 class="f-title pb-5"><i class="fa fa-graduation-cap"></i>Education
                                          <span class="more-options">
                                            <button type="button" onClick="resetEducationModalData()" class="btn btn-primary" data-toggle="modal" data-target="#educationModal">
                                              ADD
                                            </button>
                                          </span>
                                        </h5>
                                        @if(!empty($educationData) && count($educationData) > 0)
                                            @foreach($educationData as $key => $value)
                                              <div class="onoff-options">
                                                <div class="setting-row pt-30">
                                                  <span>{{ $value->title }} <d>({{ $value->start_year }} - {{ $value->end_year }})</d></span>
                                                  <p>{{ $value->degree }}, {{ $value->field_of_study }}, {{ $value->grade }}</p>
                                                  <label><a href="javascript:void();" class="deleteEducation" data-id="{{ $value->id }}"><i class="fa fa-trash"></i></a></label>
                                                  <label class="mr-10"><a href="javascript:void();" class="editEducation" data-id="{{ $value->id }}"><i class="fa fa-edit"></i></a></label>
                                                </div>
                                              </div>
                                            @endforeach
                                        @else
                                          <div class="onoff-options">
                                            <div class="setting-row">
                                              <h6>No records to display</h6>
                                            </div>
                                          </div>
                                        @endif
                                    </div>
                                    <br>
                                    <div class="editing-info">
                                        <h5 class="f-title pb-5"><i class="fa fa-briefcase"></i>Experience
                                          <span class="more-options">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" 
                                              onclick="resetExpModalData()" data-target="#experienceModal">
                                              ADD
                                            </button>
                                          </span>
                                        </h5>

                                        @if(isset($experienceData) && count($experienceData) > 0)
                                            @foreach($experienceData as $exp)
                                              <?php
                                                if($exp->employment_type == 1){
                                                    $employment_type = 'Full Time';
                                                }else{
                                                    $employment_type = 'Part Time';
                                                }
                                              ?>
                                              <div class="onoff-options">
                                                <div class="setting-row pt-30">
                                                  <span>{{ $exp->title }} ({{ date('M',mktime(0, 0, 0, $exp->start_month, 10)) }} {{ $exp->start_year }} - {{ date('M',mktime(0, 0, 0, $exp->end_month, 10)) }} {{ $exp->end_year }}) | {{ $employment_type }}</span>
                                                  <p>{{ $exp->company }}</p>
                                                  <label><a href="javascript:void();" class="deleteExperience" data-id="{{ $exp->id }}"><i class="fa fa-trash"></i></a></label>
                                                  <label class="mr-10"><a href="javascript:void();" class="editExperience" data-id="{{ $exp->id }}"><i class="fa fa-edit"></i></a></label>
                                                </div>
                                              </div>
                                            @endforeach
                                        @else
                                          <div class="onoff-options">
                                            <div class="setting-row">
                                              <h6>No records to display</h6>
                                            </div>
                                          </div>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- centerl meta -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Experience Modal -->
    <div class="modal fade bd-example-modal-lg" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="educationModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="educationModalLongTitle">Add Education</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" id="educationForm">
              <div class="form-group">    
                <input type="text" id="title" name="title" required="required" />
                <label class="control-label" for="input">School/College Name</label><i class="mtrl-select"></i>
                <span class="invalid-feedback" id="title-err"  role="alert"></span>
              </div>
              <div class="form-group half">   
                <input type="text" id="degree" name="degree" required="required"/>
                <label class="control-label" for="input">Class/Degree</label><i class="mtrl-select"></i>
                <span class="invalid-feedback" id="degree-err"  role="alert"></span>
              </div>
              <div class="form-group half">   
                <input type="text" name="field_of_study" id="field_of_study" required="required"/>
                <label class="control-label" for="input">Field of Study</label><i class="mtrl-select"></i>
                <span class="invalid-feedback" id="field_of_study-err"  role="alert"></span>
              </div>                                              
              <div class="form-group half">    
                <select class="form-control" id="start_year" name="start_year">
                    <?php
                        if(isset($details->dob)){
                            $year = date('Y',strtotime($details->dob));
                        }
                    ?>
                    <option value="">Start Year</option>
                    <?php
                        $earliest_year = 1950;
                        $latest_year = date('Y');
                        foreach ( range( $latest_year, $earliest_year ) as $i ) {
                        ?>
                            <option value="{{ $i }}" @if(isset($year) && $year == $i){{"selected"}}@endif>{{ $i }}</option>
                        <?php
                        }
                    ?>
                </select>
                <span class="invalid-feedback" id="start_year-err"  role="alert"></span>
                <!-- <label class="control-label" for="input">Start Year</label><i class="mtrl-select"></i> -->
              </div>
              <div class="form-group half">    
                <select class="form-control" id="end_year" name="end_year">
                    <?php
                        if(isset($details->dob)){
                            $year = date('Y',strtotime($details->dob));
                        }
                    ?>
                    <option value="">End Year</option>
                    <?php
                        $earliest_year = 1950;
                        $latest_year = date('Y'); 
                        foreach ( range( $latest_year, $earliest_year ) as $i ) {
                        ?>
                            <option value="{{ $i }}" @if(isset($year) && $year == $i){{"selected"}}@endif>{{ $i }}</option>
                        <?php
                        }
                    ?>
                </select>
                <span class="invalid-feedback" id="end_year-err"  role="alert"></span>
                <!-- <label class="control-label" for="input">End Year</label><i class="mtrl-select"></i> -->
              </div>
              <div class="form-group">    
                <input type="text" id="grade" name="grade" required="required" />
                <label class="control-label" for="grade">Grade</label><i class="mtrl-select"></i>
                <span class="invalid-feedback" id="grade-err"  role="alert"></span>
              </div>
              <div class="form-group">    
                <textarea rows="4" id="extra_activities" name="extra_activities" required="required"></textarea>
                <label class="control-label" for="extra_activities">Extra Actvities</label><i class="mtrl-select"></i>
                <span class="invalid-feedback" id="extra_activities-err"  role="alert"></span>
              </div>
              <input type="hidden" name="id" class="education_id" id="education_id" > 
              <div class="submit-btns">
                  <button type="button" class="mtr-btn" id="addEducation"><span>Save</span></button>
                  <button type="button" class="mtr-btn" data-dismiss="modal"><span>Cancel</span></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="experienceModal" tabindex="-1" role="dialog" aria-labelledby="educationModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="expModalLongTitle">Add Experience</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" id="experienceForm">
              <div class="form-group">    
                <input type="text" id="exp_title" name="title" required="required" />
                <label class="control-label" for="input">Title</label><i class="mtrl-select"></i>
                <span class="invalid-feedback" id="exp_title-err"  role="alert"></span>
              </div>
              <div class="form-group">    
                <select class="form-control" name="employment_type" id="employment_type">
                    <option value="">Select Employment Type</option>
                    <option value="1">Full Time</option>
                    <option value="2">Part Time</option>
                </select>
              </div>
              <div class="form-group half">   
                <input type="text" name="company" id="company" required="required"/>
                <label class="control-label" for="input">Company</label><i class="mtrl-select"></i>
                <span class="invalid-feedback" id="company-err"  role="alert"></span>
              </div>
              <div class="form-group half">   
                <input type="text" name="location" id="location" required="required"/>
                <label class="control-label" for="input">Location</label><i class="mtrl-select"></i>
                <span class="invalid-feedback" id="location-err"  role="alert"></span>
              </div>
              <div class="form-group">   
                <div class="checkbox">
                  <label>
                  <input type="checkbox" name="is_currently_working" id="is_currently_working"
                    onChange="toggleEndDateFieldsSectionInDOM()">
                      <i class="check-box"></i>I am currently working in this role
                  </label>
                </div>
              </div>                                     
              <div class="form-group half">    
                <select class="form-control" name="start_month" id="exp_start_month">
                    <?php
                        if(isset($details->dob)){
                            $month = date('m',strtotime($details->dob));
                        }
                    ?>
                    <option value="">Start Month</option>
                    @for ( $i = 1; $i <= 12; $i ++ )
                        <option value="{{ $i }}" @if(isset($month) && $month == $i){{"selected"}}@endif >{{ date( 'F', strtotime( "$i/12/10" ) ) }}</option>
                    @endfor
                </select>
                <span class="invalid-feedback" id="exp_start_month-err"  role="alert"></span>
              </div>
              <div class="form-group half">    
                <select class="form-control" name="start_year" id="exp_start_year">
                    <?php
                        if(isset($details->dob)){
                            $year = date('Y',strtotime($details->dob));
                        }
                    ?>
                    <option value="">Start Year</option>
                    <?php
                        $earliest_year = 1950;
                        $latest_year = date('Y');
                        foreach ( range( $latest_year, $earliest_year ) as $i ) {
                        ?>
                            <option value="{{ $i }}" @if(isset($year) && $year == $i){{"selected"}}@endif>{{ $i }}</option>
                        <?php
                        }
                    ?>
                </select>
                <span class="invalid-feedback" id="exp_start_year-err"  role="alert"></span>
              </div>

              <div id="end_date_fields">
                <div class="form-group half">    
                  <select class="form-control" name="end_month" id="exp_end_month">
                      <?php
                          if(isset($details->dob)){
                              $month = date('m',strtotime($details->dob));
                          }
                      ?>
                      <option value="">End Month</option>
                      @for ( $i = 1; $i <= 12; $i ++ )
                          <option value="{{ $i }}" @if(isset($month) && $month == $i){{"selected"}}@endif >{{ date( 'F', strtotime( "$i/12/10" ) ) }}</option>
                      @endfor
                  </select>
                  <span class="invalid-feedback" id="exp_end_month-err"  role="alert"></span>
                </div>              
                <div class="form-group half">    
                  <select class="form-control" name="end_year" id="exp_end_year">
                      <?php
                          if(isset($details->dob)){
                              $year = date('Y',strtotime($details->dob));
                          }
                      ?>
                      <option value="">End Year</option>
                      <?php
                          $earliest_year = 1950;
                          $latest_year = date('Y');
                          foreach ( range( $latest_year, $earliest_year ) as $i ) {
                          ?>
                              <option value="{{ $i }}" @if(isset($year) && $year == $i){{"selected"}}@endif>{{ $i }}</option>
                          <?php
                          }
                      ?>
                  </select>
                  <span class="invalid-feedback" id="exp_end_year-err"  role="alert"></span>
                </div>
              </div>
              
              <div class="form-group">    
                <textarea rows="4" id="description" name="description" required="required"></textarea>
                <label class="control-label" for="textarea">Description</label><i class="mtrl-select"></i>
              </div>
              <input type="hidden" name="id" class="experience_id" id="experience_id">
              <div class="submit-btns">
                  <button type="button" class="mtr-btn" id="addExperience"><span>Add</span></button>
                  <button type="button" class="mtr-btn" data-dismiss="modal"><span>Cancel</span></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('externalJsLoad')
@endpush
@push('internalJsLoad')
<script type="text/javascript">

  $('#addEducation').click(function(){

      var error_cnt = 0;
      var email_error = 0;

      $('.invalid-feedback').html('');
    
      if ($('#title').val() == '') {
          var msg = "The title field is required.";
          $('#title-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }
      if ($('#degree').val() == '') {
          var msg = "The degree field is required.";
          $('#degree-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }

      if ($('#field_of_study').val() == '') {
          var msg = "The field of study field is required.";
          $('#field_of_study-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }
      if ($('#grade').val() == '') {
          var msg = "The grade is required.";
          $('#grade-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }

      if ($('select[name=start_year]').val() == ''){
          var msg = "The start year field is required.";
          $('#start_year-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }

      if ($('select[name=end_year]').val() == ''){
          var msg = "The end year field is required.";
          $('#end_year-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      } else if (parseInt($('select[name=end_year]').val()) < parseInt($('select[name=start_year]').val())) {
          var msg = "The end year should not be less than start year.";
          $('#end_year-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }

      if(error_cnt > 0){
          $('.invalid-feedback').css('display','block');
          var error_cnt = 0;
          return false;
      }else{
          var formData = ($('#educationForm').serialize());
          var ajax_url = app.config.SITE_PATH + 'education/store';
          $.ajax({
              url: ajax_url,
              type: 'POST',
              data: formData,
              cache: false,
              timeout: 20000,
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              success: function (data) {
                if(data.status == 200){
                  window.location.reload();
                }else{
                  $('#educationModal').modal("hide");
                  $('#popupModal .modal-body p').text(data.msg);
                  $('#popupModal').modal("show");
                  return false;
                }
              }
          });
      }
  });
  
  $('#addExperience').click(function(){

      var error_cnt = 0;
      var email_error = 0;

      $('.invalid-feedback').html('');
    
      if ($('#exp_title').val() == '') {
          var msg = "The title field is required.";
          $('#exp_title-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }
      if ($('#employment_type').val() == '') {
          var msg = "The employment type field is required.";
          $('#employment_type-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }

      if ($('#company').val() == '') {
          var msg = "The company field is required.";
          $('#company-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }
      if ($('#location').val() == '') {
          var msg = "The location is required.";
          $('#location-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }

      if ($('#exp_start_month').val() == ''){
          var msg = "The start month field is required.";
          $('#exp_start_month-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }

      if ($('#exp_start_year').val() == ''){
          var msg = "The start year field is required.";
          $('#exp_start_year-err').html('<strong>'+msg+'</strong>');
          error_cnt = error_cnt + 1;
      }

      if(!$('#is_currently_working').is(":checked")) {
          $('#is_currently_working').val(0);

          if ($('#exp_end_month').val() == ''){
              var msg = "The end month field is required.";
              $('#exp_end_month-err').html('<strong>'+msg+'</strong>');
              error_cnt = error_cnt + 1;

          } else if ((parseInt($('#exp_end_year').val()) == parseInt($('#exp_start_year').val()))
              && (parseInt($('#exp_end_month').val()) < parseInt($('#exp_start_month').val()))){
              var msg = "The end month should not be less than start month.";
              $('#exp_end_month-err').html('<strong>'+msg+'</strong>');
              error_cnt = error_cnt + 1;
          }

          if ($('#exp_end_year').val() == ''){
              var msg = "The end year field is required.";
              $('#exp_end_year-err').html('<strong>'+msg+'</strong>');
              error_cnt = error_cnt + 1;

          } else if (parseInt($('#exp_end_year').val()) < parseInt($('#exp_start_year').val())){
              var msg = "The end year should not be less than start year.";
              $('#exp_end_year-err').html('<strong>'+msg+'</strong>');
              error_cnt = error_cnt + 1;
          }
      } else {
          $('#is_currently_working').val(1);
          $('#exp_end_month').val($('#exp_start_month').val());
          $('#exp_end_year').val($('#exp_start_year').val());
      }      

      if(error_cnt > 0){
          $('.invalid-feedback').css('display','block');
          var error_cnt = 0;
          return false;
      }else{
          var formData = ($('#experienceForm').serialize());
          var ajax_url = app.config.SITE_PATH + 'experience/store';
          $.ajax({
              url: ajax_url,
              type: 'POST',
              data: formData,
              cache: false,
              timeout: 20000,
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              success: function (data) {
                if(data.status == 200){
                  window.location.reload();
                }else{
                  $('#experienceModal').modal("hide");
                  $('#popupModal .modal-body p').text(data.msg);
                  $('#popupModal').modal("show");
                  return false;
                }
              }
          });
      }
  });

  $('.deleteEducation').click(function(){
      var delConfirmationResult = confirm("Are you sure you want to delete this item?");
      if (!delConfirmationResult) {
          return;
      }
      
      var id = $(this).attr("data-id");
      var ajax_url = app.config.SITE_PATH + 'education/delete';
      $.ajax({
          url: ajax_url,
          type: 'POST',
          data: {"id":id},
          cache: false,
          timeout: 20000,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function (data) {
            if(data.status == 200){
              window.location.reload();
            }else{
              $('#popupModal .modal-body p').text(data.msg);
              $('#popupModal').modal("show");
              return false;
            }
          }
      });
  });

  $('.deleteExperience').click(function(){
      var delConfirmationResult = confirm("Are you sure you want to delete this item?");
      if (!delConfirmationResult) {
          return;
      }

      var id = $(this).attr("data-id");
      var ajax_url = app.config.SITE_PATH + 'experience/delete';
      $.ajax({
          url: ajax_url,
          type: 'POST',
          data: {"id":id},
          cache: false,
          timeout: 20000,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function (data) {
            if(data.status == 200){
              window.location.reload();
            }else{
              $('#popupModal .modal-body p').text(data.msg);
              $('#popupModal').modal("show");
              return false;
            }
          }
      });
  });

  $('.editEducation').click(function(){
      var id = $(this).attr("data-id");
      var ajax_url = app.config.SITE_PATH + 'education/edit';
      $.ajax({
          url: ajax_url,
          type: 'POST',
          data: {"id":id},
          cache: false,
          timeout: 20000,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function (data) {
            setEditEducationModalData(data);
            $('#educationModal').modal('show')
            console.log(data);
            return false;
            
          }
      });
  });

  function setEditEducationModalData(educationJSON) {
      $('#education_id').val(educationJSON.id); 
      $('#title').val(educationJSON.title); 
      $('#degree').val(educationJSON.degree); 
      $('#field_of_study').val(educationJSON.field_of_study); 
      $('#start_year').val(educationJSON.start_year); 
      $('#end_year').val(educationJSON.end_year);  
      $('#grade').val(educationJSON.grade); 
      $('#extra_activities').val(educationJSON.extra_activities); 
      $('#educationModalLongTitle').text('Edit Education');
      $('#addEducation span').text('Update');
      $(".invalid-feedback").text("");
  }

  function resetEducationModalData() {
      $("#educationForm")[0].reset();
      $('#education_id').val(""); 
      $('#educationModalLongTitle').text('Add Education');
      $('#addEducation span').text('Save');
      $(".invalid-feedback").text("");
  }

  $('.editExperience').click(function(){
      var id = $(this).attr("data-id");
      var ajax_url = app.config.SITE_PATH + 'experience/edit';
      $.ajax({
          url: ajax_url,
          type: 'POST',
          data: {"id":id},
          cache: false,
          timeout: 20000,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success: function (data) {
            setEditExpModalData(data);
            $('#experienceModal').modal('show')
            console.log(data);
            return false;
          }
      });
  });

  function setEditExpModalData(expJSON) {
    $('#experience_id').val(expJSON.id);
    $('#exp_title').val(expJSON.title);
    $('#employment_type').val(expJSON.employment_type);
    $('#employment_type').next('div#employment_type_chosen').children('a.chosen-single').children('span').text(expJSON.employment_type);
    $('#company').val(expJSON.company);
    $('#location').val(expJSON.location);         
    $('#exp_start_month').val(expJSON.start_month);      
    $('#exp_start_year').val(expJSON.start_year);      
    $('#exp_end_month').val(expJSON.end_month);     
    $('#exp_end_year').val(expJSON.end_year);      
    $('#description').val(expJSON.description);
    $('#expModalLongTitle').text('Edit Experience');
    $('#addExperience span').text('Update');
    $(".invalid-feedback").text("");
    $('#is_currently_working').val(expJSON.is_currently_working); 

    if(expJSON.is_currently_working == 1) {
      $('#is_currently_working').attr('checked', true);
    } else {
      $('#is_currently_working').attr('checked', false);
    }

    if($('#is_currently_working').is(":checked")) {
      $("#end_date_fields").hide(); 
      $('#exp_end_month').val("");
      $('#exp_end_year').val("");
    }
  }

  function resetExpModalData() {
    $("#experienceForm")[0].reset();    
    $('#experience_id').val("");
    $('#is_currently_working').attr('checked', false);
    $('#is_currently_working').val(0);
    $("#end_date_fields").show(); 
    $('#expModalLongTitle').text('Add Experience');
    $('#addExperience span').text('Save');
    $(".invalid-feedback").text("");
  }
  
  function toggleEndDateFieldsSectionInDOM() {
    if($('#is_currently_working').is(":checked")) {
        $('#is_currently_working').val("1");
        $("#end_date_fields").hide(); 
        $('#exp_end_month').val("");
        $('#exp_end_year').val("");
    } else {
        $('#is_currently_working').val("0");
        $("#end_date_fields").show();
    }
  }
</script>
@endpush
