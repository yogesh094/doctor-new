@extends('user.layouts.app')
@section('pageTitle')
    {{ __('app.app_name') }} | {{ __("Change Password") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    @include('user.profile.topsection')
    <section>
        <div class="gap gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="col-lg-3">
                                @include('user.blocks.infomenu')
                            </div><!-- sidebar -->
                            <div class="col-lg-9">
                                <div class="central-meta">
                                    <div class="editing-info">
                                        <h5 class="f-title"><i class="ti-lock"></i>Change Password</h5>
                                        <form method="POST" action="{{ url('/changepassword/update') }}">
                                            <div class="form-group">    
                                              <input type="password" id="current-password" name="current-password" required="required"/>
                                              <label class="control-label" for="input">Current password</label><i class="mtrl-select"></i>
                                            </div>
                                            <div class="form-group">    
                                              <input type="password" id="password" name="password" required="required"/>
                                              <label class="control-label" for="input">New password</label><i class="mtrl-select"></i>
                                            </div>
                                            <div class="form-group">    
                                              <input type="password" id="password-confirm" name="password_confirmation" required="required"/>
                                              <label class="control-label" for="input">Confirm password</label><i class="mtrl-select"></i>
                                              <span class="" id="error" role="alert" >
                                                </span>
                                            </div>
                                            <!-- <a class="forgot-pwd underline" title="" href="#">Forgot Password?</a> -->
                                            <div class="submit-btns">
                                                <button type="submit" class="mtr-btn"><span>Save</span></button>
                                            </div>
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div><!-- centerl meta -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('externalJsLoad')
@endpush
@push('internalJsLoad')
<script type="text/javascript">
</script>
@endpush
