<section>
	<div class="feature-photo">
		<figure>
		@if(!empty(Auth::user()->cover_image) && Auth::user()->cover_image != '')
			<img id="cover_image" src="{{ config('services.upload_url') }}/image/user/{{ Auth::user()->cover_image }}" alt="">
		@else
			<img src="{{ url('images/frontend-image/resources/timeline-1.jpg')}}" alt="">
		@endif
		
		</figure>
		<!-- <div class="add-btn">
			<span>1205 followers</span>
			<a href="#" title="" data-ripple="">Add Friend</a>
		</div> -->
		<form class="edit-phto" action="{{url('/save-cover-image')}}" id="upload-cover" method="post" enctype="multipart/form-data">
			<i class="fa fa-camera-retro"></i>
			<label class="fileContainer">
				Edit Cover Photo
			<input type="file" name="cover_image" id="cover_avatar" onchange="form.submit()"//>
			</label>
			<input type="hidden" name="id" id="id" value="{{ Auth::user()->id }}" />
			@csrf
		</form>
		<div class="container-fluid">
			<div class="row merged">
				<div class="col-lg-2 col-sm-3">
					<div class="user-avatar">
						<figure>
							@if(!empty(Auth::user()->profile_image) && Auth::user()->profile_image != '')
								<img id="profile_image" src="{{ config('services.upload_url') }}/image/user/{{ Auth::user()->profile_image }}" alt="">
							@else
								<img id="profile_image" src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
							@endif
							<form class="edit-phto" action="{{url('/save-profile-image')}}" id="upload-profile" method="post" enctype="multipart/form-data">
								<i class="fa fa-camera-retro"></i>
								<label class="fileContainer">
									Edit Display Photo
									<input type="file" name="profile_image" id="profile_avatar" onchange="form.submit()"/>
								</label>
								<input type="hidden" name="id" id="id" value="{{ Auth::user()->id }}" />
								@csrf
							</form>
						</figure>
					</div>
				</div>
				<div class="col-lg-10 col-sm-9">
					<div class="timeline-info">
						<ul>
							<li class="admin-name">
							  <h5>{{ Auth::user()->name }}</h5>
							</li>
							<li>
								<a class="{{ (Request::segment(1) == 'timeline')?'active':''}}" href="{{ url('timeline/'.Auth::user()->username) }}" title="" data-ripple="">time line</a>
								<a class="{{ (in_array(Request::segment(1), ['profile','education','interest','changepassword']))?'active':''}}" href="{{ url('profile') }}" title="" data-ripple="">about</a>
								<a class="{{ (Request::segment(1) == 'friend-list')?'active':''}}" href="{{ url('friend-list') }}" title="" data-ripple="">Friends</a>
								<a class="{{ (Request::segment(1) == 'notification')?'active':''}}" href="{{ url('notification') }}" title="" data-ripple="">Notification</a>
								<!-- <a class="" href="timeline-photos.html" title="" data-ripple="">Photos</a>
								<a class="" href="timeline-videos.html" title="" data-ripple="">Videos</a>
								<a class="" href="groups.html" title="" data-ripple="">Groups</a> -->
								<!-- <a class="active" href="#" title="" data-ripple="">more</a> -->
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!-- top area -->
