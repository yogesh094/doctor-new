@extends('user.layouts.app')
@section('pageTitle')
	{{ __('app.app_name') }} | {{ __("Timeline") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
	<section>
		<div class="feature-photo">
			<figure>
				@if(!empty($details->cover_image) && $details->cover_image != '')
					<img id="cover_image" src="{{ config('services.upload_url') }}/image/user/{{ $details->cover_image }}" alt="">
				@else
					<img src="{{ url('images/frontend-image/resources/timeline-1.jpg')}}" alt="">
				@endif
			</figure>
			@if(isset($details->username) && Auth::user()->username != $details->username)
				<div class="add-btn">
					@if(isset($connectionrequest->type) && $connectionrequest->type == 0)
						<a href="javascript:void(0);" title="Cancel Request" id="cancelRequest" onclick="cancelRequest(this)" data-id="{{ isset($connectionrequest->id)?$connectionrequest->id:0 }}" data-to="{{ $details->id }}">Cancel Request</a>
						@if($details->id == $connectionrequest->request_from)
							<a href="javascript:void(0);" title="Confirm Request" id="confirmRequest" onclick="confirmRequest(this)" data-id="{{ isset($connectionrequest->id)?$connectionrequest->id:0 }}" data-to="{{ $details->id }}">Confirm</a>
						@endif
					@elseif(isset($connectionrequest->type) && $connectionrequest->type == 1)
						<a href="javascript:void(0);" title="Send Message" id="sendMessage" onclick="sendMessage(this)" data-to="{{ $details->id }}">Send Message</a>
					@else
						<a href="javascript:void(0);" title="Add Connection" id="sendRequest" onclick="sendRequest(this)" data-to="{{ $details->id }}">Add Connection</a>
					@endif
				</div>
			@endif
			<div class="container-fluid">
				<div class="row merged">
					<div class="col-lg-2 col-sm-3">
						<div class="user-avatar">
							<figure>
								@if(!empty($details->profile_image) && $details->profile_image != '')
									<img src="{{ config('services.upload_url') }}/image/user/{{ $details->profile_image }}" alt="">
								@else
									<img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
								@endif
							</figure>
						</div>
					</div>
					<div class="col-lg-10 col-sm-9">
						<div class="timeline-info">
							<ul>
								<li class="admin-name">
									<h5>{{ $details->name }}</h5>
									<!-- <span>{{ getProfileTypeName($details->profile_type) }}</span> -->
								</li>
								<li>
									<a class="{{ (Request::segment(1) == 'timeline')?'active':''}}" href="{{ url('timeline/'.$details->username) }}" title="" data-ripple="">time line</a>
								</li>
								<li>
									<a class="{{ (Request::segment(1) == 'profile')?'active':''}}" href="{{ url('profile/'.$details->username) }}" title="" data-ripple="">Profile</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- top area -->

	<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row merged20" id="page-contents">
							<div class="col-lg-3">
								{!! $profile_intro_html !!}
							</div><!-- sidebar -->
							<div class="col-lg-6">
								@if(isset($details->username) && Auth::user()->username == $details->username)
									{!! isset($postadd_html)?$postadd_html:'' !!}
								@endif
								<div id="new-post">
									@if(!empty($post_html))
										@foreach($post_html as $key => $value)
											{!! isset($value)?$value:'' !!}
										@endforeach
									@else
									<div class="central-meta">
                                    	<div class="editing-info">
											<div class="onoff-options">
			                                    <div class="setting-row">
			                                      <h6>No records to display</h6>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
									@endif
								</div>

								<div class="lodmore">
									@if(isset($currentPage) && isset($lastPage) && $currentPage < $lastPage)
										<input type="hidden" value="{{ isset($currentPage)?$currentPage + 1:0 }} " id="next-page-post" />
										<button class="btn-view btn-load-more" id="load-more-timeline"></button>
									@endif
                                </div>
								
							</div><!-- centerl meta -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@push('externalJsLoad')
	<script src="{{url('js/frontend-js/newsfeed.js')}}"></script>
@endpush
@push('internalJsLoad')
<script type="text/javascript">
</script>
@endpush