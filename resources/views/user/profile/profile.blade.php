@extends('user.layouts.app')
@section('pageTitle')
    {{ __('app.app_name') }} | {{ __("Profile") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <section>
        <div class="feature-photo">
            <figure>
            @if(!empty($details->cover_image) && $details->cover_image != '')
                <img id="cover_image" src="{{ config('services.upload_url') }}/image/user/{{ $details->cover_image }}" alt="">
            @else
                <img id="cover_image" src="{{ url('images/frontend-image/resources/timeline-1.jpg')}}" alt="">
            @endif
            </figure>
            @if(isset($details->username) && Auth::user()->username != $details->username)
            <div class="add-btn">
                @if(isset($connectionrequest->type) && $connectionrequest->type == 0)
                    <a href="javascript:void(0);" title="Cancel Request" id="cancelRequest" onclick="cancelRequest(this)" data-id="{{ isset($connectionrequest->id)?$connectionrequest->id:0 }}" data-to="{{ $details->id }}">Cancel Request</a>
                    @if($details->id == $connectionrequest->request_from)
                    <a href="javascript:void(0);" title="Confirm Request" id="confirmRequest" onclick="confirmRequest(this)" data-id="{{ isset($connectionrequest->id)?$connectionrequest->id:0 }}" data-to="{{ $details->id }}">Confirm</a>
                    @endif
                @elseif(isset($connectionrequest->type) && $connectionrequest->type == 1)
                    <a href="javascript:void(0);" title="Send Message" id="sendMessage" onclick="sendMessage(this)" data-to="{{ $details->id }}">Send Message</a>
                @else
                    <a href="javascript:void(0);" title="Add Connection" id="sendRequest" onclick="sendRequest(this)" data-to="{{ $details->id }}">Add Connection</a>
                @endif
            </div>
            @endif
            <div class="container-fluid">
                <div class="row merged">
                    <div class="col-lg-2 col-sm-3">
                        <div class="user-avatar">
                            <figure>
                                @if(!empty($details->profile_image) && $details->profile_image != '')
                                    <img src="{{ config('services.upload_url') }}/image/user/{{ $details->profile_image }}" alt="">
                                @else
                                    <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
                                @endif
                            </figure>
                        </div>
                    </div>
                    <div class="col-lg-10 col-sm-9">
                        <div class="timeline-info">
                            <ul>
                                <li class="admin-name">
                                  <h5>{{ $details->name }}</h5>
                                  <!-- <span>{{ getProfileTypeName($details->profile_type) }}</span> -->
                                </li>
                                <li>
                                    <a class="{{ (Request::segment(1) == 'timeline')?'active':''}}" href="{{ url('timeline/'.$details->username) }}" title="" data-ripple="">time line</a>
                                </li>
                                <li>
                                  <a class="{{ (Request::segment(1) == 'profile')?'active':''}}" href="{{ url('profile/'.$details->username) }}" title="" data-ripple="">Profile</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- top area -->

    <section>
        <div class="gap gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="col-lg-3">
                                {!! $profile_intro_html !!}
                            </div><!-- sidebar -->
                            <div class="col-lg-9">
                                <div class="central-meta">
                                    <div class="editing-info">
                                        <h5 class="f-title"><i class="fa fa-graduation-cap"></i> Education</h5>
                                        @if(!empty($educationData) && count($educationData) > 0)
                                            @foreach($educationData as $key => $value)
                                              <div class="onoff-options">
                                                <div class="setting-row">
                                                  <span>{{ $value->title }} <d>({{ $value->start_year }} - {{ $value->end_year }})</d></span>
                                                  <p>{{ $value->degree }}, {{ $value->field_of_study }}, {{ $value->grade }}</p>
                                                  <span class="extra-activities">{{ $value->extra_activities }}</span>
                                                </div>
                                              </div>
                                            @endforeach
                                        @else
                                          <div class="onoff-options">
                                            <div class="setting-row">
                                              <h6>No records to display</h6>
                                            </div>
                                          </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="central-meta">
                                    <div class="editing-info">
                                        <h5 class="f-title"><i class="fa fa-briefcase"></i> Experience</h5>
                                        @if(isset($experienceData) && count($experienceData) > 0)
                                            @foreach($experienceData as $exp)
                                              <?php
                                                if($exp->employment_type == 1){
                                                    $employment_type = 'Full Time';
                                                }else{
                                                    $employment_type = 'Part Time';
                                                }
                                              ?>
                                              <div class="onoff-options">
                                                <div class="setting-row">
                                                  <span>{{ $exp->title }} ({{ date('M',mktime(0, 0, 0, $exp->start_month, 10)) }} {{ $exp->start_year }} - {{ date('M',mktime(0, 0, 0, $exp->end_month, 10)) }} {{ $exp->end_year }}) | {{ $employment_type }}</span>
                                                  <p>{{ $exp->company }} | {{ $exp->location }}</p>
                                                  <span class="extra-activities">{{ $exp->description }}</span>
                                                </div>
                                              </div>
                                            @endforeach
                                        @else
                                          <div class="onoff-options">
                                            <div class="setting-row">
                                              <h6>No records to display</h6>
                                            </div>
                                          </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="central-meta">
                                    <div class="editing-interest">
                                        <h5 class="f-title"><i class="fa fa-book"></i>My interests / Skills</h5>
                                        @if(!empty($interestData))
                                            <form class="interest-form">
                                                <ol class="row interest-added interest-ol" id="interestListOlId">
                                                    @foreach($interestData as $key => $value)
                                                        <li class="interest-li"><a href="javascript:void(0);" title="Interest">{{ $value }}</a></li>
                                                    @endforeach
                                                </ol>
                                            </form>
                                        @else
                                          <div class="onoff-options">
                                            <div class="setting-row">
                                              <h6>No records to display</h6>
                                            </div>
                                          </div>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- centerl meta -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('externalJsLoad')
@endpush
@push('internalJsLoad')
<script type="text/javascript">
    function sendRequest(e){
        var request_to = $(e).attr("data-to");
        var ajax_url = app.config.SITE_PATH + 'send-request/'+request_to;
          $.ajax({
              url: ajax_url,
              type: 'POST',
              cache: false,
              timeout: 20000,
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              success: function (data) {
                if(data.status == 200){
                  $('#sendRequest').remove();
                  $('.add-btn').html("<a id='cancelRequest' onclick='cancelRequest(this)' href='javascript:void(0);' data-id='"+ data.request_id +"' data-to='"+ request_to +"'>Cancel Request</a>")
                }else{
                  $('#popupModal .modal-body p').text(data.msg);
                  $('#popupModal').modal("show");
                  return false;
                }
              }
          });
    }

    function cancelRequest(e){
        var request_id = $(e).attr("data-id");
        var request_to = $(e).attr("data-to");
        var ajax_url = app.config.SITE_PATH + 'cancel-request/'+request_id;
          $.ajax({
              url: ajax_url,
              type: 'POST',
              cache: false,
              timeout: 20000,
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              success: function (data) {
                if(data.status == 200){
                  $('#cancelRequest').remove();
                  $('.add-btn').html("<a id='sendRequest' onclick='sendRequest(this)' href='javascript:void(0);' data-to='"+ request_to +"' data-id='"+ request_id +"'>Add Connection</a>")
                }else{
                  $('#popupModal .modal-body p').text(data.msg);
                  $('#popupModal').modal("show");
                  return false;
                }
              }
          });
    }

    function confirmRequest(e){
      var request_id = $(e).attr("data-id");
      var request_to = $(e).attr("data-to");
      var ajax_url = app.config.SITE_PATH + 'confirm-request/'+request_id;
        $.ajax({
            url: ajax_url,
            type: 'POST',
            cache: false,
            timeout: 20000,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (data) {
              if(data.status == 200){
                  window.location.reload();
              }else{
                $('#popupModal .modal-body p').text(data.msg);
                $('#popupModal').modal("show");
                return false;
              }
            }
        });
    }

</script>
@endpush
