@extends('user.layouts.app')
@section('pageTitle')
    {{ __('app.app_name') }} | {{ __("Friend List") }}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    @include('user.profile.topsection')
    <section>
        <div class="gap gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="col-lg-3">
                                @include('user.blocks.profileintro')
                            </div><!-- sidebar -->
                            <div class="col-lg-9">
                                <div class="central-meta">
                                    <div class="frnds">
                                        <ul class="nav nav-tabs">
                                             <li class="nav-item"><a class="active" href="#frends" data-toggle="tab">My Friends</a> <span id="frends_id">{{ $my_friends_total }}</span></li>
                                             <li class="nav-item"><a class="" href="#frends-req" data-toggle="tab">Friend Requests</a><span id="frends_req_id">{{ $pending_request_total }}</span></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div class="tab-pane active fade show " id="frends" >
                                            <ul class="nearby-contct" id="new-friend">
                                            @if(isset($my_friends) && count($my_friends) > 0)
                                                @foreach($my_friends as $key => $value)
                                                    <?php 
                                                    if($value->request_to == Auth::user()->id){
                                                        $friend_id = $value->request_from;
                                                    }else{
                                                        $friend_id = $value->request_to;
                                                    }
                                                    $userData = getNameById($friend_id);
                                                    ?>
                                                    <li>
                                                        <div class="nearly-pepls">
                                                            <figure>
                                                                <a href="{{ url('profile/'.$userData->username) }}" title="">
                                                                    @if(!empty($userData->profile_image) && $userData->profile_image != '')
                                                                        <img src="{{ config('services.upload_url') }}/image/user/{{ $userData->profile_image }}" alt="">
                                                                    @else
                                                                        <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
                                                                    @endif
                                                                </a>
                                                            </figure>
                                                            <div class="pepl-info">
                                                                <h4><a href="{{ url('profile/'.$userData->username) }}" title="">{{ $userData->name }}</a></h4>
                                                                <span>{{ getProfileTypeName(Auth::user()->profile_type) }}</span>
                                                                <a href="#" title="Unfriend" class="add-butn" onclick="cancelRequest(this)" data-id="{{ $value->id }}" data-to="{{ $friend_id }}" data-type="1">Unfriend</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li>
                                                    <div class="nearly-pepls">
                                                        <div class="pepl-info">
                                                            <h6>No friends</h6>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endif
                                            
                                        </ul>
                                            <div class="lodmore">
                                                @if(isset($my_friends_currentPage) && isset($my_friends_lastPage) && $my_friends_currentPage < $my_friends_lastPage)
                                                <button class="btn-view btn-load-more" id="load-more-post"></button>
                                                    <input type="hidden" value="{{ isset($my_friends_currentPage)?$my_friends_currentPage + 1:0 }} " id="next-page-post" />
                                                @endif
                                            </div>
                                          </div>
                                          <div class="tab-pane fade" id="frends-req" >
                                            <ul class="nearby-contct" id="pending-request">
                                            @if(isset($pending_request) && count($pending_request) > 0)
                                                @foreach($pending_request as $k => $v)
                                                    <?php 
                                                    if($v->request_to == Auth::user()->id){
                                                        $friend_id = $v->request_from;
                                                    }else{
                                                        $friend_id = $v->request_to;
                                                    }
                                                    $userData = getNameById($friend_id);
                                                    ?>
                                                    <li>
                                                        <div class="nearly-pepls">
                                                            <figure>
                                                                <a href="{{ url('timeline/'.$userData->username) }}" title="">
                                                                    @if(!empty($userData->profile_image) && $userData->profile_image != '')
                                                                        <img src="{{ config('services.upload_url') }}/image/user/{{ $userData->profile_image }}" alt="">
                                                                    @else
                                                                        <img src="{{ url('images/frontend-image/resources/user-avatar.jpg')}}" alt="">
                                                                    @endif
                                                                </a>
                                                            </figure>
                                                            <div class="pepl-info">
                                                                <h4><a href="{{ url('timeline/'.$userData->username) }}" title="">{{ $userData->name }}</a></h4>
                                                                <span>{{ getProfileTypeName(Auth::user()->profile_type) }}</span>
                                                                <a href="#" title="Delete Request" class="add-butn more-action" onclick="cancelRequest(this)" data-id="{{ $v->id }}" data-to="{{ $friend_id }}" data-type="2">Delete Request</a>
                                                                <a href="#" title="Confirm Request" class="add-butn" onclick="confirmRequest(this)" data-id="{{ $v->id }}" data-to="{{ $friend_id }}" data-type="2">Confirm</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li>
                                                    <div class="nearly-pepls">
                                                        <div class="pepl-info">
                                                            <h6>No pending request</h6>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endif
                                        </ul>
                                            <div class="lodmore">
                                                @if(isset($pending_request_currentPage) && isset($pending_request_lastPage) && $pending_request_currentPage < $pending_request_lastPage)
                                                    <button id="load-more-pending-request" class="btn-view btn-load-more"></button>
                                                    <input type="hidden" value="{{ isset($pending_request_currentPage)?$pending_request_currentPage + 1:0 }} " id="next-page-pending-request" />
                                                @endif
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- centerl meta -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('externalJsLoad')
@endpush
@push('internalJsLoad')
<script type="text/javascript">

    $('#load-more-post').click(function (e) {
        e.preventDefault();
        var nextPage = $('#next-page-post').val();
        $.LoadingOverlay("show");
        //$('#loader-'+postId).show();
        $.ajax({
            url: baseUrl + "more-friend/list"+ '?page=' + nextPage,
            type: 'POST',
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                $.LoadingOverlay("hide");
                if (data.status == 200) {
                    $('#new-friend').append(data.post_html);
                    $('#next-page-post').val(data.next_page);
                    // $('#loader-'+postId).hide();
                    if (data.next_page > data.last_page) {
                        $('#load-more-post').hide();
                    }

                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
    });

    $('#load-more-pending-request').click(function (e) {
        e.preventDefault();
        var nextPage = $('#next-page-pending-request').val();
        $.LoadingOverlay("show");
        //$('#loader-'+postId).show();
        $.ajax({
            url: baseUrl + "pending-request/list"+ '?page=' + nextPage,
            type: 'POST',
            dataType: "json",
            timeout: 20000,
            headers: {'X-CSRF-TOKEN':csrf_token },
            success: function (data) {
                $.LoadingOverlay("hide");
                if (data.status == 200) {
                    $('#pending-request').append(data.post_html);
                    $('#next-page-pending-request').val(data.next_page);
                    // $('#loader-'+postId).hide();
                    if (data.next_page > data.last_page) {
                        $('#load-more-pending-request').hide();
                    }
                }else{
                    $('#popupModal .modal-body p').text('Something went wrong, please refresh page and try again');
                    $('#popupModal').modal("show");
                    return false;
                }
            },
        });
    });

    function cancelRequest(e){
        event.preventDefault();
        var request_id = $(e).attr("data-id");
        var request_to = $(e).attr("data-to");
        var data_type = $(e).attr("data-type");
        var ajax_url = app.config.SITE_PATH + 'cancel-request/'+request_id;
          $.ajax({
              url: ajax_url,
              type: 'POST',
              cache: false,
              timeout: 20000,
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              success: function (data) {
                if(data.status == 200){
                    if(data_type == 1){
                        var getfrnds_count = $('#frends_id').text();
                        $('#frends_id').text(getfrnds_count - 1);
                    }else{
                        var getfrnds_req_count = $('#frends_req_id').text();
                        $('#frends_req_id').text(getfrnds_req_count - 1);
                    }
                  $(e).parent().parent().parent().remove();
                }else{
                  $('#popupModal .modal-body p').text(data.msg);
                  $('#popupModal').modal("show");
                  return false;
                }
              }
          });
    }

    function confirmRequest(e){
        event.preventDefault();
        var request_id = $(e).attr("data-id");
        var request_to = $(e).attr("data-to");
        var data_type = $(e).attr("data-type");
        var ajax_url = app.config.SITE_PATH + 'confirm-request/'+request_id;
          $.ajax({
              url: ajax_url,
              type: 'POST',
              cache: false,
              timeout: 20000,
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              success: function (data) {
                if(data.status == 200){
                    window.location.reload();
                }else{
                  $('#popupModal .modal-body p').text(data.msg);
                  $('#popupModal').modal("show");
                  return false;
                }
              }
          });
    }
</script>
@endpush
