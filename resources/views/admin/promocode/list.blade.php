@extends('layouts.app')
@section('pageTitle')
    {{__('app.default_list_title',["app_name" => __('app.app_name'),"module"=> __('app.promocode')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">
        <div class="page-content">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ strtoupper(__('app.promocode')) }} LIST</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6">
                            <p class="">Promo Code (Active/Deactive)</p>
                            <div class="form-group">
                                <input type="checkbox" id="promocode" class="to-labelauty labelauty" name="inputLableautyCheckbox" data-plugin="labelauty" @if($is_promocode_active == 1){{"checked"}}@endif aria-hidden="true" style="display: none;">
                                <label for="labelauty-216410" aria-checked="true"></label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6">
                            <div style="float: right;">
                                <button type="button" class="btn btn-info func_SearchGridData"><i class="icon wb-search" aria-hidden="true"></i> Search</button>
                                <button type="button" class="btn btn-danger func_ResetGridData"><i class="icon wb-reload" aria-hidden="true"></i> Reset</button>
                                @can($module.'-create')
                                <a href="{{URL('admin/promocode/add')}}" class="btn btn-primary"
                                   style="margin-left: 10px;"><i class="icon fa-get-pocket" aria-hidden="true"></i>ADD {{ strtoupper(__('app.promocode'))  }}</a>
                                @endcan
                            </div>
                        </div>

                    </div>
                    <!-- Example Responsive -->
                    <table id="dataTable"
                           class="table display dt-responsive responsive nowrap table-striped table-hover table-fw-widget"
                           style="width: 100%;">

                        <thead>

                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Discount</th>
                            <th class="no-sort">Action</th>
                        </tr>

                        </thead>
                        <thead>
                        <tr>
                            <th>
                                <div class="input-search">
                                    <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control" name="filter[name]" placeholder="Search...">
                                </div>
                            </th>
                            <th>
                                <div class="input-search">
                                    <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control" name="filter[code]" placeholder="Search...">
                                </div>
                            </th>
                            <th>
                                <div class="input-search">
                                    <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control" name="filter[discount]" placeholder="Search...">
                                </div>
                            </th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                    <!-- End Example Responsive -->
                </div>
            </div>
        </div>


    </div>



@endsection

@push('externalJsLoad')
<script src="{{url('js/admin-js/appDatatable.js')}}"></script>
<script src="{{url('js/modules/promocode.js')}}"></script>
@endpush
@push('internalJsLoad')
<script>
    app.promocode.init();
    $(document).ready(function () {
        $('#promocode').on("click",function () {
            if ($(this).is(":checked"))
            {
                var status = 1;
            }else{
                var status = 0;
            }
            $.ajax({
                type: "POST",
                url: app.config.SITE_PATH +'admin/promocode/changeStatus',
                data: { status:status, _token: csrf_token}
            }).done(function(data){
                if(data.status == 200){
                    toastr.success(data.msg);
                }else{
                    toastr.error(data.msg);
                }

            });

        });
    });
</script>

@endpush