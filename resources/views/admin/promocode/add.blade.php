@extends('layouts.app')
@section('pageTitle')
    {{__('app.default_add_title',["app_name" => __('app.app_name'),"module"=> __('app.promocode')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">
        <div class="page-content">
            <div class="panel">
                @include('extra.message')
                    <div class="panel-heading">
                        <h3 class="panel-title">ADD {{ strtoupper(__('app.promocode'))  }}</h3>
                    </div>
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <form action="{{url('admin/promocode/store')}}" id="app_form" method="post"
                              class="col-lg-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder="Name" autocomplete="off" value="{{ Request::old('name') }}" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="time">Code</label>
                                    <input type="text" class="form-control" id="code" name="code"
                                           placeholder="Code" autocomplete="off" value="{{ Request::old('code') }}" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="time">Discount</label>
                                    <input type="text" class="form-control" id="discount" name="discount"
                                           placeholder="Discount (%)" autocomplete="off" value="{{ Request::old('discount') }}" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"
                                        name="action">{{trans('app.add')}} {{trans('app.promocode')}}
                                    <i class="mdi-content-send right"></i>
                                </button>
                                <a href="{{URL::previous()}}" class="btn btn-danger"
                                   style="margin-left: 10px;">{{trans('app.cancel')}}</a>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection