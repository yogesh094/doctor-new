@extends('admin.layouts.app')
@section('pageTitle')
    {{__('app.default_list_title',["app_name" => __('app.app_name'),"module"=> __('app.country')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">
        <div class="page-content">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ strtoupper(__('app.country')) }} LIST</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6">

                        </div>
                        <div class="col-lg-6 col-lg-pull-6">
                            <div style="float: right;">
                                <button type="button" class="btn btn-info func_SearchGridData"><i class="icon wb-search" aria-hidden="true"></i> Search</button>
                                <button type="button" class="btn btn-danger func_ResetGridData"><i class="icon wb-reload" aria-hidden="true"></i> Reset</button>
                                @can($module.'-create')
                                <a href="{{URL('admin/country/add')}}" class="btn btn-primary"
                                   style="margin-left: 10px;"><i class="icon fa-globe" aria-hidden="true"></i>ADD {{ strtoupper(__('app.country'))  }}</a>
                                @endcan
                            </div>
                        </div>

                    </div>
                    <!-- Example Responsive -->
                    <table id="dataTable"
                           class="table display dt-responsive responsive nowrap table-striped table-hover table-fw-widget"
                           style="width: 100%;">

                        <thead>

                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Currency</th>
                            <th>Payment Gateway</th>
                            <th>Status</th>
                            <th class="no-sort">Action</th>
                        </tr>

                        </thead>
                        <thead>
                        <tr>
                            <th>
                                <div class="input-search">
                                    <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control" name="filter[name]" placeholder="Search...">
                                </div>
                            </th>
                            <th>
                                <div class="input-search">
                                    <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control" name="filter[code]" placeholder="Search...">
                                </div>
                            </th>
                            <th>
                                <div class="input-search">
                                    <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control" name="filter[currency]" placeholder="Search...">
                                </div>
                            </th>
                            <th>
                                <div class="input-search">
                                    <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control" name="filter[payment_gateway]" placeholder="Search...">
                                </div>
                            </th>
                            <th>

                            </th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                    <!-- End Example Responsive -->
                </div>
            </div>
        </div>


    </div>



@endsection

@push('externalJsLoad')
<script src="{{url('js/admin-js/appDatatable.js')}}"></script>
<script src="{{url('js/modules/country.js')}}"></script>
@endpush
@push('internalJsLoad')
<script>
    app.country.init();
</script>

@endpush