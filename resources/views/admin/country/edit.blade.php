@extends('admin.layouts.app')
@section('pageTitle')
    {{__('app.default_edit_title',["app_name" => __('app.app_name'),"module"=> __('app.country')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">
        <div class="page-content">
            <div class="panel">

                @include('extra.message')

                <div class="panel-heading">
                    <h3 class="panel-title">EDIT {{ strtoupper(__('app.country'))  }}</h3>
                </div>
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <form action="{{url('admin/country/update')}}" id="app_form" method="post"
                              class="col-lg-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$details->name}}"
                                           placeholder="Name" autocomplete="off" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="code">Country Code</label>
                                    <input type="text" class="form-control" id="code" name="code"
                                           placeholder="Country Code" autocomplete="off" value="{{$details->code}}" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="code"> Phone Code</label>
                                    <input type="text" class="form-control" id="phonecode" name="phonecode"
                                           placeholder="Phone Code" autocomplete="off" value="{{$details->phonecode}}" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="currency">Currency</label>
                                    <input type="text" class="form-control" id="currency" name="currency"
                                           placeholder="Currency" autocomplete="off" value="{{$details->currency}}"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="payment_gateway">Payment Gateway</label>
                                    <select name="payment_gateway" class="form-control" required>
                                        <option value="">Select Payment Gateway</option>
                                            @if(count($paymentGatewayData) > 0)
                                                @foreach($paymentGatewayData as $row)
                                                    <option value="{{$row->name}}" @if($details->payment_gateway == $row->name){{"selected"}}@endif>{{$row->name}}</option>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label class="form-control-label" for="roles">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">{{trans('app.select')}}</option>
                                        <option value="1" @if(1 == $details->status){{"selected"}}@endif>{{trans('app.active')}}</option>
                                        <option value="0" @if(0 == $details->status){{"selected"}}@endif>{{trans('app.inactive')}}</option>
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="{{$details->id}}" />
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"
                                        name="action">{{trans('app.update')}} {{trans('app.country')}}
                                    <i class="mdi-content-send right"></i>
                                </button>
                                <a href="{{URL::previous()}}" class="btn btn-danger"
                                   style="margin-left: 10px;">{{trans('app.cancel')}}</a>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection