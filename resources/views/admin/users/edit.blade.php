@extends('admin.layouts.app')
@section('pageTitle')
    {{__('app.default_edit_title',["app_name" => __('app.app_name'),"module"=> __('app.user')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">
        <div class="page-content">
            <div class="panel">

                @include('extra.message')

                <div class="panel-heading">
                    <h3 class="panel-title">EDIT {{ strtoupper(__('app.user')) }}</h3>
                </div>
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <form action="{{url('admin/user/update/')}}" id="app_form" method="post"
                              class="col-lg-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$details->name}}"
                                           placeholder="Name" autocomplete="off" required/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="username">UserName</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="icon wb-user" aria-hidden="true"></i>
                                          </span>
                                        </div>
                                        <input type="text" class="form-control" name="username" value="{{ $details->username }}" placeholder="Username" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="password">Password</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="icon wb-lock" aria-hidden="true"></i>
                                          </span>
                                        </div>
                                        <input type="text" class="form-control" name="password" id="password" placeholder="Min length 8" minlength="8">
                                        <button type="button" class="btn btn-primary btn-block col-md-3" onclick="generate_password();">Password Generator</button>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="email">Email</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="icon wb-envelope" aria-hidden="true"></i>
                                          </span>
                                        </div>
                                        <input type="email" class="form-control" name="email" value="{{ $details->email }}" placeholder="email@email.com" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="form-control-label" for="roles">Roles</label>
                                        <select name="roles" class="form-control" required>
                                            @if(count($roles) > 0)
                                                @foreach($roles as $row)
                                                    <option value="{{$row->id}}" @if($userRole->role_id == $row->id){{"selected"}}@endif>{{$row->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-control-label" for="name">Position</label>
                                        <input type="text" class="form-control" id="position" name="position" value="{{$details->position}}"
                                               placeholder="Position" autocomplete="off" required/>
                                    </div>
                                </div>
                            <input type="hidden" name="id" id="id" value="{{$details->id}}" />
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"
                                        name="action">{{trans('app.update')}} {{trans('app.user')}}
                                    <i class="mdi-content-send right"></i>
                                </button>
                                <a href="{{URL::previous()}}" class="btn btn-danger"
                                   style="margin-left: 10px;">{{trans('app.cancel')}}</a>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection