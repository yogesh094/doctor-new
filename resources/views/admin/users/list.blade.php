@extends('admin.layouts.app')
@section('pageTitle')
    {{__('app.default_list_title',["app_name" => __('app.app_name'),"module"=> __('app.user')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">

        <div class="page-content">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ strtoupper(__('app.user')) }} LIST</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6">

                        </div>
                        <div class="col-lg-6 col-lg-pull-6">
                            <div style="float: right;">
                            <button type="button" class="btn btn-info func_SearchGridData"><i class="icon wb-search" aria-hidden="true"></i> Search</button>
                            <button type="button" class="btn btn-danger func_ResetGridData"><i class="icon wb-reload" aria-hidden="true"></i> Reset</button>
                            @can($module.'-create')
                            <a href="{{URL('admin/user/add')}}" class="btn btn-primary"
                               style="margin-left: 10px;"><i class="icon wb-user-add" aria-hidden="true"></i>ADD USER</a>
                            </div>
                            @endcan
                        </div>

                    </div>
                    <!-- Example Responsive -->
                    <table id="dataTable"
                           class="table display dt-responsive responsive nowrap table-striped table-hover table-fw-widget"
                           style="width: 100%;">

                        <thead>

                        <tr>
                            <th>Name</th>
                            <th>Email Address</th>
                            <th>status</th>
                            <th class="no-sort">Action</th>
                        </tr>

                        </thead>
                        <thead>
                        <tr>

                            <th>
                                <input type="text" name="filter[name]" value="" />
                            </th>

                            <th>
                                <input type="text" name="filter[email]" value="" />
                            </th>

                            <th>
                                <select name="filterSelect[status]" id="status" class="form-control" style="width: 100px;">
                                    <option value="">{{trans('app.select')}}</option>
                                    <option value="1">{{trans('app.active')}}</option>
                                    <option value="0">{{trans('app.inactive')}}</option>
                                </select>
                            </th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                    <!-- End Example Responsive -->
                </div>
            </div>
        </div>


    </div>



@endsection

@push('externalJsLoad')
<script src="{{url('js/admin-js/appDatatable.js')}}"></script>
<script src="{{url('js/modules/user.js')}}"></script>
@endpush
@push('internalJsLoad')
<script>
    app.user.init();
</script>

@endpush