@extends('admin.layouts.app')
@section('pageTitle')
    {{__('app.dashboard_title',["app_name" => __('app.app_name')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">
        <div class="page-content container-fluid">
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                

            </div>
        </div>
    </div>
@endsection
@push('externalJsLoad')
@endpush
@push('internalJsLoad')
@endpush
