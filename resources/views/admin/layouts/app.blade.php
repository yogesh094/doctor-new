<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('pageTitle')</title>
    <link rel='shortcut icon' href='{{ config('services.website.favicon') }}' type='image/x-icon' />
    <!-- Stylesheets -->

    <link rel="stylesheet" href="{{url('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/site.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/custom.css')}}">

    <!-- Plugins -->

    <link rel="stylesheet" href="{{url('global/vendor/animsition/animsition.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/intro-js/introjs.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/toastr/toastr.css')}}">
    <link rel="stylesheet" href="{{url('assets/examples/css/advanced/toastr.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/summernote/summernote.css')}}">


    <!-- DataTable CSS Starts-->
    <link rel="stylesheet" href="{{url('global/vendor/datatables.net-bs4/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">

    <link rel="stylesheet" href="{{url('assets/examples/css/tables/datatable.css')}}">
    <link rel="stylesheet" href="{{url('css/admin-css/jquery.loadmask.css')}}">
    <!-- DataTable CSS Ends-->

    <!-- Fonts -->
    <link rel="stylesheet" href="{{url('global/fonts/web-icons/web-icons.min.css')}}">
    <link rel="stylesheet" href="{{url('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel="stylesheet" href="{{url('global/fonts/font-awesome/font-awesome.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css">


    <!-- Scripts -->
    <script src="{{url('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
        Breakpoints();
    </script>

    @stack('externalCssLoad')
    @stack('internalCssLoad')

<!-- Scripts -->
    <script type="text/javascript">
        var baseUrl = "{{ url('/') }}/";
        var csrf_token = "<?php echo csrf_token(); ?>";

        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body class="animsition">
        @include('admin.layouts.header')
        @include('admin.layouts.leftsidebar')

        @yield('content')

        @include('admin.layouts.footer')

       <script src="{{url('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
       <script src="{{url('global/vendor/jquery/jquery.js')}}"></script>
       <script src="{{url('global/vendor/popper-js/umd/popper.min.js')}}"></script>
       <script src="{{url('global/vendor/bootstrap/bootstrap.js')}}"></script>
       <script src="{{url('global/vendor/animsition/animsition.js')}}"></script>
       <script src="{{url('global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
       <script src="{{url('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
       <script src="{{url('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
       <script src="{{url('global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>

       <!-- Plugins -->
       <script src="{{url('global/vendor/switchery/switchery.js')}}"></script>
       <script src="{{url('global/vendor/intro-js/intro.js')}}"></script>
       <script src="{{url('global/vendor/screenfull/screenfull.js')}}"></script>
       <script src="{{url('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
        <script src="{{url('global/vendor/summernote/summernote.min.js')}}"></script>


       <!-- Scripts -->
       <script src="{{url('global/js/Component.js')}}"></script>
       <script src="{{url('global/js/Plugin.js')}}"></script>
       <script src="{{url('global/js/Base.js')}}"></script>
       <script src="{{url('global/js/Config.js')}}"></script>

       <script src="{{url('assets/js/Section/Menubar.js')}}"></script>
       <script src="{{url('assets/js/Section/GridMenu.js')}}"></script>
       <script src="{{url('assets/js/Section/Sidebar.js')}}"></script>
       <script src="{{url('assets/js/Section/PageAside.js')}}"></script>
       <script src="{{url('assets/js/Plugin/menu.js')}}"></script>



       <script src="{{url('global/js/config/colors.js')}}"></script>
       <script src="{{url('assets/js/config/tour.js')}}"></script>
       <script>Config.set('assets', '../assets');</script>
       <!-- Page -->
       <script src="{{url('assets/js/Site.js')}}"></script>
       <script src="{{url('global/js/Plugin/asscrollable.js')}}"></script>
       <script src="{{url('global/js/Plugin/slidepanel.js')}}"></script>
       <script src="{{url('global/js/Plugin/switchery.js')}}"></script>
        <script src="{{url('global/vendor/toastr/toastr.js')}}"></script>


        <!--DataTable Js Starts Here-->
        <script src="{{url('global/vendor/datatables.net/jquery.dataTables.js')}}"></script>
        <script src="{{url('global/vendor/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
        <script src="{{url('global/vendor/datatables.net-scroller/dataTables.scroller.js')}}"></script>
        <script src="{{url('global/vendor/datatables.net-responsive/dataTables.responsive.js')}}"></script>
        <script src="{{url('global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js')}}"></script>
        <script src="{{url('global/vendor/datatables.net-buttons/dataTables.buttons.js')}}"></script>
        <script src="{{url('global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js')}}"></script>
        <script src="{{url('global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{url('global/js/Plugin/datatables.js')}}"></script>
        <script src="{{url('global/js/Plugin/input-group-file.js')}}"></script>
        <script src="{{url('assets/examples/js/tables/datatable.js')}}"></script>
        <script src="{{url('assets/examples/js/forms/editor-summernote.js')}}"></script>

        <!--DataTable Js Ends Here-->


        <!--Extra Config Js Starts-->
        <script src="{{url('js/admin-js/jquery.loadmask.min.js')}}" type="text/javascript"></script>
        <script src="{{url('js/admin-js/jsconfig.js')}}" type="text/javascript"></script>
        <script src="{{url('js/admin-js/main.js')}}" type="text/javascript"></script>

        <!--Extra Config Js Ends-->


        <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/js-yaml/3.6.0/js-yaml.min.js" type="text/javascript"></script>



<script>
    (function(document, window, $){
        'use strict';

        var Site = window.Site;
        $(document).ready(function(){
            Site.run();
        });
    })(document, window, jQuery);

    // For showing the toaster JS Session messages
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    @if($msg == 'success')
        toastr.success("<?php echo Session::get('alert-' . $msg);?>");
    @endif
    @if($msg == 'danger')
        toastr.error("<?php echo Session::get('alert-' . $msg);?>");
    @endif
    @if($msg == 'warning')
        toastr.warning("<?php echo Session::get('alert-' . $msg);?>");
    @endif
    @endif
    @endforeach

</script>
        @stack('externalJsLoad')
        @stack('internalJsLoad')
</body>
</html>