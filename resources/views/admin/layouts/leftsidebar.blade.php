<!-- Sidebar Menu Starts Here -->
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu" data-plugin="menu">
                    <li class="site-menu-category">Menus</li>
                    <li class="site-menu-item has-sub {{$homeTab ?? ''}}">
                        <a class="animsition-link" href="{{url('/admin/')}}">
                            <i style="color: #23b7e5" class="site-menu-icon wb-pie-chart" aria-hidden="true"></i>
                            <span class="site-menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="site-menu-item has-sub {{$userManagementTab ?? ''}}">
                        <a href="javascript:void(0)">
                            <i style="color: #fbe165;" class="site-menu-icon wb-users" aria-hidden="true"></i>
                            <span class="site-menu-title">User Management</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub" style="">
                           
                                <li class="site-menu-item has-sub {{$roleTab ?? ''}}">
                                    <a class="animsition-link" href="{{url('/admin/role')}}">
                                        <i style="color: #c678dd;" class="site-menu-icon wb-user-circle" aria-hidden="true"></i>
                                        <span class="site-menu-title">Roles</span>
                                    </a>
                                </li>
                            
                                <li class="site-menu-item has-sub {{$permissionTab ?? ''}}">
                                    <a class="animsition-link" href="{{url('/admin/permission')}}">
                                        <i style="color: #F03434;" class="site-menu-icon wb-lock" aria-hidden="true"></i>
                                        <span class="site-menu-title">Permissions</span>
                                    </a>
                                </li>
                            
                                <li class="site-menu-item has-sub {{$userTab ?? ''}}">
                                    <a class="animsition-link" href="{{url('/admin/user')}}">
                                        <i style="color: #fbe165;" class="site-menu-icon wb-users" aria-hidden="true"></i>
                                        <span class="site-menu-title">Users</span>
                                    </a>
                                </li>
                           
                        </ul>
                    </li>
                  
                        <li class="site-menu-item has-sub {{ $countryTab ?? ''}}">
                            <a class="animsition-link" href="{{url('/admin/country')}}">
                                <i style="color: #00e7af;" class="site-menu-icon fa-globe" aria-hidden="true"></i>
                                <span class="site-menu-title">Country</span>
                            </a>
                        </li>
                   
                </ul>
            </div>
        </div>
    </div>

    <div class="site-menubar-footer">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Settings">
            {{--<span class="icon wb-settings" aria-hidden="true"></span>--}}
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
            {{--<span class="icon wb-eye-close" aria-hidden="true"></span>--}}
        </a>
        <a href="{{url('/admin/logout')}}"
           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon wb-power" aria-hidden="true"></span>
        </a>

        <form id="logout-form" action="{{url('/admin/logout')}}" method="POST" style="display: none;">
            @csrf
        </form>
    </div></div>    <div class="site-gridmenu">
    <div>
        <div>
            <ul>
                <li>
                    <a href="../apps/mailbox/mailbox.html">
                        <i class="icon wb-envelope"></i>
                        <span>Mailbox</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/calendar/calendar.html">
                        <i class="icon wb-calendar"></i>
                        <span>Calendar</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/contacts/contacts.html">
                        <i class="icon wb-user"></i>
                        <span>Contacts</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/media/overview.html">
                        <i class="icon wb-camera"></i>
                        <span>Media</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/documents/categories.html">
                        <i class="icon wb-order"></i>
                        <span>Documents</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/projects/projects.html">
                        <i class="icon wb-image"></i>
                        <span>Project</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/forum/forum.html">
                        <i class="icon wb-chat-group"></i>
                        <span>Forum</span>
                    </a>
                </li>
                <li>
                    <a href="../index.html">
                        <i class="icon wb-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- Sidebar Menu Ends Here -->
