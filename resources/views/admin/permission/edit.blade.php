@extends('admin.layouts.app')
@section('pageTitle')
    {{__('app.default_edit_title',["app_name" => __('app.app_name'),"module"=> __('app.permission')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">
        <div class="page-content">
            <div class="panel">

                @include('extra.message')

                <div class="panel-heading">
                    <h3 class="panel-title">EDIT {{ strtoupper(__('app.permission'))  }}</h3>
                </div>
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <form action="{{url('admin/permission/update')}}" id="app_form" method="post"
                              class="col-lg-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="model_name" name="model_name" value="{{$details->model_name}}"
                                           placeholder="Name" autocomplete="off" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="model_slug" name="model_slug" value="{{$details->model_slug}}"
                                           placeholder="Name" autocomplete="off" required/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$details->name}}"
                                           placeholder="Name" autocomplete="off" required/>
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="{{$details->id}}" />
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"
                                        name="action">{{trans('app.update')}} {{trans('app.permission')}}
                                    <i class="mdi-content-send right"></i>
                                </button>
                                <a href="{{URL::previous()}}" class="btn btn-danger"
                                   style="margin-left: 10px;">{{trans('app.cancel')}}</a>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection