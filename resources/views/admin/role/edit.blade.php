@extends('admin.layouts.app')
@section('pageTitle')
    {{__('app.default_edit_title',["app_name" => __('app.app_name'),"module"=> __('app.role')])}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="page">
        <div class="page-content">
            <div class="panel">

                @include('extra.message')

                <div class="panel-heading">
                    <h3 class="panel-title">EDIT {{ strtoupper(__('app.role')) }}</h3>
                </div>
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <form action="{{url('admin/role/update')}}" id="app_form" method="post"
                              class="col-lg-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$details->name}}"
                                           placeholder="Name" autocomplete="off" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="name">Permission</label>
                                    <div class="permissionTab">
                                        <div class="panel-group" id="exampleAccordionDefault" aria-multiselectable="true"
                                             role="tablist">
                                            @php
                                                $count = 1;
                                            @endphp
                                            @foreach($permissionList as $key => $list)
                                                <div class="panel">
                                                    <div class="panel-heading" id="list<?= $count ?>" role="tab">
                                                        <a class="panel-title collapsed" data-toggle="collapse" href="#listCollapse<?= $count ?>"
                                                           data-parent="#exampleAccordionDefault" aria-expanded="false"
                                                           aria-controls="listCollapse<?= $count ?>">
                                                            {{ $key }}
                                                        </a>
                                                    </div>
                                                    <div class="panel-collapse collapse" id="listCollapse<?= $count ?>" aria-labelledby="list<?= $count ?>"
                                                         role="tabpanel">
                                                        <div class="panel-body">
                                                            @foreach($list as $id => $value)
                                                                <div class="checkbox-custom checkbox-primary">
                                                                    <input type="checkbox" id="inputChecked<?= $id ?>" name="permission[]" value="<?= $id ?>" @if(in_array($id, $rolePermissions)){{"checked"}}@endif>
                                                                    <label for="inputChecked<?= $id ?>">{{ $value }}</label>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                @php $count++; @endphp
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" id="id" value="{{$details->id}}" />
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"
                                        name="action">{{trans('app.update')}} {{trans('app.role')}}
                                    <i class="mdi-content-send right"></i>
                                </button>
                                <a href="{{URL::previous()}}" class="btn btn-danger"
                                   style="margin-left: 10px;">{{trans('app.cancel')}}</a>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection